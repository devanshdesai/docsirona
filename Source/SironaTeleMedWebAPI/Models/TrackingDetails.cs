﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OptnBuyClient.Models
{
    public class TrackingDetails
    {
        public List<ShipmentData> ShipmentData { get; set; }
    }

    public class Status
    {
        public string status { get; set; }
        public string StatusLocation { get; set; }
        public DateTime StatusDateTime { get; set; }
        public string RecievedBy { get; set; }
        public string Instructions { get; set; }
        public string StatusType { get; set; }
        public string StatusCode { get; set; }
    }

    public class GeoLocation
    {
        public double lat { get; set; }
        public double @long { get; set; }
    }

    public class ScanDetail
    {
        public DateTime ScanDateTime { get; set; }

        public string ScanDateTimeString => ScanDateTime != null ? ScanDateTime.ToString("ddd, dd MMM yyy hh:mm tt") : "";

        public string ScanType { get; set; }
        public string Scan { get; set; }
        public DateTime StatusDateTime { get; set; }
        public string ScannedLocation { get; set; }
        public string Instructions { get; set; }
        public string StatusCode { get; set; }
        public int? call_duration { get; set; }
        public GeoLocation geo_location { get; set; }
    }

    public class Scan
    {
        public ScanDetail ScanDetail { get; set; }
    }

    public class Shipment
    {
        public string Origin { get; set; }
        public Status Status { get; set; }
        public DateTime PickUpDate { get; set; }
        public object ChargedWeight { get; set; }
        public string OrderType { get; set; }
        public string Destination { get; set; }
        public string ReferenceNo { get; set; }
        public object ReturnedDate { get; set; }
        public DateTime DestRecieveDate { get; set; }
        public DateTime OriginRecieveDate { get; set; }
        public DateTime OutDestinationDate { get; set; }
        public int CODAmount { get; set; }
        public List<object> EWBN { get; set; }
        public object FirstAttemptDate { get; set; }
        public bool ReverseInTransit { get; set; }
        public List<Scan> Scans { get; set; }
        public string SenderName { get; set; }
        public string AWB { get; set; }
        public int DispatchCount { get; set; }
        public int InvoiceAmount { get; set; }
    }

    public class ShipmentData
    {
        public Shipment Shipment { get; set; }
    }  

}