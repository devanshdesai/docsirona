﻿using SironaTeleMed.APICommon;
using SironaTeleMed.Common;
using SironaTeleMed.Entities.Contract;
using SironaTeleMed.Entities.V1;
using SironaTeleMed.Services.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace SironaTeleMedWebAPI.Controllers.V1
{
    public class AccountV1Controller : AbstractBaseController
    {
        private AbstractUsersService usersService;

        public AccountV1Controller(AbstractUsersService usersService)
        {
            this.usersService = usersService;
        }

        [HttpPost]
        [InheritedRoute("Login")]
        public async Task<IHttpActionResult>Login(string Email, string Password)
        {
            AbstractUsers abstractUsers = new Users();
            abstractUsers.Email = Email;
            abstractUsers.Password = Password;
            SuccessResult<AbstractUsers> result = usersService.Login(abstractUsers);
            if (result.Item != null)
            {
                return this.Content(HttpStatusCode.OK, result);
            }
            else
            {                
                return this.Content(HttpStatusCode.NotFound, result);
            }
        }

    }
}