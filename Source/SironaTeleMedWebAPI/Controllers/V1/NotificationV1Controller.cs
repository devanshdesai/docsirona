﻿using SironaTeleMed.APICommon;
using SironaTeleMed.Common;
using SironaTeleMed.Common.Paging;
using SironaTeleMed.Entities.Contract;
using SironaTeleMed.Entities.V1;
using SironaTeleMed.Services.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace SironaTeleMedWebAPI.Controllers.V1
{
    public class NotificationV1Controller : AbstractBaseController
    {
        private AbstractNotificationsService AbstractNotifications;

        // GET: UserV1
        public NotificationV1Controller(AbstractNotificationsService AbstractNotifications)
        {
            this.AbstractNotifications = AbstractNotifications;
        }
        // GET: NotificationV1
        [HttpPost]
        [InheritedRoute("AddNotifications")]
        public async Task<IHttpActionResult> AddNotifications([FromBody]Notifications notifications)
        {
            SuccessResult<AbstractNotifications> result = new SuccessResult<AbstractNotifications>();
            if (notifications.NotificationTo == 0)
            {
               
                //result.Message = Users.;
                return this.Content(HttpStatusCode.BadRequest, "NotificationTo Can't be empty");
            }
            else
            {
                result = this.AbstractNotifications.NotificationsInsert(notifications);
                return this.Content(HttpStatusCode.OK, result);
            }
           
        }
        [HttpGet]
        [InheritedRoute("GetNotificationByNotificationTo/{id:int}")]
        public async Task<IHttpActionResult> GetNotificationByNotificationTo(int id)
        {
            PageParam pageParam = new PageParam();
            PagedList<AbstractNotifications> result = this.AbstractNotifications.NotificationsSelectByNotificationTo(pageParam,id);

            return this.Content(HttpStatusCode.OK, result);
        }
    }
}