﻿using SironaTeleMed.APICommon;
using SironaTeleMed.Common;
using SironaTeleMed.Common.Paging;
using SironaTeleMed.Entities.Contract;
using SironaTeleMed.Entities.V1;
using SironaTeleMed.Services.Contract;
using Stripe;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;
using Twilio.Jwt.AccessToken;
using Twilio.Rest.Video.V1;

namespace SironaTeleMedWebAPI.Controllers.V1
{
    public class AppointmentV1Controller : AbstractBaseController
    {
        private readonly AbstractUsersService usersService;
        private AbstractSessionsServices AbstractSessions;
        private AbstractSessionScheduleServices abstractSessionSchedule;

        public AppointmentV1Controller(AbstractSessionsServices AbstractSessions, AbstractSessionScheduleServices abstractSessionSchedule, AbstractUsersService usersService)
        {
            this.AbstractSessions = AbstractSessions;
            this.abstractSessionSchedule = abstractSessionSchedule;
            this.usersService = usersService;
        }

        [HttpPost]
        [InheritedRoute("SearchAppointments")]
        public async Task<IHttpActionResult> SearchAppointments(string date)
        {
            PagedList<AbstractSessionSchedule> result = new PagedList<AbstractSessionSchedule>();
            result = abstractSessionSchedule.SearchSessionsSelectByDate(date);
            return this.Content(HttpStatusCode.OK, result);
        }

        [HttpPost]
        [InheritedRoute("GetAppointments")]
        public async Task<IHttpActionResult> GetAppointments(PageParam pageParam, int UserId, string StatusId, string DateSearch = "")
        {
            PagedList<AbstractSessions> result = new PagedList<AbstractSessions>();
            result = AbstractSessions.SessionScheduleSelectByUserIdStatusId(pageParam, StatusId, UserId, DateSearch);
            return this.Content(HttpStatusCode.OK, result);
        }

        [HttpPost]
        [InheritedRoute("BookAppointment")]
        public async Task<IHttpActionResult> BookAppointment(Sessions sessions)
        {
            SuccessResult<AbstractSessions> successResult = new SuccessResult<AbstractSessions>();
            if (sessions.SessionScheduleId > 0 || sessions.UserId > 0)
            {
                successResult = AbstractSessions.InsertUpdateSessions(sessions);
                if (successResult.Item != null)
                {
                    string PatientEmail = successResult.Item.PatientEmail;
                    string PatientName = successResult.Item.PatientName;
                    if (PatientEmail != "")
                    {
                        string body = string.Empty;
                        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/EmailTemplate/Telemed.html")))
                        {
                            body = reader.ReadToEnd();
                        }
                        body = body.Replace("#titlemsg1", "New Appointment");
                        body = body.Replace("#titlemsg2", "");
                        body = body.Replace("#name", PatientName);
                        body = body.Replace("#mainmsg", "Your appointment for " + successResult.Item.DisplayDate + " at " + successResult.Item.DisplayTime + " has been successfully created and a doctor shall soon be assigned");
                        EmailHelper.Send(PatientEmail, "", "", "New Appointment Registered", body);
                        CommonHelper.NotificationSent(successResult.Item.PatientDeviceToken,
                            "Your appointment for " + successResult.Item.DisplayDate + " at " + successResult.Item.DisplayTime + " has been successfully created and a doctor shall soon be assigned", "New Appointment Registered");
                        CommonHelper.SendTextMessage("Your appointment for " + successResult.Item.DisplayDate + " at " + successResult.Item.DisplayTime + " has been successfully created and a doctor shall soon be assigned", successResult.Item.PatientPhone, "");
                    }

                    #region Stripe charge code
                    try
                    {
                        StripeConfiguration.ApiKey = Configurations.StripeSecretKey;

                        var planService = new PlanService();
                        var planOptions = new PlanListOptions
                        {
                            ProductId = Configurations.ProductId
                        };
                        var planList = planService.List(planOptions).FirstOrDefault();

                        var customerService = new CustomerService();
                        var sourceId = customerService.Get(successResult.Item.StripeId).DefaultSourceId;

                        var items = new List<SubscriptionItemOption> {
                        new SubscriptionItemOption {
                            PlanId = planList.Id
                           }
                        };
                        var options = new SubscriptionCreateOptions
                        {
                            CustomerId = successResult.Item.StripeId,
                            Items = items
                        };
                        var subscriptionService = new SubscriptionService();
                        Subscription subscription = subscriptionService.Create(options);

                        var invoiceService = new InvoiceService();
                        var invoice = invoiceService.Get(subscription.LatestInvoiceId);

                        var paymentIntentService = new PaymentIntentService();

                        var paymentintentUpdate = new PaymentIntentUpdateOptions
                        {
                            Description = "Your appointment for " + successResult.Item.DisplayDate + " at " + successResult.Item.DisplayTime + " has been successfully created -> (" + successResult.Item.PatientName + "," + successResult.Item.Id + ")"
                        };

                        paymentIntentService.Update(invoice.PaymentIntentId, paymentintentUpdate);
                        #endregion

                    }
                    catch (Exception ex)
                    {

                    }

                    return this.Content(HttpStatusCode.OK, successResult);
                }
                else
                {
                    if (successResult.Code == 400)
                    {
                        successResult.Code = 400;
                        successResult.Message = "Appointment can't be booked as it already exists";
                        return this.Content(HttpStatusCode.InternalServerError, successResult);
                    }
                    else
                    {
                        successResult.Code = 500;
                        successResult.Message = "An error occure while booking the appointment";
                        return this.Content(HttpStatusCode.InternalServerError, successResult);
                    }
                }
            }
            else
            {
                successResult.Code = 400;
                successResult.Message = "Bad Request";
                return this.Content(HttpStatusCode.BadRequest, successResult);
            }
        }

        [HttpPost]
        [InheritedRoute("GetAppointmentById")]
        public async Task<IHttpActionResult> GetAppointmentById(int Id,int UserType,int UserId)
        {
            CommonHelper.CallViaTwilio("test", "9149025545", "ALEXA");
            SuccessResult<AbstractSessions> successResult = new SuccessResult<AbstractSessions>();
            successResult = AbstractSessions.SessionsSelect(Id, UserType,UserId);
            if (successResult.Item != null)
            {
                successResult.Item.UserDescription = successResult.Item.UUserDescription;
                successResult.Item.SpecialRequests = successResult.Item.SSpecialRequests;
                successResult.Item.DoctorComments =  successResult.Item.DDoctorComments;
                successResult.Item.DoctorComments2 = successResult.Item.DDoctorComments2;
                successResult.Item.DoctorComments3 = successResult.Item.DDoctorComments3;
                successResult.Item.DoctorComments4 = successResult.Item.DDoctorComments4;
                return this.Content(HttpStatusCode.OK, successResult);
            }
            else
            {
                successResult.Code = 404;
                successResult.Message = "Appointment Details Not Found";
                return this.Content(HttpStatusCode.NotFound, successResult);
            }
        }

        [HttpPost]
        [InheritedRoute("UpdateAppointmentStatus")]
        public IHttpActionResult UpdateAppointmentStatus(int Id, string CancelledReason = "")
        {
            SuccessResult<AbstractSessions> successResult = new SuccessResult<AbstractSessions>();
            if (Id > 0)
            {
                successResult = AbstractSessions.SessionUpdateByStatusId(Id, 4, "", CancelledReason, "0");
                if (successResult.Item != null)
                {
                    string body = string.Empty;
                    string doctorbody = string.Empty;
                    string PatientEmail = successResult.Item.PatientEmail;
                    string PatientName = successResult.Item.PatientName;
                    string DoctorEmail = successResult.Item.DoctorEmail;
                    string DoctorName = successResult.Item.DoctorName;
                    using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/EmailTemplate/Telemed.html")))
                    {
                        body = reader.ReadToEnd();
                        doctorbody = body;
                    }

                    if (PatientEmail != "")
                    {

                        body = body.Replace("#titlemsg1", "Appointment Cancelled");
                        body = body.Replace("#titlemsg2", "");
                        body = body.Replace("#name", PatientName);
                        if (CancelledReason != string.Empty)
                        {
                            body = body.Replace("#mainmsg", "Your appointment for " + successResult.Item.DisplayDate + " at " + successResult.Item.DisplayTime + " has been cancelled due to the following reason : " + CancelledReason);
                        }
                        else
                        {
                            body = body.Replace("#mainmsg", "Your appointment for " + successResult.Item.DisplayDate + " at " + successResult.Item.DisplayTime + " has been cancelled");
                        }

                        EmailHelper.Send(PatientEmail, "", "", "Appointment Cancelled", body);
                        CommonHelper.NotificationSent(successResult.Item.PatientDeviceToken, "Your appointment for " + successResult.Item.DisplayDate + " at " + successResult.Item.DisplayTime + " has been cancelled", "Appointment Cancelled");
                        CommonHelper.SendTextMessage("Your appointment for " + successResult.Item.DisplayDate + " at " + successResult.Item.DisplayTime + " has been cancelled", successResult.Item.PatientPhone, "");
                    }

                    if (DoctorEmail != "")
                    {
                        doctorbody = doctorbody.Replace("#titlemsg1", "Appointment Cancelled");
                        doctorbody = doctorbody.Replace("#titlemsg2", "");
                        doctorbody = doctorbody.Replace("#name", DoctorName);
                        if (CancelledReason != string.Empty)
                        {
                            doctorbody = doctorbody.Replace("#mainmsg", "Your appointment for " + successResult.Item.DisplayDate + " at " + successResult.Item.DisplayTime + " has been cancelled due to the following reason : " + CancelledReason);
                        }
                        else
                        {
                            doctorbody = doctorbody.Replace("#mainmsg", "Your appointment for " + successResult.Item.DisplayDate + " at " + successResult.Item.DisplayTime + " has been cancelled");
                        }
                        EmailHelper.Send(DoctorEmail, "", "", "Appointment Cancelled", doctorbody);
                        CommonHelper.NotificationSent(successResult.Item.DoctorDeviceToken, "Your appointment for " + successResult.Item.DisplayDate + " at " + successResult.Item.DisplayTime + " has been cancelled", "Appointment Cancelled");
                        CommonHelper.SendTextMessage("Your appointment for " + successResult.Item.DisplayDate + " at " + successResult.Item.DisplayTime + " has been cancelled", successResult.Item.DoctorPhone, "");
                    }

                    return this.Content(HttpStatusCode.OK, successResult);
                }
                else
                {
                    successResult.Code = 500;
                    successResult.Message = "An error occure while updating the appointment status";
                    return this.Content(HttpStatusCode.InternalServerError, successResult);
                }
            }
            else
            {
                successResult.Code = 400;
                successResult.Message = "Bad Request";
                return this.Content(HttpStatusCode.BadRequest, successResult);
            }
        }

        [HttpPost]
        [InheritedRoute("CallForTwilio")]
        public IHttpActionResult CallForTwilio(int Id, int UserType, int UserId)
        {
            try
            {
                string twilioAccountSid = ConfigurationManager.AppSettings["twilioAccountSid"].ToString(); //"AC68f41fd9b35c118de63bef2c1bee11cf";
                string twilioApiKey = ConfigurationManager.AppSettings["twilioApiKey"].ToString();     //"SK69a5a7ad66a9ccd6e415cadcffba357a";
                string twilioApiSecret = ConfigurationManager.AppSettings["twilioApiSecret"].ToString();   //"hjs2AqPYJ4p07DthfgWjgGTK7IU5UcHH";
                string accountSid = ConfigurationManager.AppSettings["accountSid"].ToString();  //"AC68f41fd9b35c118de63bef2c1bee11cf";
                string authToken = ConfigurationManager.AppSettings["authToken"].ToString(); //"aac151e76013a26e66b24f6cf0ddc828";

                SuccessResult<AbstractSessions> obj = AbstractSessions.SessionsSelect(Id, UserType, UserId);
                Twilio.TwilioClient.Init(accountSid, authToken);
                string identity = obj.Item.PatientName; // User name

                var room = RoomResource.Fetch(pathSid: "room" + Id); // Room15
                var grant = new VideoGrant();
                grant.Room = "room" + Id; // Room15

                var grants = new HashSet<IGrant> { grant };

                // Create an Access Token generator
                var token = new Twilio.Jwt.AccessToken.Token(
                    twilioAccountSid,
                    twilioApiKey,
                    twilioApiSecret,
                    identity: identity,
                    grants: grants);

                return this.Content(HttpStatusCode.OK, token.ToJwt());
            }
            catch(Exception ex)
            {
                return this.Content(HttpStatusCode.InternalServerError, ex);
            }
            
        }

        [HttpPost]
        [InheritedRoute("AddCard")]
        public IHttpActionResult AddCard(string stripeToken, int UserId)
        {
            try
            {
                AbstractUsers users = usersService.UsersSelect(UserId).Item;

                StripeConfiguration.ApiKey = Configurations.StripeSecretKey;

                var options = new CustomerCreateOptions { Email = users.Email, Name = users.FullName, Description = "Customer for " + users.Email, Source = stripeToken };
                var service = new Stripe.CustomerService();

                Customer customer = service.Create(options);

                users.StripeId = customer.Id;
                usersService.InsertUpdateUsers(users);
                return this.Content(HttpStatusCode.OK, customer.Id);
            }
            catch (Exception ex)
            {
                return this.Content(HttpStatusCode.InternalServerError, ex);
            }
        }
    }
}