﻿using SironaTeleMed.APICommon;
using SironaTeleMed.Common;
using SironaTeleMed.Common.Paging;
using SironaTeleMed.Entities.Contract;
using SironaTeleMed.Entities.V1;
using SironaTeleMed.Services.Contract;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace SironaTeleMedWebAPI.Controllers.V1
{
    public class MasterDropDownV1Controller : AbstractBaseController
    {
        private readonly AbstractMasterCountryServices masterCountryServices;
        private readonly AbstractMasterStateService masterStateServices;        

        public MasterDropDownV1Controller(AbstractMasterCountryServices masterCountryServices, AbstractMasterStateService masterStateServices)
        {
            this.masterCountryServices = masterCountryServices;
            this.masterStateServices = masterStateServices;
        }

        [HttpPost]
        [InheritedRoute("GetCountries")]
        public async Task<IHttpActionResult> GetCountries()
        {
            PageParam pageParam = new PageParam();
            var result = masterCountryServices.MasterCountrySelectAll(pageParam);
            return this.Content(HttpStatusCode.OK, result);
        }

        [HttpPost]
        [InheritedRoute("GetStates")]
        public async Task<IHttpActionResult> GetStates(int Id)
        {
            PageParam pageParam = new PageParam();
            var result = masterStateServices.MasterStateSelectAll(pageParam);
            result.Values = result.Values.Where(x => x.CountryId == Id).ToList();
            result.TotalRecords = result.Values.Count();
            return this.Content(HttpStatusCode.OK, result);
        }

    }
}