﻿using SironaTeleMed.APICommon;
using SironaTeleMed.Common;
using SironaTeleMed.Entities.Contract;
using SironaTeleMed.Entities.V1;
using SironaTeleMed.Services.Contract;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace SironaTeleMedWebAPI.Controllers.V1
{
    public class UserV1Controller : AbstractBaseController
    {
        private AbstractUsersService abstractUsersService;
        private string otp;
       
        public UserV1Controller(AbstractUsersService abstractUsersService)
        {
            this.abstractUsersService = abstractUsersService;
        }
        
        [HttpPost]
        [InheritedRoute("SignUp")]
        public async Task<IHttpActionResult> SignUp(Users user)
        {
            user.UserType = 2;
            user.CreatedBy = 1;
            SuccessResult<AbstractUsers> result = new SuccessResult<AbstractUsers>();
            if (user != null)
            {
                if (user.Password.Length >= 6)
                {
                    result = this.abstractUsersService.InsertUpdateUsers(user);
                    if(result != null && result.Item != null && result.Code == 200)
                    {
                        result.Code = 200;
                        result.Message = "User Registered Successfully";
                        string Email = result.Item.Email;
                        if (Email != "")
                        {
                            string body = string.Empty;

                            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/EmailTemplate/Telemed.html")))
                            {
                                body = reader.ReadToEnd();
                            }
                            body = body.Replace("#titlemsg1", "Welcome,");
                            body = body.Replace("#titlemsg2", "to Sirona Tele Med !");
                            body = body.Replace("#name", result.Item.FirstName + " " + result.Item.LastName);
                            body = body.Replace("#mainmsg", "Thank you for signing up. You can now log into your account and book an appointment");
                            EmailHelper.Send(Email, "", "", "Welcome To Sirona Tele Med", body);
                            CommonHelper.SendTextMessage("Welcome To Sirona Tele Med and Thank you for signing up. You can now log into your account and book an appointment", result.Item.Phone, "");
                        }
                        return this.Content(HttpStatusCode.OK, result);
                    }
                    else
                    {
                        if(result.Code != 400)
                        {
                            result.Code = 500;
                            result.Message = "Internal Error Occured";
                        }
                        return this.Content(HttpStatusCode.OK, result);
                    }                    
                }
                else
                {
                    result.Message = "You have to enter at least 6 characters.";
                    result.Code = 400;
                    result.Item = null;
                    return this.Content(HttpStatusCode.OK, result);
                }
            }
            else
            {
                result.Code = 400;
                result.Message = "Please enter valid input";
                return this.Content(HttpStatusCode.BadRequest, result);
            }
        }
        
        [HttpPost]
        [InheritedRoute("GetUserProfileById/{id:int}")]
        public async Task<IHttpActionResult> GetUserProfileById(int Id)
        {
            SuccessResult<AbstractUsers> result = this.abstractUsersService.UsersSelect(Id);
            if(result.Item.UserType == 2)
            {
                return this.Content(HttpStatusCode.OK, result);
            }
            else
            {
                result.Code = 400;
                result.Message = "Not Authorised To Access The Record";
                result.Item = null;
                return this.Content(HttpStatusCode.BadRequest, result);
            }
            
        }
        
        [HttpPost]
        [InheritedRoute("LogIn")]
        public async Task<IHttpActionResult> LogIn(string Email, string Password,string DeviceToken)
        {
            AbstractUsers users = new Users();
            users.Email = Email;
            users.Password = Password;
            users.DeviceToken = DeviceToken;
            SuccessResult<AbstractUsers> result = this.abstractUsersService.Login(users);
            if (result.Item != null)
            {
                if (result.Item.UserType == 2)
                {
                    return this.Content(HttpStatusCode.OK, result);
                }
                else
                {
                    result.Message = "User does not exist";
                    result.Code = 404;
                    result.Item = null;
                    return this.Content(HttpStatusCode.NotFound, result);
                }
            }
            else
            {
                return this.Content(HttpStatusCode.NotFound, result);
            }
        }

        [HttpPost]
        [InheritedRoute("ResetPassword")]
        public async Task<IHttpActionResult> ResetPassword(int Id, string OldPassword, string Password)
        {
            SuccessResult<AbstractUsers> result = new SuccessResult<AbstractUsers>();
            AbstractUsers users = new Users();
            users.OldPassword = OldPassword;
            users.Id = Id;
            
            int id = Id;
            var data = abstractUsersService.UsersSelect(id).Item;
            if(data != null)
            {
                if (OldPassword == data.Password)
                {
                    users.Id = Id;
                    users.Password = Password;
                    result = abstractUsersService.UsersResetPassword(users);
                    if(result.Item != null)
                    {
                        return this.Content(HttpStatusCode.OK, result);
                    }
                    else
                    {
                        result.Code = 500;
                        result.Message = "internal Server Error";
                        return this.Content(HttpStatusCode.OK, result);
                    }                    
                }
                else
                {
                    result.Code = 400;
                    result.Message = "Old Password does not match with the one in the system";
                    return this.Content(HttpStatusCode.BadRequest, result);
                }
            }
            else
            {
                result.Code = 404;
                result.Message = "User Not Found";
                return this.Content(HttpStatusCode.NotFound, result);
            }           
        }

       
        [HttpPost]
        [InheritedRoute("UpdateProfile")]
        public async Task<IHttpActionResult> UpdateProfile(Users users)
        {
            SuccessResult<AbstractUsers> result = new SuccessResult<AbstractUsers>();
            int id = users.Id;
            var data = abstractUsersService.UsersSelect(id).Item;
            if(data != null)
            {
                users.Email = data.Email;
                users.Password = data.Password;               
                result = this.abstractUsersService.InsertUpdateUsers(users);
                if (result.Item != null)
                {
                    return this.Content(HttpStatusCode.OK, result);
                }
                else
                {
                    result.Code = 500;
                    result.Message = "Internal Server Error";
                    return this.Content(HttpStatusCode.InternalServerError, result);
                }                
            }
            else
            {
                result.Code = 404;
                result.Message = "User Not Found";
                return this.Content(HttpStatusCode.NotFound, result);
            }
        }



        #region Old Code for Creating the Password
        //[HttpPost]
        //[InheritedRoute("CreatePassword")]
        //public async Task<IHttpActionResult> CreatePassword([FromUri]string Password, string ConfirmPassword)
        //{
        //    return this.Content(HttpStatusCode.OK, 111);
        //}
        //[HttpGet]
        //[InheritedRoute("ForgotPassword")]
        //public async Task<IHttpActionResult> ForgotPassword(string email, string otp = "")
        //{

        //    string otpvalue = "";
        //    if (!string.IsNullOrWhiteSpace(otp))
        //    {
        //        otpvalue = otp;
        //    }
        //    else
        //    {
        //        Random generator = new Random();
        //        int r = generator.Next(1, 1000000);
        //        string s = r.ToString().PadLeft(6, '0');
        //        otpvalue = s;
        //    }

        //    string body = "<p>Hello, we have recieved a request to change the password for Sirona Tele Med login<br/>Following is your one time password :" + (otpvalue) + "</p>";


        //    AdminEmailService.Send(email, string.Empty, string.Empty, "Password Change Request", body, null);

        //    return this.Content(HttpStatusCode.OK, otpvalue);

        //}
        #endregion

    }
}