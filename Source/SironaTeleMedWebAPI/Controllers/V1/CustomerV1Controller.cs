
namespace SironaTeleMedWebAPI.Controllers.V1
{
    using System;
    using System.Net;
    using System.Threading.Tasks;
    using System.Web.Http;
    using SironaTeleMed.APICommon;
    using SironaTeleMed.Common;
    using SironaTeleMed.Common.Paging;
    using SironaTeleMed.Entities.Contract;
    using SironaTeleMed.Entities.V1;
    using SironaTeleMed.Services.Contract;
    using System.Data;
    using System.Data.SqlClient;
    using System.Net.Http;
    using System.Web;
    using System.IO;
    using System.Web.Script.Serialization;

    public class CustomerV1Controller : AbstractBaseController
    {
        //private AbstractCustomerServices absCustomerServices;
        //private AbstractMessageServices absmessageServices;
        //private AbstractCashbackTransactionServices cashbackTransactionServices;
        //private AbstractNotificationService abstractNotificationService;
        //private AbstractEmailServices emailServices;

        public CustomerV1Controller(
            //AbstractCustomerServices _absCustomerServices, AbstractCashbackTransactionServices cashbackTransactionServices, AbstractNotificationService abstractNotificationService, AbstractEmailServices emailServices, AbstractMessageServices absmessageServices
            )
        {
            //this.absCustomerServices = _absCustomerServices;
            //this.cashbackTransactionServices = cashbackTransactionServices;
            //this.abstractNotificationService = abstractNotificationService;
            //this.emailServices = emailServices;
            //this.absmessageServices = absmessageServices;
        }

        //[HttpPost]
        //[InheritedRoute("SignUp")]
        //public async Task<IHttpActionResult> SignUp([FromBody]Customer customer)
        //{
        //    customer.IsActive = false;
        //    if (!string.IsNullOrWhiteSpace(customer.ReferenceCode))
        //    {
        //        var model = absCustomerServices.InsertRefId(customer.ReferenceCode);

        //        if (!model.IsSuccessStatusCode)
        //        {
        //            return this.Content(HttpStatusCode.OK, model);
        //        }
        //        else
        //        {
        //            customer.RefCustomerId = model.Item.Id;
        //        }
        //    }
        //    SuccessResult<AbstractCustomer> result = this.absCustomerServices.InsertUpdateCustomer(customer);
        //    if (!string.IsNullOrWhiteSpace(result.Item.Email))
        //    {
        //        string body = string.Empty;

        //        using (StreamReader reader = new StreamReader(System.Web.Hosting.HostingEnvironment.MapPath("~/EmailTemplate/Reg.html")))
        //        {
        //            body = reader.ReadToEnd();
        //        }
        //        body = body.Replace("#CustomerName#", result.Item.Name);
        //        EmailHelper.Send(result.Item.Email, "", "", "Welcome To SironaTeleMed", body);

        //        Email email = new Email();
        //        email.CustomerId =result.Item.Id;
        //        email.Text = "Registration";
        //        SuccessResult<AbstractEmail> res = emailServices.InsertUpdateEmail(email);
        //    }
        //    return this.Content(HttpStatusCode.OK, result);

        //}

        //[HttpGet]
        //[InheritedRoute("GetCustomerById/{id:int}")]
        //public async Task<IHttpActionResult> GetCustomerById([FromUri]int id)
        //{
        //    SuccessResult<AbstractCustomer> result = this.absCustomerServices.Select(id);

        //    return this.Content(HttpStatusCode.OK, result);
        //}

        //[HttpGet]
        //[InheritedRoute("GetCashbackTransactionByCustomerId/{id:int}")]
        //public async Task<IHttpActionResult> GetCashbackTransactionByCustomerId([FromUri]int id)
        //{
        ////    PageParam pageParam = new PageParam();
        ////    pageParam.Offset = 0;
        ////    pageParam.Limit = 50000;
        //    PagedList<AbstractCashbackTransaction> result = this.cashbackTransactionServices.CashbackTransactionByCustomerId( id);
        //    decimal totalBalance = 0;
        //    foreach(var item in result.Values)
        //    {
        //        totalBalance += item.Amount;                
        //    }

        //    foreach (var item in result.Values)
        //    {
        //        item.TotalBalance = totalBalance;
        //    }

        //    return this.Content(HttpStatusCode.OK, result);
        //}



        //[HttpGet]
        //[InheritedRoute("CustomerChangeStatus/{id:int}")]
        //public async Task<IHttpActionResult> CustomerChangeStatus([FromUri]int id)
        //{
        //    SuccessResult<AbstractCustomer> result = null;
        //    if (this.absCustomerServices.CustomerStatusChange(id))
        //    {
        //        result = this.absCustomerServices.Select(id);
        //    }

        //    return this.Content(HttpStatusCode.OK, result);
        //}

        //[HttpGet]
        //[InheritedRoute("CheckExistingUser")]
        //public async Task<IHttpActionResult> CheckExistingUser([FromUri]string Email = "", string Mobile = "",string ReferenceCode = "")
        //{
        //    SuccessResult<AbstractCustomer> result = this.absCustomerServices.CheckExistingUser(Email, Mobile,ReferenceCode);
        //    return this.Content(HttpStatusCode.OK, result);
        //}

        //[HttpPost]
        //[InheritedRoute("UpdateProfile")]
        //public async Task<IHttpActionResult> UpdateProfile([FromBody]Customer customer)
        //{
        //    SuccessResult<AbstractCustomer> result = this.absCustomerServices.InsertUpdateCustomer(customer);

        //    return this.Content(HttpStatusCode.OK, result);
        //}

        //[HttpGet]
        //[InheritedRoute("VerifyMobile")]
        //public async Task<IHttpActionResult> VerifyMobile([FromUri]string Mobile, string otp = "")
        //{
        //    string otpvalue = "";
        //    if (!string.IsNullOrWhiteSpace(otp))
        //    {
        //        otpvalue = otp;
        //    }
        //    else
        //    {
        //        Random generator = new Random();
        //        int r = generator.Next(1, 1000000);
        //        string s = r.ToString().PadLeft(6, '0');
        //        otpvalue = s;
        //    }
        //    string text = "Your One Time Password for SironaTeleMed is: " + otpvalue;
        //    string url = @"http://www.myvaluefirst.com/smpp/sendsms";
        //    string parameters = @"?username=cn12176&password=Admin@12176&to=" + Mobile + "&from=OPTNBY&text=" + HttpUtility.UrlEncode(text) + "&dlr-mask=19&dlr-url&coding=3";
        //    HttpClient client = new HttpClient();
        //    client.BaseAddress = new Uri(url);

        //    HttpResponseMessage response = client.GetAsync(parameters).Result;

        //    Message message = new Message();
        //    message.CustomerId = 0;
        //    message.Text = parameters;
        //    SuccessResult<AbstractMessage> Response = this.absmessageServices.InsertUpdateMessage(message);

        //    return this.Content(HttpStatusCode.OK, otpvalue);            
        //}

        //[HttpGet]
        //[InheritedRoute("LogIn")]
        //public async Task<IHttpActionResult> LogIn([FromUri]string Mobile, string Password,string devicetoken=null)
        //{
        //    SuccessResult<AbstractCustomer> result = this.absCustomerServices.LogIn(Mobile, Password, devicetoken);            
        //    return this.Content(HttpStatusCode.OK, result);
        //}

        //[HttpGet]
        //[InheritedRoute("UpdatePassword")]
        //public async Task<IHttpActionResult> UpdatePassword([FromUri] string Password, int CustId)
        //{
        //    SuccessResult<AbstractCustomer> result = this.absCustomerServices.UpdatePassword(Password, CustId);
        //    return this.Content(HttpStatusCode.OK, result);
        //}

        //[HttpGet]
        //[InheritedRoute("ForgotPassword")]
        //public async Task<IHttpActionResult> ForgotPassword([FromUri] string mobile, string password)
        //{
        //    SuccessResult<AbstractCustomer> result = this.absCustomerServices.ForgotPassword(mobile, password);
        //    return this.Content(HttpStatusCode.OK, result);
        //}

        //[HttpGet]
        //[InheritedRoute("NotificationSelectByCustomerId")]
        //public async Task<IHttpActionResult> NotificationSelectByCustomerId([FromUri]int CustId)
        //{
        //    PagedList<AbstractNotification> result = this.abstractNotificationService.SelectByCustomerId(CustId);

        //    foreach (var item in result.Values)
        //    {
        //        if(!string.IsNullOrWhiteSpace(item.redirectjson))
        //        {
        //            var json = new JavaScriptSerializer().Deserialize<RedirectJson>(item.redirectjson);

        //            item.Notificationtype = json.Type;
        //            item.ProductMainId = !string.IsNullOrWhiteSpace(json.ProductMainId) ? Convert.ToInt32(json.ProductMainId) : 0;
        //            if (item.Notificationtype.ToLower() == "offer")
        //            {
        //                item.OfferSlug = json.Id;
        //                if (!string.IsNullOrWhiteSpace(item.ImageURL))
        //                {
        //                    item.ImageURL = Configurations.S3BaseUrl + item.ImageURL;
        //                }
        //            }
        //            if (item.Notificationtype.ToLower() == "category")
        //            {
        //                item.CategoryId = !string.IsNullOrWhiteSpace(json.Id) ? Convert.ToInt32(json.Id) : 0;
        //                if (!string.IsNullOrWhiteSpace(item.ImageURL))
        //                {
        //                    item.ImageURL = Configurations.S3BaseUrl + item.ImageURL;
        //                }
        //            }
        //            if (item.Notificationtype.ToLower() == "product")
        //            {
        //                item.ProductId = !string.IsNullOrWhiteSpace(json.Id) ? Convert.ToInt32(json.Id) : 0;

        //                if (!string.IsNullOrWhiteSpace(item.ImageURL))
        //                {
        //                    item.ImageURL = Configurations.S3BaseUrl + item.ImageURL;
        //                }
        //            }
        //            if (item.Notificationtype.ToLower() == "order")
        //            {
        //                if (!string.IsNullOrWhiteSpace(item.ImageURL))
        //                {
        //                    item.ImageURL = Configurations.S3BaseUrl + item.ImageURL;
        //                }
        //            }
        //        }
        //    }
            

        //    return this.Content(HttpStatusCode.OK, result);
        //}

        //[HttpPost]
        //[InheritedRoute("InsertBankDetails")]
        //public async Task<IHttpActionResult> InsertBankDetails([FromBody]BankDetails bankDetails)
        //{
        //    CashbackTransaction cashbackTransaction = new CashbackTransaction();
        //    cashbackTransaction.Amount = Math.Floor(bankDetails.Amount) * -1;
        //    cashbackTransaction.OrderAmount = 0;
        //    cashbackTransaction.OrderId = 0;
        //    cashbackTransaction.Date = DateTime.Now;
        //    if (!string.IsNullOrWhiteSpace(bankDetails.IFSC))
        //    {
        //        cashbackTransaction.Note = "Send to Bank";
        //    }
        //    else
        //    {
        //        cashbackTransaction.Note = "Send to Paytm";
        //    }
        //    cashbackTransaction.CustomerId = bankDetails.CustomerId;
        //    cashbackTransaction.RefCustomerId = null;
        //    cashbackTransaction.Success = true;

        //    var objCashback = cashbackTransactionServices.InsertCashbackTransaction(cashbackTransaction);

        //    if (objCashback.IsSuccessStatusCode)
        //    {
        //        if (objCashback.Item.Id > 0)
        //        {
        //            bankDetails.CashbackTransactionId = objCashback.Item.Id;
        //        }

        //        bankDetails.Date = DateTime.Now;
        //        bankDetails.Status = "Pending";
        //        SuccessResult<AbstractBankDetails> result = this.absCustomerServices.InsertBankDetails(bankDetails);

        //        Customer cusData = new Customer();
        //        cusData.Id = bankDetails.CustomerId;
        //        cusData.CurrentBalance = Math.Floor(bankDetails.Amount);
        //        var cus = absCustomerServices.CurrentBalanceUpdate(cusData).Item;
        //    }
        //    return this.Content(HttpStatusCode.OK, objCashback);
        //}

        //[HttpGet]
        //[InheritedRoute("BankDetailsSelectByCashbackTranscationId")]
        //public async Task<IHttpActionResult> BankDetailsSelectByCashbackTranscationId([FromUri]int CashbackTranscationId)
        //{
        //    SuccessResult<AbstractBankDetails> result = this.absCustomerServices.BankDetailsSelectByCashbackTranscationId(CashbackTranscationId);
            
        //    return this.Content(HttpStatusCode.OK, result);
        //}

    }
}