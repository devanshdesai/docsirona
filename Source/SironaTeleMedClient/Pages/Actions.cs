﻿namespace SironaTeleMedClient.Pages
{
    public class Actions
    {       

        #region MyAccount Controller
        public const string UpdateUser = "UpdateUser";
        public const string Index = "Index";
        #endregion

        #region SearchCourses
        public const string BindSearchCourses = "BindSearchCourses";
        public const string ServiceDetails = "ServiceDetails";
        public const string CheckAvailability = "CheckAvailability";
        #endregion

        public const string Send = "Send";
        public const string ManagePayment = "ManagePayment";
        public const string Userdetails = "Userdetails";
        public const string UserListing = "UserListing";
        public const string ResetPSW = "ResetPSW";
        public const string ResetPassword = "ResetPassword";
        public const string AddCard = "AddCard";
        public const string RecentlyAddedCourses = "RecentlyAddedCourses";
        public const string _TestResult = "_TestResult";
        public const string BookNow = "BookNow";
        public const string _MyCourses = "_MyCourses";
        public const string ThankYou = "ThankYou";
        public const string UnassignTest = "UnassignTest";
        public const string Rating = "Rating";

        public const string _ResourceDetail = "_ResourceDetail";
        
        public const string BindResults = "BindResults";
        public const string Payment = "Payment";
        public const string Invoice = "Invoice";

        public const string StartCourse = "StartCourse";
        public const string EndCourse = "EndCourse";
        public const string SaveCourse = "SaveCourse";

        public const string SearchSaveCourse = "SearchSaveCourse";
        public const string SearchSaveResource = "SearchSaveResource";

        public const string AssignCourse = "AssignCourse";
        public const string AssignResource = "AssignResource";

        public const string UnassignCourse = "UnassignCourse";
        public const string UnassignResource = "UnassignResource";
        public const string Logout = "Logout";
        public const string IndividualTestResult = "IndividualTestResult";

        public const string Employee = "Employee";
        public const string AddEmployee = "AddEmployee";
        public const string EditEmployee = "EditEmployee";
        public const string DeleteEmployee = "DeleteEmployee";
        public const string GetUserDetailFromEmail = "GetUserDetailFromEmail";

        public const string Register = "Register";
        public const string ProcessPayment = "ProcessPayment";
        public const string CompletePayment= "CompletePayment";

        public const string LogIn = "LogIn";
        public const string ForgotPassword = "ForgotPassword";

         public const string CancelledSession = "CancelledSession";
        //public const string ResetPassword = "ResetPassword";

        public const string TakeQuizStart = "TakeQuizStart";
        public const string TakeQuiz = "TakeQuiz";
        public const string SubmitResult = "SubmitResult";

        public const string WebContact = "WebContact";

        public const string SubscribeCourse = "SubscribeCourse";
        public const string SubscribeResource = "SubscribeResource";
        public const string SubscribeEmail = "SubscribeEmail";
        public const string CourseDetail = "CourseDetail";
        public const string NewsDetail = "NewsDetail";

        public const string SendEmail = "SendEmail";              
        public const string UserCourses = "UserCourses";

        public const string AssignedCourses = "AssignedCourses";
        public const string AssignedResources = "AssignedResources";
        public const string AssignedTests = "AssignedTests";
        public const string Documents = "Documents";
        public const string AddDocuments = "AddDocuments";
        public const string EditDocuments = "EditDocuments";
        public const string DeleteDocuments = "DeleteDocuments";
        public const string Download = "Download";

        public const string SearchTest = "SearchTest";
        public const string DeleteInstitute = "DeleteInstitute";
        public const string StartTest = "StartTest";
        public const string AssignTest = "AssignTest";

        public const string SubscribeTest = "SubscribeTest";
        public const string Search = "Search";
        public const string UserResources = "UserResources";
        public const string UserTests = "UserTests";

        public const string SearchPartial = "SearchPartial";
        public const string _SearchResourcesPartial = "_SearchResourcesPartial";
        public const string _SearchTestPartial = "_SearchTestPartial";
        public const string Upgrade = "Upgrade";
        public const string SaveImage = "SaveImage";

        public const string DeleteCard = "DeleteCard";
        public const string SetCardAsDefault = "SetCardAsDefault";
        public const string _CardsPartial = "_CardsPartial";

        public const string RequestClass = "RequestClass";
        public const string _ClassPartial = "_ClassPartial";

        public const string RequestInstitute = "RequestInstitute";
        public const string RequestDetail = "RequestDetail";
        public const string ApproveRequest = "ApproveRequest";
        public const string _SearchInstitutePartial = "_SearchInstitutePartial";


        public const string UpcomingSessions = "UpcomingSessions";
        public const string CompletedSessions  = "CompletedSessions";
        public const string UserDetails = "UserDetails";
        public const string SessionUpdateByStatusId = "SessionUpdateByStatusId";
        public const string AddUserDetails = "AddUserDetails";

        public const string RegisterUser = "RegisterUser";
        public const string GuestRegisterUser = "GuestRegisterUser";

        public const string Search1 = "Search1";
        public const string Search2 = "Search2";

        public const string UserProfile = "UserProfile";
        public const string UpdateProfile = "UpdateProfile";

        public const string AccountSetting = "AccountSetting";

        public const string Header = "Header";

        public const string BindPatientHistory = "BindPatientHistory";

        public const string DoctorStatusUpdate = "DoctorStatusUpdate";

        #region Appointment
        public const string SearchAppointments = "SearchAppointments";
        public const string BookAppointments = "BookAppointments";
        public const string DoctorAppointment = "DoctorAppointment";
        public const string AddBookAppointments = "AddBookAppointments";
        public const string MyAppointments = "MyAppointments";
        public const string DoctorsAppointments = "DoctorsAppointments";
        public const string CompletedAppointments = "CompletedAppointments";
        public const string BindCompletedAppointments = "BindCompletedAppointments";
        public const string BindUpcomingAppointments = "BindUpcomingAppointments";
        public const string BindNewAppointments = "BindNewAppointments";
        public const string BindCreateAppointment = "BindCreateAppointment";
        public const string BindRejectedAppointments = "BindRejectedAppointments";
        public const string Rejectedintments = "Rejectedintments";
        public const string UpcomingAppointments = "UpcomingAppointments";
        public const string NewAppointments = "NewAppointments";
        public const string CreateAppointment = "CreateAppointment";

        public const string PatientsHistory = "PatientsHistory";
        public const string ManageDoctors = "ManageDoctors";
        public const string ManagePatients = "ManagePatients";
        public const string BindDoctors = "BindDoctors";
        public const string BindPatients = "BindPatients";
        public const string BindDashNewAppointments = "BindDashNewAppointments";
        public const string StartCall = "StartCall";
        public const string JoinCall = "JoinCall";
        public const string GuestRegister = "GuestRegister";
        public const string ViewCompletedAppointment = "ViewCompletedAppointment";
        #endregion
    }
}