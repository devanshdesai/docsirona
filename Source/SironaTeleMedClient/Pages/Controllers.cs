﻿namespace SironaTeleMedClient.Pages
{
    public static class Controllers
    {
        public const string Home = "Home";
        public const string MyAccount = "MyAccount";
        public const string PatientsHistory = "PatientsHistory";
        public const string Payment = "Payment";
        public const string Messages = "Messages";
        public const string Notification = "Notification";
        public const string Search = "Search";                                               
        public const string Logout = "Logout";
        public const string SignUp = "SignUp";
        public const string Dashboard = "Dashboard";
        public const string Login = "Login";
        public const string ContactUs = "ContactUs";
        public const string UserListing = "UserListing";                                                            
        public const string Register = "Register";
        public const string Appointment = "Appointment";
        public const string ManagePayment = "ManagePayment";
        public const string StripePayment = "StripePayment";

        public static string[] ConsumerAccess = { Search, Dashboard, Home, Appointment, MyAccount,
                                                  Notification,ManagePayment,StripePayment };
        public static string[] ProviderAccess = { Search, Dashboard, Home, Appointment, MyAccount,
                                                  Notification};

        public static string[] AdminAccess = { Search, Dashboard, Home, Appointment, MyAccount,
                                                  Notification, UserListing};

    }
}