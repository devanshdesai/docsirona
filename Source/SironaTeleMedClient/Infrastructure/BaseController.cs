﻿using SironaTeleMed.Common;
using SironaTeleMed.Common.Paging;
using SironaTeleMedClient.Pages;
using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Twilio;
using Twilio.Rest.Api.V2010.Account;

namespace SironaTeleMedClient.Infrastructure
{
    public class BaseController : Controller
    {
        private string UserId;
        private string SubscriptionId;
        private string InstituteId;
        private string Email;
        private string UserName;
        private string UserType;

        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);
        }

        ///// <summary>
        ///// Gets the page parameter.
        ///// </summary>
        ///// <param name="requestModel">The request model.</param>
        ///// <returns></returns>
        //public PageParam GetPageParam(IDataTablesRequest requestModel)
        //{
        //    PageParam pageParam = new PageParam();
        //    pageParam.Offset = requestModel.Start;
        //    pageParam.Limit = requestModel.Length;
        //    pageParam.SortBy = requestModel.Columns.ElementAt(requestModel.OrderColumn).Data;            
        //    return pageParam;
        //}

        private bool IsAllowed(string controllerName, string[] accessControllers)
        {
            foreach (string accessController in accessControllers)
            {
                if (accessController.ToLower() == controllerName)
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Called by the ASP.NET MVC framework before the action method executes.
        /// </summary>
        /// <param name="filterContext">The filter context.</param>
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpCookie reqCookie = Request.Cookies["AdminUserLogin_Client"];
            try
            {
                if (reqCookie == null)
                {
                    if (filterContext.HttpContext.Request.RawUrl.Length > 2)
                    {
                        filterContext.Result = new RedirectResult("~/Login?ReturnUrl=" + filterContext.HttpContext.Request.Url);
                    }
                    else
                    {
                        filterContext.Result = new RedirectResult("~/Login");
                    }
                    return;
                }

                Email = reqCookie["Email"].ToString();
                UserName = reqCookie["UserName"].ToString();
                UserId = reqCookie["SironaTeleMedUserId"].ToString();
                UserType = reqCookie["UserType"].ToString();

                ProjectSession.Email = Email;
                ProjectSession.UserName = UserName;
                ProjectSession.UserProfilePicture = reqCookie["ProfileUrl"].ToString();
                ProjectSession.UserID = Convert.ToInt32(UserId);
                ProjectSession.IsLogin = true;
                ProjectSession.UserType = Convert.ToInt32(UserType);
                ProjectSession.UserTypeStr = reqCookie["UserTypeStr"].ToString();

                if (ProjectSession.UserID == null || ProjectSession.UserID == 0)
                {
                    filterContext.Result = new RedirectResult("~/Home");
                    return;
                }
                
                
               
                //ProjectSession.DoctorStatus = Convert.ToInt32(reqCookie["IsActive"]);

                

                bool isAllow = true;

                if (ProjectSession.UserType == (int)UserTypeEnum.Admin &&
                    IsAllowed(filterContext.ActionDescriptor.ControllerDescriptor.ControllerName.ToLower(),
                    Pages.Controllers.AdminAccess
                    ))
                {
                    isAllow = true;
                }

                if (ProjectSession.UserType == (int)UserTypeEnum.Consumer &&
                    IsAllowed(filterContext.ActionDescriptor.ControllerDescriptor.ControllerName.ToLower(),
                    Pages.Controllers.ConsumerAccess
                    ))
                {
                    isAllow = true;
                }


                if (ProjectSession.UserType == (int)UserTypeEnum.Provider &&
                   IsAllowed(filterContext.ActionDescriptor.ControllerDescriptor.ControllerName.ToLower(),
                   Pages.Controllers.ProviderAccess
                   ))
                {
                    isAllow = true;
                }


                if((ProjectSession.UserType == (int)UserTypeEnum.Consumer &&
                   filterContext.ActionDescriptor.ActionName.ToLower() == "opd") || (ProjectSession.UserType == (int)UserTypeEnum.Consumer &&
                   filterContext.ActionDescriptor.ActionName.ToLower() == "newappointments"))
                {
                    isAllow = false;
                }

                if ((ProjectSession.UserType == (int)UserTypeEnum.Provider &&
                   filterContext.ActionDescriptor.ActionName.ToLower() == "opd") || (ProjectSession.UserType == (int)UserTypeEnum.Provider &&
                   filterContext.ActionDescriptor.ActionName.ToLower() == "newappointments"))
                {
                    isAllow = false;
                }



                if (!isAllow)
                {
                    filterContext.Result = new RedirectResult("~/Login");
                    return;
                }

                if(filterContext.ActionDescriptor.ControllerDescriptor.ControllerName.ToLower() == "dashboard")
                {
                    ViewBag.DashboardHighlight = "active";
                }
                else
                {
                    ViewBag.DashboardHighlight = "";
                }

                ////base.OnActionExecuting(filterContext);
                base.OnActionExecuting(filterContext);
                return;

            }
            catch (Exception ex)
            {
                ////ErrorLogHelper.Log(ex);
                throw ex;
            }
        }

        public void SendTextMessage(string text,string too,string from)
        {
            try
            {
                TwilioClient.Init(Configurations.AccountSid, Configurations.AuthToken);
                var message = MessageResource.Create(
                 body: text,
                 to: new Twilio.Types.PhoneNumber(too),
                 from: new Twilio.Types.PhoneNumber(Configurations.TwilioFromMsgNumber)
               );
            }
            catch(Exception ex)
            {

            }
        }
    }
}