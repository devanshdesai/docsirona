﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SironaTeleMedClient.Infrastructure
{
    public class Messages
    {
        #region General
        public static string AccessDenied = "You are not authorized to access this page.";
        public static string AccessDeniedContactAdmin = "If you require to access this page, Please contact the system administrator.";
        public static string CommonErrorMessage = "Something went wrong, Please try again";
        public static string ConfirmDelete = "Are you sure you want to delete this Record?";
        public static string ContactToAdmin = "An error occurred on the system. Please contact the administrator.";
        public static string RecordSavedSuccessfully = "Record saved successfully.";
        public static string RecordDeletedSuccessfully = "Record deleted successfully.";
        public static string RecordNotDeleted = "Record is not deleted successfully because it refers to another entity.";
        public static string ConfirmActivate = "Are you sure you want to activate this Record?";
        public static string ConfirmDectivate = "Are you sure you want to deactivate this Record?";
        public static string RecordActivatedSuccessfully = "Record activated successfully.";
        public static string RecordDeactivatedSuccessfully = "Record deactivated successfully.";
        public static string RecordActivationFailed = "Record activation/deactivation failed.";

        public static string SaveCourse = "Course saved successfully.";
        public static string SaveResource = "Resource saved successfully.";

        public static string AssignCourse = "Course assigned successfully.";
        public static string AssignResource = "Resource assigned successfully.";
        public static string AssignTest = "Test assigned successfully.";

        public static string UnassignTest = "Test unassigned successfully.";
        public static string UnassignCourse = "Course unassigned successfully.";
        public static string UnassignResource = "Resource unassigned successfully.";
        public static string UserUpgrade = "User upgraded successfully.";
        public static string RequestPlaced = "Request placed successfully.";
        public static string RequestApproved = "Request approved successfully.";


        public static string WelcomeText = "<p style='margin: 0 0 20px;font-size: 16px;line-height: 1.4;'>Welcome to HIPO Insights, our tool analyses your goals to help you understand if your goal is SMART (Specific, Measurable, Achievable, Realistic & Time-Bound).</p>";
        public static string EmployeeDetailsUploaded(int total = 0, int success = 0)
        {
            return string.Format("{0} of {1} user's has been created successfully", success, total);
        }

        public static string ConfirmEmployee = "This will charge you $";

        public static string ConfirmPayment = "Are you sure you want to pay?";

        public static string PasswordUnmatch = "Old Password does't match.";
        #endregion

        #region LogIn
        public static string InValidClientCode = "Please Enter valid Client Code.";
        public static string InValidCredential = "Please Enter valid UserName or Password.";
        public static string InActiveAccount = "Your Account is not active. Please contact to administrator.";
        public static string ClientConfigInvalid = "Client is not Configured properly. Please contact to administrator.";
        public static string Mailsend = "Instructions on how to reset your password have been sent to your email account.";
        public static string InvalidEmail = "We couldn't find a SironaTeleMed account associated with email address provided.";
        public static string PasswordReSet = "Password change successfully.";
        #endregion

        #region DuplicateRecord
        public static string MeasurableExist = "Measurable with this name already Exist.";
        public static string TimeBoundExist = "Timebound with this name already Exist.";
        public static string ActionableExist = "Actionable with this name already Exist.";
        public static string SpecificExist = "Specific with same name already exists.";
        public static string DepartmentExist = "Department with this name already Exist.";
        public static string DepartmentCodeExist = "Department with this code already Exist.";
        public static string EmployeeEmailExists = "Employee email already exists.";
        public static string EmailExists = "Email already exists.";
        public static string ClientExists = "Client already exists with same email or clientcode.";
        public static string GoalTypeExist = "Goal Type with this name already Exist.";
        public static string PaymentModeExist = "Payment Mode with this name already Exist.";
        public static string PackageExist = "Package with this name already Exist.";
        public static string ReceivedPaymentExist = "Received Payment with this name already Exist.";
        public static string PartyExist = "Party with this name already Exist.";
        public static string PartyTypeExist = "Party Type with this name already Exist.";
        public static string ShopExist = "Shop with this name already Exist.";
        public static string ItemExist = "Item with this name already Exist."; 
        public static string IndustryExist = "Industry with this name already Exist.";
        public static string SalesmanExist = "Salesman with this name already Exist.";
        public static string LicenseInformationExist = "License Information with this name already Exist.";
        #endregion

        #region Goal
        public static string SelectUserForEmail = "Please select user's to send an email.";
        public static string GoalStatusSendToUser = "Goal Status mail has been sent successfully.";
        public static string GoalDetailsUploaded(int total = 0, int success = 0) {
            return string.Format("{0} of {1} goals has been uploaded successfully.", success, total);
        }
        public static string GoalDetailsUpdated = "Goal details has been updated successfully.";
        public static string InvalidGoalsheetUploaded = "Please select valid data sheet having goals data.";
        public static string InvalidGoalDetails = "Pelase select valid Goal details";

        #endregion

        #region Permission
        public static string RolePermissionsSaved = "Permissions for this Role saved successfully.";
        public static string RolePermissionsFailed = "Permissions for this Role failed to save, Please try again.";
        #endregion

        #region Api

        public static string InvalidApiCredential = "Please enter valid Api Credentials.";
        public static string InvalidApiKey = "Please enter valid Api key.";
        public static string InvalidClientCode = "Please enter valid Client Code.";
        public static string GoalUploaded = "Goal detail has been updated successfully.";
        public static string InvalidGoalsExist = "There are some Invalid Goals please correct out it before upload it again.";

        #endregion Api

    }
}