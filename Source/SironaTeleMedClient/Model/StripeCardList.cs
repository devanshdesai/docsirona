﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SironaTeleMedClient.Model
{
    public class StripeCardList
    {
        public string AddressZip { get; set; }
        public string Brand { get; set; }
        public string ExpMonth { get; set; }
        public string ExpYear { get; set; }
        public string Id { get; set; }
        public string Last4 { get; set; }
        public string DefaultCCId { get; set; }
    }

    public class StripeListCard
    {
        public List<StripeCardList> stripeCardLists { get; set; }
    }
}