﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SironaTeleMed.Common;
using SironaTeleMed.Entities.Contract;
using SironaTeleMed.Services.Contract;
using SironaTeleMedClient.Infrastructure;
using SironaTeleMedClient.Model;
using SironaTeleMedClient.Pages;
using Stripe;
using static SironaTeleMedClient.Infrastructure.Enums;

namespace SironaTeleMedClient.Controllers
{
    public class StripePaymentController : BaseController
    {
        private readonly AbstractUsersService usersService;

        public StripePaymentController(AbstractUsersService usersService)
        {
            this.usersService = usersService;
        }

        // GET: StripePayment
        public ActionResult Index()
        {
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddCard(string stripeToken)
        {
            try
            {
                AbstractUsers users = usersService.UsersSelect(ProjectSession.UserID).Item;
                StripeConfiguration.ApiKey = Configurations.StripeSecretKey;

                var options = new CustomerCreateOptions { Email = users.Email, Name = users.FullName, Description = "Customer for " + users.Email, Source = stripeToken };
                var service = new Stripe.CustomerService();

                Customer customer = service.Create(options);
                users.StripeId = customer.Id;
                usersService.InsertUpdateUsers(users);

                ProjectSession.StripeId = customer.Id;
                return RedirectToAction(Actions.Index, Pages.Controllers.Dashboard, new { Area = "" });
            }
            catch (Exception ex)
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), ex.Message.ToString());
                return RedirectToAction(Actions.Index, Pages.Controllers.StripePayment, new { Area = "" });
            }
        }

        // GET: StripePayment
        public ActionResult AddAnotherCard()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddAnotherCard(string stripeToken)
        {
            try
            {
                AbstractUsers users = usersService.UsersSelect(ProjectSession.UserID).Item;
                StripeConfiguration.ApiKey = Configurations.StripeSecretKey;

                var cardoptions = new CardCreateOptions
                {
                    Source = stripeToken
                };

                var cardService = new CardService();
                cardService.Create(ProjectSession.StripeId, cardoptions);

                return RedirectToAction("ManageCard", Pages.Controllers.StripePayment, new { Area = "" });
            }
            catch (Exception ex)
            {
                TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), ex.Message.ToString());
                return RedirectToAction(Actions.Index, Pages.Controllers.StripePayment, new { Area = "" });
            }
        }

        public ActionResult ManageCard()
        {
            StripeConfiguration.ApiKey = Configurations.StripeSecretKey;

            var cardservice = new CardService();
            var options = new CardListOptions { Limit = 1000 };
            StripeList<Card> cards = cardservice.List(ProjectSession.StripeId, options);

            var customerservice = new CustomerService();
            Customer customer = customerservice.Get(ProjectSession.StripeId);
            ViewBag.DefaultSourceId = customer.DefaultSourceId;

            StripeListCard stripeCardList = new StripeListCard();
            List<StripeCardList> lststripeCardLists = new List<StripeCardList>();
            foreach (var item in cards)
            {
                if (item.Id == customer.DefaultSourceId)
                {
                    lststripeCardLists.Add(new StripeCardList
                    {
                        AddressZip = item.AddressZip,
                        Brand = item.Brand,
                        ExpMonth = item.ExpMonth.ToString(),
                        ExpYear = item.ExpYear.ToString(),
                        Id = item.Id,
                        Last4 = item.Last4,
                        DefaultCCId = "Default"
                    });
                }
                else
                {
                    lststripeCardLists.Add(new StripeCardList
                    {
                        AddressZip = item.AddressZip,
                        Brand = item.Brand,
                        ExpMonth = item.ExpMonth.ToString(),
                        ExpYear = item.ExpYear.ToString(),
                        Id = item.Id,
                        Last4 = item.Last4,
                        DefaultCCId = string.Empty
                    });
                }
                stripeCardList.stripeCardLists = lststripeCardLists;
            }

            return View(stripeCardList);
        }

        [HttpPost]
        public ActionResult SetDefaultCard(string CardId)
        {
            StripeConfiguration.ApiKey = Configurations.StripeSecretKey;
            var options = new CustomerUpdateOptions
            {
                DefaultSource = CardId
            };

            var service = new CustomerService();
            service.Update(ProjectSession.StripeId, options);

            return Json(new object[] { }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Subscriptions()
        {
            StripeConfiguration.ApiKey = Configurations.StripeSecretKey;
            var customerService = new CustomerService();
            var customer = customerService.Get(ProjectSession.StripeId);
            StripeList<Subscription> subscription = customer.Subscriptions;

            var service = new ChargeService();
            var options = new ChargeListOptions
            {
                Limit = 500,
                CustomerId = ProjectSession.StripeId
            };
            StripeList<Charge> charges = service.List(options);

            return View();
        }
    }
}