﻿namespace SironaTeleMedClient.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using SironaTeleMed.Entities.V1;
    using SironaTeleMed.Common;
    using System.IO;
    using SironaTeleMed.Services.Contract;
    using SironaTeleMed.Entities.Contract;
    using SironaTeleMedClient.Pages;
    using SironaTeleMedClient.Infrastructure;

    public class NotificationController : BaseController
    {
        #region Fields

        //private readonly AbstractNotificationService notificationService;
        
        #endregion

        public NotificationController()
        {
            //this.notificationService = notificationService;           
        }

        // GET: Notification
        public ActionResult Index()
        {           
                   
           // var model = notificationService.SelectByNotificationTo(ProjectSession.UserID);
            return View();
        }
        public ActionResult appointment()
        {
            return View();
        }
        public ActionResult welcome()
        {
            return View();
        }
        public ActionResult setup()
        {
            return View();
        }

        public ActionResult rating()
        {
            return View();
        }
    }
}