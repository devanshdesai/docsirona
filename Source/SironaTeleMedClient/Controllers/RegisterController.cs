﻿using System;
using System.Web.Mvc;
using DataTables.Mvc;
using SironaTeleMed.Entities.V1;
using SironaTeleMed.Services.Contract;
using SironaTeleMedClient.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Square.Connect.Api;
using Square.Connect.Client;
using Square.Connect.Model;
using SironaTeleMedClient.Infrastructure;
using SironaTeleMed.Common;
using SironaTeleMed.Entities.Contract;
using System.IO;
using SironaTeleMed.Common.Paging;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Stripe;

namespace SironaTeleMedClient.Controllers
{
    public class RegisterController : Controller
    {
        #region NewCode        
        private readonly AbstractUsersService usersService;
        private readonly AbstractMasterCountryServices masterCountryServices;
        private readonly AbstractMasterStateService masterStateServices;
        private readonly AbstractSessionScheduleServices sessionsScheduleService;
        private readonly AbstractSessionsServices sessionsService;

        public RegisterController(AbstractUsersService usersService, AbstractMasterCountryServices masterCountryServices, AbstractMasterStateService masterStateServices, AbstractSessionScheduleServices sessionsScheduleService, AbstractSessionsServices sessionsService)
        {
            this.usersService = usersService;
            this.masterCountryServices = masterCountryServices;
            this.masterStateServices = masterStateServices;
            this.sessionsScheduleService = sessionsScheduleService;
            this.sessionsService = sessionsService;
        }

        public ActionResult Index(string err = "", int? plan = null, string ReturnUrl = "")
        {
            ViewBag.ReturnUrl = ReturnUrl;
            ViewBag.Category = null;
            ViewBag.ErrorMessage = err;
            ViewBag.PageTitleName = "Register";
            if (plan != null)
            {
                ViewBag.Plan = plan;
            }
            else
            {
                ViewBag.Plan = 0;
            }
            ViewBag.Country = BindCountryDropdown();
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem() { Text = "Select State", Value = "0" });
            ViewBag.State = GetState(0,0);
            return View();
        }

        public IList<SelectListItem> BindStateDropdown()
        {
            PageParam pageParam = new PageParam();
            var model = masterStateServices.MasterStateSelectAll(pageParam);

            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem() { Text = "Select State", Value = "0" });

            foreach (var masterStateServices in model.Values)
            {
                items.Add(new SelectListItem() { Text = masterStateServices.State.ToString(), Value = masterStateServices.Id.ToString() });
            }

            return items;
        }

        public IList<SelectListItem> BindCountryDropdown()
        {
            PageParam pageParam = new PageParam();
            var model = masterCountryServices.MasterCountrySelectAll(pageParam);

            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem() { Text = "Select Country", Value = "0" });

            foreach (var masterCountryServices in model.Values)
            {
                items.Add(new SelectListItem() { Text = masterCountryServices.Country.ToString(), Value = masterCountryServices.Id.ToString() });
            }

            return items;
        }

        [HttpPost]
        public ActionResult RegisterUser(Users users)
        {
            users.UserType = 2;
            var result = usersService.InsertUpdateUsers(users);

            if (result.Code == 400)
            {
                return RedirectToAction(Actions.Index, Pages.Controllers.Register, new { err = result.Message });
            }
            else if (result.Code == 200)
            {
                ProjectSession.UserID = result.Item.Id;
                string Email = result.Item.Email;               
                if (Email != "")
                {
                    string body = string.Empty;

                    using (StreamReader reader = new StreamReader(Server.MapPath("~/EmailTemplates/Telemed.html")))
                    {
                        body = reader.ReadToEnd();
                    }
                    body = body.Replace("#titlemsg1", "Welcome,");
                    body = body.Replace("#titlemsg2", "to Sirona Tele Med !");                    
                    body = body.Replace("#name", result.Item.FirstName+" "+result.Item.LastName);
                    body = body.Replace("#mainmsg", "Thank you for signing up. You can now log into your account and book an appointment");
                    EmailHelper.Send(Email, "", "", "Welcome To Sirona Tele Med", body);
                }
                return RedirectToAction(Actions.Index, Pages.Controllers.Login, new { suc = result.Message });

            }
            else
            {
                return RedirectToAction(Actions.Index, Pages.Controllers.Login);
            }
        }

        [HttpPost]
        [ActionName(Actions.Search1)]
        public JsonResult Search1(int Id)
        {
            try
            {
                return Json(new object[] { Enums.MessageType.success.GetHashCode(), Enums.MessageType.success.ToString(), GetState(Id) }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new object[] { Enums.MessageType.danger.GetHashCode(), Enums.MessageType.danger.ToString(), Messages.ContactToAdmin }, JsonRequestBehavior.AllowGet);
            }
        }

        public IList<SelectListItem> GetState(int id, int Sid = 0)
        {
            PageParam pageParam = new PageParam();

            List<AbstractMasterState> result = masterStateServices.MasterStateSelectAll(pageParam).Values.ToList();
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem() { Text = "Select State"});
            if (result.Count > 0)
            {
                //result = result.Where(x => x.CountryId == id).ToList();
                foreach (var state in result)
                {
                    if (Sid != 0 && Sid == state.Id)
                    {
                        items.Add(new SelectListItem() { Text = state.State.ToString(), Value = state.Id.ToString(), Selected = true });
                    }
                    else
                    {
                        items.Add(new SelectListItem() { Text = state.State.ToString(), Value = state.Id.ToString() });
                    }
                }
            }
            return items;
        }

        /// <summary>
        /// Guest Register
        /// </summary>
        /// <returns></returns>
        public ActionResult GuestRegister(int SSid=0,string ReturnUrl="",string err = "", int? plan = null)
        {
            ViewBag.ReturnUrl = ReturnUrl;
            Users users = new Users();
            ViewBag.Category = null;
            ViewBag.ErrorMessage = err;
            ViewBag.PageTitleName = "Guest Sign Up";
            if (plan != null)
            {
                ViewBag.Plan = plan;
            }
            else
            {
                ViewBag.Plan = 0;
            }
            ViewBag.Country = BindCountryDropdown();
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem() { Text = "Select State", Value = "0" });
            ViewBag.State = GetState(0,0);
            users.SessionScheduleId = SSid;
            var sessionschedule = sessionsScheduleService.SessionScheduleSelect(SSid);
            if(sessionschedule.Item != null)
            {
                users.SessionStart = sessionschedule.Item.SessionStart;
            }            
            return View(users);
        }

        [HttpPost]
        public ActionResult GuestRegisterUser(Users users) //, string stripeToken
        {
            try
            {
                StripeConfiguration.ApiKey = Configurations.StripeSecretKey;

                var options = new CustomerCreateOptions { Email = users.Email, Name = users.FullName, Description = "Customer for " + users.Email, Source = users.StripeToken };
                var service = new Stripe.CustomerService();
                Stripe.Customer customer = service.Create(options);
                users.StripeId = customer.Id;
                ProjectSession.StripeId = customer.Id;
                users.UserType = 4;
                var result = usersService.GuestInsert(users);

                if (result.Code == 400)
                {
                    return RedirectToAction(Actions.Index, Pages.Controllers.Login, new { suc = result.Message });
                }
                else if (result.Code == 200)
                {
                    ProjectSession.UserID = result.Item.Id;
                    string Email = result.Item.Email;

                    AbstractSessions sessions = new Sessions();
                    sessions.CreatedBy = ProjectSession.UserID;
                    sessions.UserId = ProjectSession.UserID;
                    sessions.UserDescription = users.UserDescription;
                    sessions.SpecialRequests = users.SpecialRequests;
                    sessions.SessionScheduleId = users.SessionScheduleId;
                    sessions.HasInsurance = users.HasInsurance;
                    sessions.InsuranceCompanyName = users.InsuranceCompanyName;
                    sessions.InsuranceCompanyContact = users.InsuranceCompanyContact;
                    sessions.InsuranceId = users.InsuranceId;
                    sessions.InsuranceType = users.InsuranceType;
                    sessions.GroupNumber = users.GroupNumber;
                    var resultSession = sessionsService.InsertUpdateSessions(sessions);
                    if (Email != "")
                    {
                        string body = string.Empty;

                        using (StreamReader reader = new StreamReader(Server.MapPath("~/EmailTemplates/Telemed.html")))
                        {
                            body = reader.ReadToEnd();
                        }
                        body = body.Replace("#titlemsg1", "Welcome,");
                        body = body.Replace("#titlemsg2", "to Sirona Tele Med !");
                        body = body.Replace("#name", result.Item.FirstName + " " + result.Item.LastName);
                        body = body.Replace("#mainmsg", "Your appointment for " + resultSession.Item.DisplayDate + " at " + resultSession.Item.DisplayTime + " has been successfully created and a doctor shall soon be assigned");
                        EmailHelper.Send(Email, "", "", "Welcome To Sirona Tele Med", body);
                        SendTextMessage("Your appointment for " + resultSession.Item.DisplayDate + " at " + resultSession.Item.DisplayTime + " has been successfully created and a doctor shall soon be assigned", result.Item.Phone, "");

                        #region Charge from Stripe
                        var planService = new PlanService();
                        var planOptions = new PlanListOptions
                        {
                            ProductId = Configurations.ProductId
                        };
                        var planList = planService.List(planOptions).FirstOrDefault();

                        var customerService = new CustomerService();
                        var sourceId = customerService.Get(ProjectSession.StripeId).DefaultSourceId;

                        var items = new List<SubscriptionItemOption> {
                        new SubscriptionItemOption {
                            PlanId = planList.Id
                           }
                        };
                        var options1 = new SubscriptionCreateOptions
                        {
                            CustomerId = ProjectSession.StripeId,
                            Items = items
                        };
                        var subscriptionService = new SubscriptionService();
                        Subscription subscription = subscriptionService.Create(options1);

                        var invoiceService = new InvoiceService();
                        var invoice = invoiceService.Get(subscription.LatestInvoiceId);

                        var paymentIntentService = new PaymentIntentService();

                        var paymentintentUpdate = new PaymentIntentUpdateOptions
                        {
                            Description = "Your appointment for " + resultSession.Item.DisplayDate + " at " + resultSession.Item.DisplayTime + " has been successfully created and a doctor shall soon be assigned"
                        };

                        paymentIntentService.Update(invoice.PaymentIntentId, paymentintentUpdate);
                        #endregion                        
                    }
                    return RedirectToAction(Actions.Index, Pages.Controllers.Login, new { suc = "Your appointment has been registered successfully. You shall be notified on your registered email once the appointment starts !" });
                }
                else
                {
                    return RedirectToAction(Actions.Index, Pages.Controllers.Login);
                }
            }
            catch(Exception ex)
            {
                //"There was an error registering your appoinment ! Please try again."
                return RedirectToAction(Actions.Index, Pages.Controllers.Login, new { suc = ex.InnerException+"--->"+ex.Message });
            }            
        }

        public void SendTextMessage(string text, string too, string from)
        {
            try
            {
                TwilioClient.Init(Configurations.AccountSid, Configurations.AuthToken);
                var message = MessageResource.Create(
                 body: text,
                 to: new Twilio.Types.PhoneNumber(too),
                 from: new Twilio.Types.PhoneNumber(Configurations.TwilioFromMsgNumber)
               );
            }
            catch (Exception ex)
            {

            }
        }

        [HttpGet]
        public ActionResult JoinCall(string id)
        {
            ViewBag.LinkableId = Configurations.VideoCall + Convert.ToString(ConvertTo.Base64Decode(id));
            var result = sessionsService.SessionsSelect(Convert.ToInt32(ConvertTo.Base64Decode(id)));
            ViewBag.UserName = result.Item.PatientName;
            return View();
        }

        #endregion
    }
}