﻿
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using SironaTeleMed.Entities.V1;
    using SironaTeleMed.Common;
    using System.IO;
    using SironaTeleMed.Services.Contract;
    using SironaTeleMed.Entities.Contract;
    using SironaTeleMedClient.Pages;
    using SironaTeleMedClient.Infrastructure;
    using Square.Connect.Api;
    using Square.Connect.Model;
    using SironaTeleMed.Common.Paging;

namespace SironaTeleMedClient.Controllers
{
    public class MyAccountController : BaseController
    {
        #region OldCode

       
        //// GET: MyAccount
        private readonly AbstractUsersService usersService;
        private readonly AbstractMasterCountryServices masterCountryServices;
        private readonly AbstractMasterStateService masterStateServices;
     

        public MyAccountController(AbstractUsersService usersService, AbstractMasterCountryServices masterCountryServices, AbstractMasterStateService masterStateServices)
        {
            this.usersService = usersService;
            this.masterCountryServices = masterCountryServices;
            this.masterStateServices = masterStateServices;
        }

        public ActionResult Index()
        {
            ViewBag.Category = null;
            ViewBag.PageTitleName = "Profile";
            
            ViewBag.Country = BindCountryDropdown();
            //ViewBag.State = BindStateDropdown();

            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem() { Text = "Select State", Value = "0" });
            ViewBag.State = GetState(0, 0);

            AbstractUsers users = usersService.UsersSelect(ProjectSession.UserID).Item;
            if(users.Id >0)
            {
                ViewBag.State = GetState(0,0);
            }


            users.Password = string.Empty;
            return View(users);
        }

        public IList<SelectListItem> BindStateDropdown()
        {
            PageParam pageParam = new PageParam();
            var model = masterStateServices.MasterStateSelectAll(pageParam);

            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem() { Text = "Select State", Value = "0" });

            foreach (var masterStateServices in model.Values)
            {
                items.Add(new SelectListItem() { Text = masterStateServices.State.ToString(), Value = masterStateServices.Id.ToString() });
            }

            return items;
        }

        public IList<SelectListItem> BindCountryDropdown()
        {
            PageParam pageParam = new PageParam();
            var model = masterCountryServices.MasterCountrySelectAll(pageParam);

            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem() { Text = "Select Country", Value = "0" });

            foreach (var masterCountryServices in model.Values)
            {
                items.Add(new SelectListItem() { Text = masterCountryServices.Country.ToString(), Value = masterCountryServices.Id.ToString() });
            }

            return items;
        }

      
        

        [HttpPost]
        public ActionResult UpdateProfile(Users users)
        {          
           var result = usersService.InsertUpdateUsers(users);
            ProjectSession.UserName = users.FirstName + " " + users.LastName + " ( " + ProjectSession.UserTypeStr + " )"; 
           return RedirectToAction(Actions.Index, Pages.Controllers.Dashboard);            
        }

        [HttpPost]
        [ActionName(Actions.Search1)]
        public JsonResult Search1(int Id)
        {
            try
            {
                return Json(new object[] { Enums.MessageType.success.GetHashCode(), Enums.MessageType.success.ToString(), GetState(Id) }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new object[] { Enums.MessageType.danger.GetHashCode(), Enums.MessageType.danger.ToString(), Messages.ContactToAdmin }, JsonRequestBehavior.AllowGet);
            }
        }

        public IList<SelectListItem> GetState(int id, int Sid = 0)
        {
            PageParam pageParam = new PageParam();

            List<AbstractMasterState> result = masterStateServices.MasterStateSelectAll(pageParam).Values.ToList();
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem() { Text = "Select State", Value = "0" });
            if (result.Count > 0)
            {
                //result = result.Where(x => x.CountryId == id).ToList();
                foreach (var state in result)
                {
                    if (Sid != 0 && Sid == state.Id)
                    {
                        items.Add(new SelectListItem() { Text = state.State.ToString(), Value = state.Id.ToString(), Selected = true });
                    }
                    else
                    {
                        items.Add(new SelectListItem() { Text = state.State.ToString(), Value = state.Id.ToString() });
                    }
                }
            }
            return items;
        }

        [HttpPost]
        [ActionName(Actions.ResetPassword)]
        public JsonResult ResetPassword(string OldPassword, string Password)
        {
            AbstractUsers users = usersService.UsersSelect(ProjectSession.UserID).Item;
            if (OldPassword == users.Password)
            {
                users.Password = Password;
                var result = usersService.UsersResetPassword(users);
                return Json(new object[] { Enums.MessageType.success.GetHashCode(), Enums.MessageType.success.ToString(), Messages.PasswordReSet }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new object[] { Enums.MessageType.danger.GetHashCode(), Enums.MessageType.danger.ToString(), Messages.PasswordUnmatch, ConvertTo.Base64Encode(users.Id.ToString()) }, JsonRequestBehavior.AllowGet);
               
            }
            
        }

        
        [HttpPost]
        public JsonResult DoctorStatusUpdate()
        {            
            var users = usersService.DoctorStatusUpdate(ProjectSession.UserID);
            if (users)
            {
                ProjectSession.DoctorStatus = 1;
            }
            else
            {
                ProjectSession.DoctorStatus = 8;
            }
            return Json("",JsonRequestBehavior.AllowGet);
        }

        #endregion

    }
}