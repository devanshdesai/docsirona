﻿using DataTables.Mvc;
using SironaTeleMed.Common;
using SironaTeleMed.Common.Paging;
using SironaTeleMed.Entities.Contract;
using SironaTeleMed.Entities.V1;
using SironaTeleMed.Services.Contract;
using SironaTeleMedClient.Infrastructure;
using SironaTeleMedClient.Pages;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace SironaTeleMedClient.Controllers
{
    public class UserListingController : BaseController
    {
        //private object usersService;

        private readonly AbstractUsersService usersService;
        private readonly AbstractMasterCountryServices masterCountryServices;
        private readonly AbstractMasterStateService masterStateServices;
        private readonly AbstractSessionsServices sessionsService;

        public UserListingController(AbstractUsersService usersService, AbstractMasterCountryServices masterCountryServices, AbstractMasterStateService masterStateServices, AbstractSessionsServices sessionsService)
        {
            this.usersService = usersService;
            this.masterCountryServices = masterCountryServices;
            this.masterStateServices = masterStateServices;
            this.sessionsService = sessionsService;
        }


        // GET: UserListing
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult UserListing()
        {
            return View();
        }       

        #region Patients
        public ActionResult ManagePatients()
        {
            return View();
        }


        [HttpPost]
        [ActionName(Actions.BindPatients)]
        public JsonResult BindPatients([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                var model = usersService.UsersSelectAll(pageParam,2);
                totalRecord = (int)model.TotalRecords;
                filteredRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new object[] { Enums.MessageType.danger.GetHashCode(), Enums.MessageType.danger.ToString(), Messages.CommonErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Doctors
        public ActionResult ManageDoctors(string suc = "")
        {
            ViewBag.SuccessMessage = suc;
            return View();
        }

        [HttpPost]
        [ActionName(Actions.BindDoctors)]
        public JsonResult BindDoctors([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                var model = usersService.UsersSelectAll(pageParam,1);
                totalRecord = (int)model.TotalRecords;
                filteredRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new object[] { Enums.MessageType.danger.GetHashCode(), Enums.MessageType.danger.ToString(), Messages.CommonErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }


        #endregion

        [HttpPost]
        public ActionResult UserStatusUpdate(string Id)
        {
            int decryptedId = Convert.ToInt32(ConvertTo.Base64Decode(Id));
            PageParam pageParam = new PageParam();

            var users = usersService.UserStatusUpdate(decryptedId);
            return RedirectToAction(Actions.ManageDoctors, Pages.Controllers.UserListing);
        }


        public ActionResult UserDetails(string Id, string err = "")
        {
            ViewBag.Category = null;
            ViewBag.ErrorMessage = err;
            ViewBag.Country = BindCountryDropdown();

            int decryptedId = 0;
            decryptedId = Convert.ToInt32(ConvertTo.Base64Decode(Id));
           
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem() { Text = "Select State", Value = "0" });
            ViewBag.State = GetState(0,0);
            SuccessResult<AbstractUsers> successResult = new SuccessResult<AbstractUsers>();
            AbstractUsers users = new Users();
            users = usersService.UsersSelect(decryptedId).Item;
            if (successResult.Item != null && successResult.Item.Id > 0)
            {
                ViewBag.State = GetState(0, successResult.Item.StateId);
            }
            if (decryptedId == 0)
            {
                if(users == null)
                {
                    users = new Users();
                    users.UserType = 1;
                }                
            }
            return View(users);
        }

        [HttpPost]
        public ActionResult AddUserDetails(Users users,Sessions sessions)
        {
            users.UserType = 1;
            var result = usersService.InsertUpdateUsers(users);

            if (result.Code == 400)
            {
                return RedirectToAction(Actions.UserDetails, Pages.Controllers.UserListing, new {Id=ConvertTo.Base64Encode("0"), err = result.Message });
            }
            else
            {
                ProjectSession.UserID = result.Item.Id;
                string Email = result.Item.Email;
                string Name = result.Item.FirstName + "" + result.Item.LastName;                
                if (Email != "")
                {
                    string body = string.Empty;

                    using (StreamReader reader = new StreamReader(Server.MapPath("~/EmailTemplates/Telemed.html")))
                    {
                        body = reader.ReadToEnd();
                    }
                    body = body.Replace("#titlemsg1", "Welcome,");
                    body = body.Replace("#titlemsg2", "to Sirona Tele Med !");
                    body = body.Replace("#name", result.Item.FirstName + " " + result.Item.LastName);
                    body = body.Replace("#mainmsg", "You have been registered as a doctor. Username : "+ result.Item.Email+". Password : "+result.Item.Password);
                    EmailHelper.Send(Email, "", "", "Welcome To Sirona Tele Med", body);
                }
                return RedirectToAction(Actions.ManageDoctors, Pages.Controllers.UserListing, new { suc = result.Message });
            }
        }

        public IList<SelectListItem> BindStateDropdown()
        {
            PageParam pageParam = new PageParam();
            var model = masterStateServices.MasterStateSelectAll(pageParam);

            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem() { Text = "Select State", Value = "0" });

            foreach (var masterStateServices in model.Values)
            {
                items.Add(new SelectListItem() { Text = masterStateServices.State.ToString(), Value = masterStateServices.Id.ToString() });
            }
            return items;
        }

        public IList<SelectListItem> BindCountryDropdown()
        {
            PageParam pageParam = new PageParam();
            var model = masterCountryServices.MasterCountrySelectAll(pageParam);

            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem() { Text = "Select Country", Value = "0" });

            foreach (var masterCountryServices in model.Values)
            {
                items.Add(new SelectListItem() { Text = masterCountryServices.Country.ToString(), Value = masterCountryServices.Id.ToString() });
            }

            return items;
        }

        [HttpPost]
        [ActionName(Actions.Search1)]
        public JsonResult Search1(int Id)
        {
            try
            {
                return Json(new object[] { Enums.MessageType.success.GetHashCode(), Enums.MessageType.success.ToString(), GetState(Id) }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new object[] { Enums.MessageType.danger.GetHashCode(), Enums.MessageType.danger.ToString(), Messages.ContactToAdmin }, JsonRequestBehavior.AllowGet);
            }
        }

        public IList<SelectListItem> GetState(int id, int Sid = 0)
        {
            PageParam pageParam = new PageParam();

            List<AbstractMasterState> result = masterStateServices.MasterStateSelectAll(pageParam).Values.ToList();
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem() { Text = "Select State", Value = "0" });
            if (result.Count > 0)
            {
                //result = result.Where(x => x.CountryId == id).ToList();
                foreach (var state in result)
                {
                    if (Sid != 0 && Sid == state.Id)
                    {
                        items.Add(new SelectListItem() { Text = state.State.ToString(), Value = state.Id.ToString(), Selected = true });
                    }
                    else
                    {
                        items.Add(new SelectListItem() { Text = state.State.ToString(), Value = state.Id.ToString() });
                    }
                }
            }
            return items;
        }
    }
}