﻿using SironaTeleMed.Entities.Contract;
using SironaTeleMed.Entities.V1;
using SironaTeleMed.Services.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SironaTeleMedClient.Controllers
{
    public class BookNowController : Controller
    {
        private readonly AbstractSessionScheduleServices sessionsScheduleService;
        public BookNowController(AbstractSessionScheduleServices sessionsScheduleService)
        {
            this.sessionsScheduleService = sessionsScheduleService;
        }

        // GET: BookNow
        public ActionResult Index()
        {
            AbstractIndexMaster abstractIndexMaster = new IndexMaster();
            List<AbstractSessionSchedule> sessionSchedule = new List<AbstractSessionSchedule>();// new List<SessionSchedule>();
            var sessionsSchedules = sessionsScheduleService.SearchSessionsSelectByDate(DateTime.Now.ToString("dd/MM/yyyy"));
            if (sessionsSchedules.Values.Count() > 0)
            {
                abstractIndexMaster.abstractSessionSchedules = sessionsSchedules.Values;
            }
            return View(abstractIndexMaster);
        }

        [HttpPost]
        public ActionResult _SearchSessions(string date)
        {
            AbstractIndexMaster abstractIndexMaster = new IndexMaster();
            List<AbstractSessionSchedule> sessionSchedule = new List<AbstractSessionSchedule>();// new List<SessionSchedule>();
            var sessions = sessionsScheduleService.SearchSessionsSelectByDate(date);
            if (sessions.Values.Count() > 0)
            {
                abstractIndexMaster.abstractSessionSchedules = sessions.Values;
            }
            return PartialView(abstractIndexMaster);
        }

    }
}