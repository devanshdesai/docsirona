﻿using System;
using System.Web.Mvc;
using DataTables.Mvc;
using SironaTeleMed.Common;
using SironaTeleMed.Entities.Contract;
using SironaTeleMedClient.Infrastructure;
using SironaTeleMed.Services.Contract;
using static SironaTeleMedClient.Infrastructure.Enums;
using System.Web;
using SironaTeleMed.Entities.V1;
using SironaTeleMedClient.Pages;
using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace SironaTeleMedClient.Controllers
{
    public class LoginController : Controller
    {
        private readonly AbstractUsersService usersService;

        public LoginController(AbstractUsersService usersService)
        {
            this.usersService = usersService;
        }

        ///// <summary>
        ///// Default Action Of User page.
        ///// </summary>
        ///// <returns></returns>
        public ActionResult Index(string suc = "", string ReturnUrl = null)
        {
            if(ReturnUrl == null)
            {
                ReturnUrl = "";
            }
            ViewBag.ReturnUrl = ReturnUrl;
            ViewBag.ErrorMessage = "";
            ViewBag.SuccessMessage = suc;
            ViewBag.PageTitleName = "Login";
            if (TempData["openPopup"] != null)
                ViewBag.openPopup = TempData["openPopup"];

            ViewBag.SSId = "";
            if (ReturnUrl != null && Convert.ToString(ReturnUrl).Contains("SSid"))
            {
                ViewBag.SSId = ReturnUrl.Split('=')[1];
            }            
            return View();
        }

        ///// <summary>
        ///// Logs the in.
        ///// </summary>
        ///// <param name="objmodel">The objmodel.</param>
        ///// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult Index(Users objmodel, string ReturnUrl = null)
        {
            int userTypeId = 2;
            string userTypeStr = "";
            SuccessResult<AbstractUsers> userData = usersService.Login(objmodel);
            if (userData != null && userData.Code == 200 && userData.Item != null && userData.Item.UserType != 4)
            {
                Session.Clear();
                ProjectSession.UserID = userData.Item.Id;
                ProjectSession.DoctorStatus = userData.Item.IsActive;
                ProjectSession.UserProfilePicture = userData.Item.ProfileUrl;
                ProjectSession.DeviceToken = userData.Item.DeviceToken;
                ProjectSession.StripeId = userData.Item.StripeId;
                userTypeId = userData.Item.UserType;
                ProjectSession.Password = userData.Item.Password;
                //Remember me
                if (userTypeId == 1)
                {
                    userTypeStr = "DOCTOR";
                }
                else if (userTypeId == 2)
                {
                    userTypeStr = "USER";
                }
                else if (userTypeId == 3)
                {
                    userTypeStr = "ADMIN";
                }

                ProjectSession.UserName = userData.Item.FirstName + " " + userData.Item.LastName + " ( " + userTypeStr + " )";
                HttpCookie cookie = new HttpCookie("AdminUserLogin_Client");
                cookie.Values.Add("Email", userData.Item.Email);
                cookie.Values.Add("UserName", ProjectSession.UserName);
                cookie.Values.Add("IsRememberMe", "False");
                cookie.Values.Add("SironaTeleMedUserId", userData.Item.Id.ToString());                               
                cookie.Values.Add("UserType", userTypeId.ToString());
                cookie.Values.Add("UserTypeStr", userTypeStr);
                cookie.Values.Add("IsActive", ""+userData.Item.IsActive);
                cookie.Values.Add("ProfileUrl", ProjectSession.UserProfilePicture);
                cookie.Expires = DateTime.Now.AddDays(1);               
                Response.Cookies.Add(cookie);

                if(ReturnUrl != "" && ReturnUrl != null)
                {
                    if(userTypeId == 2)
                    {
                        if (ReturnUrl.ToLower().Contains("https"))
                        {
                            ReturnUrl = ReturnUrl.ToLower().Replace("https", "http");
                        }
                        return Redirect(ReturnUrl);
                    }
                }
                //if (string.IsNullOrEmpty(userData.Item.StripeId) && userData.Item.UserType == 2)
                //{
                //    return RedirectToAction(Actions.Index, Pages.Controllers.StripePayment, new { Area = "" });
                //}
                return RedirectToAction(Actions.Index, Pages.Controllers.Dashboard, new { Area = "" });                
            }
            else 
            {
                ViewBag.openPopup = CommonHelper.ShowAlertMessageToastr(MessageType.danger.ToString(), Messages.InValidCredential);
            }
            ViewBag.ErrorMessage = "Invalid USERNAME or PASSWORD !!";
            ViewBag.ReturnUrl = "No Return Url";
            return View();
        }

        [AllowAnonymous]
        [ActionName(Actions.Logout)]
        public ActionResult Logout()
        {
            ProjectSession.UserID = 0;
            ProjectSession.UserName = "";
            ProjectSession.Email = "";
            ProjectSession.UserType = 0;
            ProjectSession.UserTypeStr = "";
            Session.Clear();
            Session.Abandon();

            string[] ck = Request.Cookies.AllKeys;
            foreach (string cookie in ck)
            {
                Response.Cookies[cookie].Expires = DateTime.Now.AddDays(-1);
            }
            return RedirectToAction(Actions.Index, Pages.Controllers.Login);
        }
        
    }
}