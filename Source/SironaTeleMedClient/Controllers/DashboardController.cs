﻿using DataTables.Mvc;
using SironaTeleMed.Common;
using SironaTeleMed.Common.Paging;
using SironaTeleMed.Entities.Contract;
using SironaTeleMed.Entities.V1;
using SironaTeleMed.Services.Contract;
using SironaTeleMedClient.Infrastructure;
using SironaTeleMedClient.Pages;
using Square.Connect.Api;
using Square.Connect.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SironaTeleMedClient.Controllers
{
    public class DashboardController : BaseController
    {
        private readonly AbstractUsersService usersService;
        private readonly AbstractSessionsServices sessionsService;
        private readonly AbstractSessionScheduleServices sessionsScheduleService;

        public DashboardController(AbstractUsersService usersService, AbstractSessionsServices sessionsService, AbstractSessionScheduleServices sessionsScheduleService)
        {
            this.usersService = usersService;
            this.sessionsService = sessionsService;
            this.sessionsScheduleService = sessionsScheduleService;
        }

        public ActionResult Index(string showPopUp = "")
        {
            PageParam pageParam = new PageParam();
            AbstractIndexMaster abstractIndexMaster = new IndexMaster();
            pageParam.Limit = 5;
            pageParam.Offset = 0;
            string StatusId = "2,3,9";

            ViewBag.Doctor = BindDoctorsDdp();
            ViewBag.Users = BindPatientsDdp();

            AbstractUsers users = usersService.UsersSelect(ProjectSession.UserID).Item;

            if (ProjectSession.UserType == 3 || ProjectSession.UserType == 1)
            {
                StatusId = "3,9";
            }

            if (ProjectSession.UserType == 3)
            {
                abstractIndexMaster.abstractSessions = sessionsService.SessionScheduleSelectByUserIdStatusId(pageParam, StatusId, null, null).Values;
                abstractIndexMaster.abstractNewSessions = sessionsService.SessionScheduleSelectByUserIdStatusId(pageParam, "2", null, null).Values;
            }
            else
            {
                abstractIndexMaster.abstractSessions = sessionsService.SessionScheduleSelectByUserIdStatusId(pageParam, StatusId, ProjectSession.UserID, null).Values;
            }

            // start Session Schedule
            if (ProjectSession.UserType == 2)
            {
                List<AbstractSessionSchedule> sessionSchedule = new List<AbstractSessionSchedule>();// new List<SessionSchedule>();
                var sessionsSchedules = sessionsScheduleService.SearchSessionsSelectByDate(DateTime.Now.ToString("dd/MM/yyyy"));
                if (sessionsSchedules.Values.Count() > 0)
                {
                    abstractIndexMaster.abstractSessionSchedules = sessionsSchedules.Values;
                }
            }
            // end Session Schedule
            abstractIndexMaster.IsMessage = ProjectSession.IsMsg;
            return View(abstractIndexMaster);
        }

        [HttpPost]
        public ActionResult SessionUpdateByStatusId(string Id, int StatusId, string RejectedReason, string CancelledReason, string AssignedTo)
        {
            int decryptedId = Convert.ToInt32(ConvertTo.Base64Decode(Id));
            PageParam pageParam = new PageParam();

            var session = sessionsService.SessionUpdateByStatusId(decryptedId, StatusId, RejectedReason, CancelledReason, AssignedTo);
            ProjectSession.UserID = session.Item.Id;
            string PatientEmail = session.Item.PatientEmail;
            string PatientName = session.Item.PatientName;
            string DoctorEmail = session.Item.DoctorEmail;
            string DoctorName = session.Item.DoctorName;

            string body = string.Empty;
            string doctorbody = string.Empty;
            using (StreamReader reader = new StreamReader(Server.MapPath("~/EmailTemplates/Telemed.html")))
            {
                body = reader.ReadToEnd();
                doctorbody = body;
            }

            if (session.Item.StatusId == 3)
            {
                if (PatientEmail != "" && DoctorEmail != "")
                {
                    string calenderbody = "";
                    if (session.Item.UserType != 4)
                    {
                        body = body.Replace("#titlemsg1", "Doctor Assigned");
                        body = body.Replace("#titlemsg2", "");
                        body = body.Replace("#name", PatientName);
                        body = body.Replace("#mainmsg", "Your appointment for " + session.Item.DisplayDate + " at " + session.Item.DisplayTime + " has been assigned to the doctor. For more information you can log into your account");
                        EmailHelper.Send(PatientEmail, "", "", "Doctor Assigned", body);
                        CommonHelper.NotificationSent(session.Item.PatientDeviceToken, "Your appointment for " + session.Item.DisplayDate + " at " + session.Item.DisplayTime + " has been assigned to the doctor.", "Doctor Assigned");
                        SendTextMessage("Your appointment for " + session.Item.DisplayDate + " at " + session.Item.DisplayTime + " has been assigned to the doctor.", session.Item.PatientPhone, "");
                        calenderbody = "<h4>Greetings from Sirona Tele Med.</h4><br/><p>Hello " + PatientName + ".</p><p>Your appointment for " + session.Item.DisplayDate + " at " + session.Item.DisplayTime + " has been assigned to the doctor. For more information you can log into your account.</p>";
                        AdminEmailService.Send(PatientEmail, "", "", "Doctor Assigned", "Your appointment for " + session.Item.DisplayDate + " at " + session.Item.DisplayTime + " has been assigned to the doctor. For more information you can log into your account",
                        null, null, session.Item.SessionStart, session.Item.SessionStart.Value.AddMinutes(30), calenderbody);
                    }
                    else
                    {
                        body = body.Replace("#titlemsg1", "Doctor Assigned");
                        body = body.Replace("#titlemsg2", "");
                        body = body.Replace("#name", PatientName);
                        body = body.Replace("#mainmsg", "Your appointment for " + session.Item.DisplayDate + " at " + session.Item.DisplayTime + " has been assigned to the doctor. Doctor name is : " + DoctorName + ". NOTE : You shall be notified once the doctor starts the appointment on the scheduled time.");
                        EmailHelper.Send(PatientEmail, "", "", "Doctor Assigned", body);
                        SendTextMessage("Your appointment for " + session.Item.DisplayDate + " at " + session.Item.DisplayTime + " has been assigned to the doctor.Doctor name is : " + DoctorName + ".NOTE : You shall be notified once the doctor starts the appointment on the scheduled time.", session.Item.PatientPhone, "");
                        calenderbody = "<h4>Greetings from Sirona Tele Med.</h4><br/><p>Hello " + PatientName + ".</p><p>Your appointment for " + session.Item.DisplayDate + " at " + session.Item.DisplayTime + " has been assigned to the doctor. Doctor name is : " + DoctorName + ". NOTE : You shall be notified once the doctor starts the appointment on the scheduled time.</p>";
                        AdminEmailService.Send(PatientEmail, "", "", "Doctor Assigned", "Your appointment for " + session.Item.DisplayDate + " at " + session.Item.DisplayTime + " has been assigned to the doctor. Doctor name is : " + DoctorName + ". NOTE : You shall be notified once the doctor starts the appointment on the scheduled time.",
                        null, null, session.Item.SessionStart, session.Item.SessionStart.Value.AddMinutes(30), calenderbody);
                    }

                    doctorbody = doctorbody.Replace("#titlemsg1", "New Appointment Assigned");
                    doctorbody = doctorbody.Replace("#titlemsg2", "");
                    doctorbody = doctorbody.Replace("#name", DoctorName);
                    doctorbody = doctorbody.Replace("#mainmsg", "New appointment for " + session.Item.DisplayDate + " at " + session.Item.DisplayTime + " has been assigned you. For more information you can log into your account");
                    EmailHelper.Send(DoctorEmail, "", "", "New Appointment Assigned", doctorbody);
                    CommonHelper.NotificationSent(session.Item.DoctorDeviceToken, "New appointment for " + session.Item.DisplayDate + " at " + session.Item.DisplayTime + " has been assigned you.", "New Appointment Assigned");
                    SendTextMessage("New appointment for " + session.Item.DisplayDate + " at " + session.Item.DisplayTime + " has been assigned you.", session.Item.DoctorPhone, "");
                }
            }
            else if (session.Item.StatusId == 4)
            {
                if (PatientEmail != "")
                {

                    body = body.Replace("#titlemsg1", "Appointment Cancelled");
                    body = body.Replace("#titlemsg2", "");
                    body = body.Replace("#name", PatientName);
                    if (CancelledReason != string.Empty)
                    {
                        body = body.Replace("#mainmsg", "Your appointment for " + session.Item.DisplayDate + " at " + session.Item.DisplayTime + " has been cancelled due to the following reason : " + CancelledReason);
                    }
                    else
                    {
                        body = body.Replace("#mainmsg", "Your appointment for " + session.Item.DisplayDate + " at " + session.Item.DisplayTime + " has been cancelled");
                    }
                    EmailHelper.Send(PatientEmail, "", "", "Appointment Cancelled", body);
                    CommonHelper.NotificationSent(session.Item.PatientDeviceToken, "Your appointment for " + session.Item.DisplayDate + " at " + session.Item.DisplayTime + " has been cancelled", "Appointment Cancelled");
                    SendTextMessage("Your appointment for " + session.Item.DisplayDate + " at " + session.Item.DisplayTime + " has been cancelled", session.Item.PatientPhone, "");
                }

                if (DoctorEmail != "")
                {
                    doctorbody = doctorbody.Replace("#titlemsg1", "Appointment Cancelled");
                    doctorbody = doctorbody.Replace("#titlemsg2", "");
                    doctorbody = doctorbody.Replace("#name", DoctorName);
                    if (CancelledReason != string.Empty)
                    {
                        doctorbody = doctorbody.Replace("#mainmsg", "Your appointment for " + session.Item.DisplayDate + " at " + session.Item.DisplayTime + " has been cancelled due to the following reason : " + CancelledReason);
                    }
                    else
                    {
                        doctorbody = doctorbody.Replace("#mainmsg", "Your appointment for " + session.Item.DisplayDate + " at " + session.Item.DisplayTime + " has been cancelled");
                    }
                    EmailHelper.Send(DoctorEmail, "", "", "Appointment Cancelled", doctorbody);
                    CommonHelper.NotificationSent(session.Item.DoctorDeviceToken, "Your appointment for " + session.Item.DisplayDate + " at " + session.Item.DisplayTime + " has been cancelled", "Appointment Cancelled");
                    SendTextMessage("Your appointment for " + session.Item.DisplayDate + " at " + session.Item.DisplayTime + " has been cancelled", session.Item.DoctorPhone, "");
                }
            }
            else if (session.Item.StatusId == 5)
            {
                if (PatientEmail != "")
                {
                    body = body.Replace("#titlemsg1", "Appointment Rejected");
                    body = body.Replace("#titlemsg2", "");
                    body = body.Replace("#name", PatientName);
                    if (RejectedReason != string.Empty)
                    {
                        body = body.Replace("#mainmsg", "Your appointment for " + session.Item.DisplayDate + " at " + session.Item.DisplayTime + " has been rejected due to the following reason : " + RejectedReason);
                    }
                    else
                    {
                        body = body.Replace("#mainmsg", "Your appointment for " + session.Item.DisplayDate + " at " + session.Item.DisplayTime + " has been rejected");
                    }

                    EmailHelper.Send(PatientEmail, "", "", "Appointment Rejected", body);
                    CommonHelper.NotificationSent(session.Item.PatientDeviceToken, "Your appointment for " + session.Item.DisplayDate + " at " + session.Item.DisplayTime + " has been rejected", "Appointment Rejected");
                    SendTextMessage("Your appointment for " + session.Item.DisplayDate + " at " + session.Item.DisplayTime + " has been rejected", session.Item.PatientPhone, "");
                }
            }

            return RedirectToAction(Actions.NewAppointments, Pages.Controllers.Appointment);
        }

        [HttpPost]
        public ActionResult _SearchSessions(string date)
        {
            AbstractIndexMaster abstractIndexMaster = new IndexMaster();
            List<AbstractSessionSchedule> sessionSchedule = new List<AbstractSessionSchedule>();// new List<SessionSchedule>();
            var sessions = sessionsScheduleService.SearchSessionsSelectByDate(date);
            if (sessions.Values.Count() > 0)
            {
                abstractIndexMaster.abstractSessionSchedules = sessions.Values;
            }
            return PartialView(abstractIndexMaster);
        }

        [HttpGet]
        public JsonResult UpdateIsMessage()
        {
            if(ProjectSession.IsMsg == "1")
            {
                ProjectSession.IsMsg = "0";
            }
            return Json("", JsonRequestBehavior.AllowGet);
        }
        
        public IList<SelectListItem> BindDoctorsDdp()
        {
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 0;

            var model = usersService.UsersSelectAll(pageParam, 1);
            model.Values = model.Values.Where(x => x.IsActive == 1).ToList();

            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem() { Text = "Select Doctor", Value = "0" });

            foreach (var usersService in model.Values)
            {
                items.Add(new SelectListItem() { Text = usersService.FirstName.ToString() + " " + usersService.LastName, Value = usersService.Id.ToString() });
            }

            return items;
        }

        public IList<SelectListItem> BindPatientsDdp()
        {
            PageParam pageParam = new PageParam();
            var model = usersService.UsersSelectAll(pageParam, 2);
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem() { Text = "Select Patients", Value = "0" });

            foreach (var usersService in model.Values)
            {
                items.Add(new SelectListItem() { Text = Convert.ToString(usersService.FirstName) + " " + Convert.ToString(usersService.LastName), Value = usersService.Id.ToString() });
            }

            return items;
        }
    }
}