﻿using DataTables.Mvc;
using SironaTeleMed.Common;
using SironaTeleMed.Common.Paging;
using SironaTeleMed.Entities.Contract;
using SironaTeleMed.Entities.V1;
using SironaTeleMed.Services.Contract;
using SironaTeleMedClient.Infrastructure;
//using SironaTeleMed.Infrastructure;
using SironaTeleMedClient.Pages;
using Square.Connect.Api;
using Square.Connect.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
//using static SironaTeleMed.Infrastructure.Enums;
using System.Web;
using System.Web.Mvc;

namespace SironaTeleMedClient.Controllers
{
    public class HomeController : Controller
    {
        
        public ActionResult Index()
        {
            return View();
        }
    }
}