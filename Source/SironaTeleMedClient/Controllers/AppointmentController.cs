﻿using SironaTeleMed.Entities.Contract;
using SironaTeleMedClient.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using SironaTeleMed.Common.Paging;
using SironaTeleMed.Entities.V1;
using SironaTeleMed.Services.Contract;
using SironaTeleMedClient.Pages;
using System.Web;
using System.Web.Mvc;
using SironaTeleMed.Common;
using DataTables.Mvc;
using System.IO;
using Twilio;
using Twilio.Rest.Video.V1;
using Stripe;
using System.Configuration;
using System.Xml.Serialization;
using System.Net.Http;
using System.Text;
using System.Xml;
using static SironaTeleMedClient.Infrastructure.Enums;
using System.Threading.Tasks;
using System.Net;
using System.Data;
using System.Data.SqlClient;

namespace SironaTeleMedClient.Controllers
{
    public class AppointmentController : BaseController
    {
        private readonly AbstractUsersService usersService;
        private readonly AbstractSessionsServices sessionsService;
        private readonly AbstractSessionScheduleServices sessionsScheduleService;
        private readonly AbstractSessionDocumentServices sessionDocumentServices;

        public AppointmentController(AbstractUsersService usersService, AbstractSessionsServices sessionsService, AbstractSessionScheduleServices sessionsScheduleService, AbstractSessionDocumentServices sessionDocumentServices)
        {
            this.usersService = usersService;
            this.sessionsService = sessionsService;
            this.sessionsScheduleService = sessionsScheduleService;
            this.sessionDocumentServices = sessionDocumentServices;
        }

        [HttpGet]
        public ActionResult BookAppointments(int id, int SSid = 0)
        {
            AbstractSessions sessions = new Sessions();
            if (id == 0)
            {
                PageParam pageParam = new PageParam();
                sessions.SessionScheduleId = SSid;
                var sessionschedule = sessionsScheduleService.SessionScheduleSelect(sessions.SessionScheduleId);
                if (sessionschedule != null)
                {
                    sessions.SessionStart = sessionschedule.Item.SessionStart;
                }
                return View(sessions);
            }
            else
            {
                sessions = sessionsService.SessionsSelect(id).Item;
                sessions.UserDescription = sessions.UUserDescription;
                sessions.SpecialRequests = sessions.SSpecialRequests;
                sessions.DoctorComments = sessions.DDoctorComments;
                sessions.DoctorComments2 = sessions.DDoctorComments2;
                sessions.DoctorComments3 = sessions.DDoctorComments3;
                sessions.DoctorComments4 = sessions.DDoctorComments4;
                if (sessions == null)
                {
                    return HttpNotFound();
                }
                return View(sessions);
            }
        }

        [HttpGet]
        public JsonResult UpdateIsMessage()
        {
            if (ProjectSession.IsMsg == "1")
            {
                ProjectSession.IsMsg = "0";
            }
            return Json("", JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public ActionResult DoctorAppointment(int id, int SSid = 0)
        {
            AbstractSessions sessions = new Sessions();
            if (id == 0)
            {
                PageParam pageParam = new PageParam();
                sessions.SessionScheduleId = SSid;
                var sessionschedule = sessionsScheduleService.SessionScheduleSelect(sessions.SessionScheduleId);
                if (sessionschedule != null)
                {
                    sessions.SessionStart = sessionschedule.Item.SessionStart;
                }
                return View(sessions);
            }
            else
            {
                sessions = sessionsService.SessionsSelect(id).Item;
                sessions.UserDescription = sessions.UUserDescription;
                sessions.SpecialRequests = sessions.SSpecialRequests;
                sessions.DoctorComments = sessions.DDoctorComments;
                sessions.DoctorComments2 = sessions.DDoctorComments2;
                sessions.DoctorComments3 = sessions.DDoctorComments3;
                sessions.DoctorComments4 = sessions.DDoctorComments4;
                if (sessions == null)
                {
                    return HttpNotFound();
                }
                sessions.RxInput = "-";
                if(sessions.StatusId == 10)
                {
                    DataTable dtp = new DataTable();
                    DataTable dtd = new DataTable();
                    SqlConnection con = new SqlConnection(Configurations.ConnectionString);
                    string var_sql = "select U.*,MS.State as StateName from Users U left join MasterState MS on U.StateId=MS.Id where U.Id = " + sessions.UserId;
                    SqlDataAdapter sda = new SqlDataAdapter(var_sql, con);
                    sda.Fill(dtp);

                    var_sql = "select U.*,MS.State as StateName,MSD.State as LicenseStateName from Users U left join MasterState MS on U.StateId=MS.Id left join MasterStates MSD on U.Doctorlicensestate=MSD.Id where U.Id = " + sessions.AssignedTo;
                    SqlDataAdapter sdad = new SqlDataAdapter(var_sql, con);
                    sdad.Fill(dtd);

                    if(dtp.Rows.Count > 0 && dtd.Rows.Count > 0)
                    {
                        string tempPrescription = "<NCScript xmlns='http://secure.newcropaccounts.com/interfaceV7' xmlns:NCStandard='http://secure.newcropaccounts.com/interfaceV7:NCStandard' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'><Credentials><partnerName>@partnername</partnerName><name>@name</name><password>@password</password><productName>@productname</productName><productVersion>@productversion</productVersion></Credentials><UserRole><user>@user</user><role>@role</role></UserRole><Destination><requestedPage>@requestedpage</requestedPage></Destination><Account ID='demo'><accountName>@accountname</accountName><siteID>@siteId</siteID><AccountAddress><address1>@customeraddress1</address1><address2>@customeraddress2</address2><city>@customercity</city><state>@customerstate</state><zip>@customerzip</zip><country>@customercountry</country></AccountAddress><accountPrimaryPhoneNumber>@accountprimaryphonenumber</accountPrimaryPhoneNumber><accountPrimaryFaxNumber>@accountprimaryfaxnumber</accountPrimaryFaxNumber></Account>";
                        tempPrescription += "<Location ID='DEMOLOC1'><locationName>@customerLocation</locationName><LocationAddress><address1>@customeraddress1</address1><address2>@customeraddress2</address2><city>@customercity</city><state>@customerstate</state><zip>@customerzip</zip><country>@customercountry</country></LocationAddress><primaryPhoneNumber>@primaryphonenumber</primaryPhoneNumber><primaryFaxNumber>@primaryfaxnumber</primaryFaxNumber><pharmacyContactNumber>@pharmacycontactnumber</pharmacyContactNumber></Location>";
                        tempPrescription += "<LicensedPrescriber ID='DEMOLP1'><LicensedPrescriberName><last>@doctorlastname</last><first>@doctorfirstname</first><middle>@doctormiddlename</middle></LicensedPrescriberName><dea>@dea</dea><upin>@upin</upin><licenseState>@doctorlicensestate</licenseState><licenseNumber>@doctorlicensenumber</licenseNumber><npi/></LicensedPrescriber>";
                        tempPrescription += "<Patient ID='DEMOPT1'><PatientName><last>@customerlastname</last><first>@customerfirtname</first><middle>@customermiddlename</middle></PatientName><medicalRecordNumber>@medicalrecordnumber</medicalRecordNumber><memo>@memo</memo><PatientAddress><address1>@customeraddress1</address1><address2>@customeraddress2</address2><city>@customercity</city><state>@customerstate</state><zip>@customerzip</zip><country>@customercountry</country></PatientAddress>";
                        tempPrescription += "<PatientContact><homeTelephone>@hometelephone</homeTelephone></PatientContact><PatientCharacteristics><dob>@dob</dob><gender>@gender</gender></PatientCharacteristics></Patient></NCScript>";


                        tempPrescription = tempPrescription.Replace("@partnername", "demo");
                        tempPrescription = tempPrescription.Replace("@name", "demo");
                        tempPrescription = tempPrescription.Replace("@password", "demo");
                        tempPrescription = tempPrescription.Replace("@productname", "SuperDuperSoftware");
                        tempPrescription = tempPrescription.Replace("@productversion", "V5.3");
                        tempPrescription = tempPrescription.Replace("@user", "LicensedPrescriber");
                        tempPrescription = tempPrescription.Replace("@requestedpage", "compose");
                        tempPrescription = tempPrescription.Replace("@role", "doctor");
                        tempPrescription = tempPrescription.Replace("@accountname", "Your Customer's Account Name");
                        tempPrescription = tempPrescription.Replace("@siteId", "demo");

                        tempPrescription = tempPrescription.Replace("@customeraddress1", Convert.ToString(dtp.Rows[0]["Address"]));
                        tempPrescription = tempPrescription.Replace("@customeraddress2", Convert.ToString(dtp.Rows[0]["AddressLine2"]));
                        tempPrescription = tempPrescription.Replace("@customercity", Convert.ToString(dtp.Rows[0]["City"]));
                        tempPrescription = tempPrescription.Replace("@customerstate", Convert.ToString(dtp.Rows[0]["StateName"]));
                        tempPrescription = tempPrescription.Replace("@customerzip", Convert.ToString(dtp.Rows[0]["CountryId"]));
                        //tempPrescription = tempPrescription.Replace("@zip4", "1234");
                        tempPrescription = tempPrescription.Replace("@customercountry", "US");
                        tempPrescription = tempPrescription.Replace("@accountprimaryphonenumber", Convert.ToString(dtp.Rows[0]["Phone"]));
                        tempPrescription = tempPrescription.Replace("@accountprimaryfaxnumber", Convert.ToString(dtd.Rows[0]["FaxNumber"]));

                        tempPrescription = tempPrescription.Replace("@customerLocation", "Your Customer's Location Name");
                        tempPrescription = tempPrescription.Replace("@customeraddress1", Convert.ToString(dtp.Rows[0]["Address"]));
                        tempPrescription = tempPrescription.Replace("@customeraddress2", Convert.ToString(dtp.Rows[0]["AddressLine2"]));
                        tempPrescription = tempPrescription.Replace("@customercity", Convert.ToString(dtp.Rows[0]["City"]));
                        tempPrescription = tempPrescription.Replace("@customerstate", Convert.ToString(dtp.Rows[0]["StateName"]));
                        tempPrescription = tempPrescription.Replace("@customerzip", Convert.ToString(dtp.Rows[0]["CountryId"]));
                        //tempPrescription = tempPrescription.Replace("@zip4", "1234");
                        tempPrescription = tempPrescription.Replace("@customercountry", "US");
                        tempPrescription = tempPrescription.Replace("@primaryphonenumber", Convert.ToString(dtp.Rows[0]["Phone"]));
                        tempPrescription = tempPrescription.Replace("@primaryfaxnumber", Convert.ToString(dtp.Rows[0]["FaxNumber"]));
                        tempPrescription = tempPrescription.Replace("@pharmacycontactnumber", "5555551212");

                        tempPrescription = tempPrescription.Replace("@doctorlastname", Convert.ToString(dtd.Rows[0]["LastName"]));
                        tempPrescription = tempPrescription.Replace("@doctorfirstname", Convert.ToString(dtd.Rows[0]["FirstName"]));
                        tempPrescription = tempPrescription.Replace("@doctormiddlename", "");
                        tempPrescription = tempPrescription.Replace("@dea", Convert.ToString(dtd.Rows[0]["Dea"]));
                        tempPrescription = tempPrescription.Replace("@upin", Convert.ToString(dtd.Rows[0]["Upin"]));
                        tempPrescription = tempPrescription.Replace("@doctorlicensestate", Convert.ToString(dtd.Rows[0]["LicenseStateName"]));
                        tempPrescription = tempPrescription.Replace("@doctorlicensenumber", Convert.ToString(dtd.Rows[0]["Doctorlicensenumber"]));

                        tempPrescription = tempPrescription.Replace("@customerlastname", Convert.ToString(dtp.Rows[0]["LastName"]));
                        tempPrescription = tempPrescription.Replace("@customerfirtname", Convert.ToString(dtp.Rows[0]["FirstName"]));
                        tempPrescription = tempPrescription.Replace("@customermiddlename", "");
                        tempPrescription = tempPrescription.Replace("@medicalrecordnumber", Convert.ToString(dtp.Rows[0]["MedicalRecordNumber"]));
                        tempPrescription = tempPrescription.Replace("@memo", "");
                        tempPrescription = tempPrescription.Replace("@customeraddress1", Convert.ToString(dtp.Rows[0]["Address"]));
                        tempPrescription = tempPrescription.Replace("@customeraddress2", Convert.ToString(dtp.Rows[0]["AddressLine2"]));
                        tempPrescription = tempPrescription.Replace("@customercity", Convert.ToString(dtp.Rows[0]["City"]));
                        tempPrescription = tempPrescription.Replace("@customerstate", Convert.ToString(dtp.Rows[0]["StateName"]));
                        tempPrescription = tempPrescription.Replace("@customerzip", Convert.ToString(dtp.Rows[0]["CountryId"]));
                        tempPrescription = tempPrescription.Replace("@customercountry", "US");

                        string[] dobsplit = null;
                        string dob = "";
                        if (Convert.ToString(dtp.Rows[0]["DateOfBirth"]) != string.Empty || Convert.ToString(dtp.Rows[0]["DateOfBirth"]) != null)
                        {
                            dobsplit = Convert.ToString(dtp.Rows[0]["DateOfBirth"]).Split('-');
                            dob = dobsplit[2] + dobsplit[1] + dobsplit[0];
                        }
                        else
                        {
                            dob = "";
                        }
                        
                        tempPrescription = tempPrescription.Replace("@hometelephone", Convert.ToString(dtp.Rows[0]["Phone"]));
                        tempPrescription = tempPrescription.Replace("@dob", dob);
                        tempPrescription = tempPrescription.Replace("@gender", Convert.ToString(dtp.Rows[0]["Gender"]));

                        sessions.RxInput = tempPrescription;
                    }
                }

                return View(sessions);
            }

        }

        [HttpPost]
        public ActionResult AddBookAppointments(Sessions sessions, IEnumerable<HttpPostedFileBase> files)
        {
            StripeConfiguration.ApiKey = Configurations.StripeSecretKey;
            var users = usersService.UsersSelect(ProjectSession.UserID).Item;
            var options = new CustomerCreateOptions { Email = users.Email, Name = users.FullName, Description = "Customer for " + users.Email, Source = sessions.StripeToken };
            var service = new Stripe.CustomerService();
            Stripe.Customer customer = service.Create(options);
            users.StripeId = customer.Id;
            ProjectSession.StripeId = customer.Id;

            sessions.CreatedBy = ProjectSession.UserID;
            sessions.UserId = ProjectSession.UserID;
            var result = sessionsService.InsertUpdateSessions(sessions);

            if (result.Code == 200)
            {
                //ProjectSession.UserID = result.Item.Id;
                string PatientEmail = result.Item.PatientEmail;
                string PatientName = result.Item.PatientName;
                if (PatientEmail != "")
                {
                    string body = string.Empty;
                    using (StreamReader reader = new StreamReader(Server.MapPath("~/EmailTemplates/Telemed.html")))
                    {
                        body = reader.ReadToEnd();
                    }
                    body = body.Replace("#titlemsg1", "New Appointment");
                    body = body.Replace("#titlemsg2", "");
                    body = body.Replace("#name", PatientName);
                    body = body.Replace("#mainmsg", "Your appointment for " + result.Item.DisplayDate + " at " + result.Item.DisplayTime + " has been successfully created and a doctor shall soon be assigned");
                    EmailHelper.Send(PatientEmail, "", "", "New Appointment Registered", body);
                    CommonHelper.NotificationSent(result.Item.PatientDeviceToken, "Your appointment for " + result.Item.DisplayDate + " at " + result.Item.DisplayTime + " has been successfully created and a doctor shall soon be assigned", "New Appointment Registered");
                    SendTextMessage("Your appointment for " + result.Item.DisplayDate + " at " + result.Item.DisplayTime + " has been successfully created and a doctor shall soon be assigned", result.Item.PatientPhone, "");
                    try
                    {                        
                        var planService = new PlanService();
                        var planOptions = new PlanListOptions
                        {
                            ProductId = Configurations.ProductId
                        };
                        var planList = planService.List(planOptions).FirstOrDefault();

                        var customerService = new CustomerService();
                        var sourceId = customerService.Get(ProjectSession.StripeId).DefaultSourceId;

                        var items = new List<SubscriptionItemOption> {
                        new SubscriptionItemOption {
                            PlanId = planList.Id
                           }
                        };
                        var options1 = new SubscriptionCreateOptions
                        {
                            CustomerId = ProjectSession.StripeId,
                            Items = items
                        };
                        var subscriptionService = new SubscriptionService();
                        Subscription subscription = subscriptionService.Create(options1);

                        var invoiceService = new InvoiceService();
                        var invoice = invoiceService.Get(subscription.LatestInvoiceId);

                        var paymentIntentService = new PaymentIntentService();

                        var paymentintentUpdate = new PaymentIntentUpdateOptions
                        {
                            Description = "Your appointment for " + result.Item.DisplayDate + " at " + result.Item.DisplayTime + " has been successfully created -> (" + result.Item.PatientName + "," + result.Item.Id + ")"
                        };
                        paymentIntentService.Update(invoice.PaymentIntentId, paymentintentUpdate);

                    }
                    catch (Exception ex)
                    {

                    }

                }

                for (var i = 0; i < files.Count(); i++)
                {
                    if (files.ElementAt(i) != null)
                    {
                        AbstractSessionDocument sessionDocument = new SessionDocument();
                        string imgName = string.Empty;
                        var file = files.ElementAt(i);
                        string path = "SessionDocuments/" + result.Item.Id + @"/";
                        string avatarfolder = Server.MapPath("~/" + path);
                        if (!Directory.Exists(avatarfolder))
                        {
                            Directory.CreateDirectory(avatarfolder);
                        }

                        if (file != null && file.ContentLength > 0)
                        {
                            imgName = DateTime.Now.ToString("ddMMyyyyhhmmss") + file.FileName;
                        }

                        string avatarpath = avatarfolder + imgName;
                        files.ElementAt(0).SaveAs(avatarpath);
                        sessionDocument.DocumentPath = path + imgName;
                        sessionDocument.SessionId = result.Item.Id;
                        var sessionDoc = sessionDocumentServices.InsertSessionDocument(sessionDocument);
                        TempData["openPopup"] = CommonHelper.ShowAlertMessageToastr(MessageType.success.ToString(), Messages.RecordSavedSuccessfully);
                    }
                }
            }
            ProjectSession.IsMsg = "1";
            return RedirectToAction(Actions.Index, Pages.Controllers.Dashboard, new {area="", showPopUp = "yes"});
        }

        public ActionResult MyAppointments()
        {
            return View();
        }

        public ActionResult DoctorsAppointments()
        {
            return View();
        }

        #region Completed Appointments
        public ActionResult CompletedAppointments(Sessions sessions)
        {
            return View();
        }

        [HttpPost]
        [ActionName(Actions.BindCompletedAppointments)]
        public JsonResult BindCompletedAppointments([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string DateSearch)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;
                
                PagedList<AbstractSessions> model = new PagedList<AbstractSessions>();
                if (ProjectSession.UserType == 3)
                {
                    model = sessionsService.SessionScheduleSelectByUserIdStatusId(pageParam, "6,10", null, DateSearch);
                }
                else
                {
                    model = sessionsService.SessionScheduleSelectByUserIdStatusId(pageParam, "6,10", ProjectSession.UserID, DateSearch);
                }

                totalRecord = (int)model.TotalRecords;
                filteredRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new object[] { Enums.MessageType.danger.GetHashCode(), Enums.MessageType.danger.ToString(), Messages.CommonErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Rejected Appointments
        public ActionResult Rejectedintments()
        {
            return View();
        }


        [HttpPost]
        [ActionName(Actions.BindRejectedAppointments)]
        public JsonResult BindRejectedAppointments([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string DateSearch = null)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;
                
                PagedList<AbstractSessions> model = new PagedList<AbstractSessions>();
                if (ProjectSession.UserType == 3)
                {
                    model = sessionsService.SessionScheduleSelectByUserIdStatusId(pageParam, "5,4", null, DateSearch);
                }
                else
                {
                    model = sessionsService.SessionScheduleSelectByUserIdStatusId(pageParam, "5,4", ProjectSession.UserID, DateSearch);
                }

                totalRecord = (int)model.TotalRecords;
                filteredRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new object[] { Enums.MessageType.danger.GetHashCode(), Enums.MessageType.danger.ToString(), Messages.CommonErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Upcoming Appointments
        public ActionResult UpcomingAppointments()
        {
            return View();
        }

        [HttpPost]
        [ActionName(Actions.BindUpcomingAppointments)]
        public JsonResult BindUpcomingAppointments([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string DateSearch)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;
                
                PagedList<AbstractSessions> model = new PagedList<AbstractSessions>();
                string StatusId = "2,3,9";
                if (ProjectSession.UserType == 3)
                {
                    StatusId = "3,9";
                }
                if (ProjectSession.UserType == 3)
                {
                    model = sessionsService.SessionScheduleSelectByUserIdStatusId(pageParam, StatusId, null, DateSearch);
                }
                else
                {
                    model = sessionsService.SessionScheduleSelectByUserIdStatusId(pageParam, StatusId, ProjectSession.UserID, DateSearch);
                }
                totalRecord = (int)model.TotalRecords;
                filteredRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new object[] { Enums.MessageType.danger.GetHashCode(), Enums.MessageType.danger.ToString(), Messages.CommonErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region New Appointments
        public ActionResult NewAppointments()
        {
            ViewBag.Doctor = BindDoctorsDdp();
            AbstractUsers users = usersService.UsersSelect(ProjectSession.UserID).Item;
            return View();
        }

        [HttpPost]
        [ActionName(Actions.BindNewAppointments)]
        public JsonResult BindNewAppointments([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string DateSearch)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;
                
                PagedList<AbstractSessions> model = new PagedList<AbstractSessions>();
                if (ProjectSession.UserType == 3)
                {
                    model = sessionsService.SessionScheduleSelectByUserIdStatusId(pageParam, "2", null, DateSearch);
                }
                else
                {
                    model = sessionsService.SessionScheduleSelectByUserIdStatusId(pageParam, "2", ProjectSession.UserID, DateSearch);
                }

                totalRecord = (int)model.TotalRecords;
                filteredRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new object[] { Enums.MessageType.danger.GetHashCode(), Enums.MessageType.danger.ToString(), Messages.CommonErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }

        public IList<SelectListItem> BindDoctorsDdp()
        {
            PageParam pageParam = new PageParam();
            pageParam.Offset = 0;
            pageParam.Limit = 0;

            var model = usersService.UsersSelectAll(pageParam, 1);
            model.Values = model.Values.Where(x => x.IsActive == 1).ToList();
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem() { Text = "Select Doctor", Value = "0" });

            foreach (var usersService in model.Values)
            {
                items.Add(new SelectListItem() { Text = usersService.FirstName + " " + usersService.LastName, Value = usersService.Id.ToString() });
            }

            return items;
        }
        #endregion
        
        #region Create Appointments
        public ActionResult CreateAppointment()
        {
            ViewBag.Doctor = BindDoctorsDdp();
            return View();
        }

        [HttpPost]
        [ActionName(Actions.BindCreateAppointment)]
        public JsonResult BindCreateAppointment([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel, string DateSearch)
        {
            try
            {
                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                //for default date search
                if (DateSearch == "")
                {
                    DateSearch = DateTime.Now.ToString("dd/MM/yyyy");
                }

                PagedList<AbstractSessionSchedule> model = new PagedList<AbstractSessionSchedule>();
                model = sessionsScheduleService.SessionScheduleSelectAllByDate(pageParam, DateSearch);

                totalRecord = (int)model.TotalRecords;
                filteredRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new object[] { Enums.MessageType.danger.GetHashCode(), Enums.MessageType.danger.ToString(), Messages.CommonErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }
        
        #endregion

        [HttpPost]
        public ActionResult SessionUpdateByStatusId(string Id, int StatusId, string RejectedReason, string CancelledReason, string AssignedTo)
        {
            int decryptedId = Convert.ToInt32(ConvertTo.Base64Decode(Id));
            PageParam pageParam = new PageParam();

            var session = sessionsService.SessionUpdateByStatusId(decryptedId, StatusId, RejectedReason, CancelledReason, AssignedTo);
            string body = string.Empty;
            string doctorbody = string.Empty;
            string PatientEmail = session.Item.PatientEmail;
            string PatientName = session.Item.PatientName;
            string DoctorEmail = session.Item.DoctorEmail;
            string DoctorName = session.Item.DoctorName;
            using (StreamReader reader = new StreamReader(Server.MapPath("~/EmailTemplates/Telemed.html")))
            {
                body = reader.ReadToEnd();
                doctorbody = body;
            }
            if (StatusId == 3 && session.Item != null)
            {
                if (PatientEmail != "" && DoctorEmail != "")
                {
                    string calenderbody = "";
                    if (session.Item.UserType != 4)
                    {
                        body = body.Replace("#titlemsg1", "Doctor Assigned");
                        body = body.Replace("#titlemsg2", "");
                        body = body.Replace("#name", PatientName);
                        body = body.Replace("#mainmsg", "Your appointment for " + session.Item.DisplayDate + " at " + session.Item.DisplayTime + " has been assigned to the doctor. For more information you can log into your account.");
                        EmailHelper.Send(PatientEmail, "", "", "Doctor Assigned", body);
                        CommonHelper.NotificationSent(session.Item.PatientDeviceToken, "Your appointment for " + session.Item.DisplayDate + " at " + session.Item.DisplayTime + " has been assigned to the doctor.", "Doctor Assigned");
                        calenderbody = "<h4>Greetings from Sirona Tele Med.</h4><br/><p>Hello "+PatientName+ ".</p><p>Your appointment for " + session.Item.DisplayDate + " at " + session.Item.DisplayTime + " has been assigned to the doctor. For more information you can log into your account.</p>";
                        SendTextMessage("Your appointment for " + session.Item.DisplayDate + " at " + session.Item.DisplayTime + " has been assigned to the doctor.", session.Item.PatientPhone, "");
                        AdminEmailService.Send(PatientEmail, "", "", "Doctor Assigned", "Your appointment for " + session.Item.DisplayDate + " at " + session.Item.DisplayTime + " has been assigned to the doctor. For more information you can log into your account",
                        null,null, session.Item.SessionStart,session.Item.SessionStart.Value.AddMinutes(30),calenderbody);
                       
                    }
                    else
                    {
                        body = body.Replace("#titlemsg1", "Doctor Assigned");
                        body = body.Replace("#titlemsg2", "");
                        body = body.Replace("#name", PatientName);
                        body = body.Replace("#mainmsg", "Your appointment for " + session.Item.DisplayDate + " at " + session.Item.DisplayTime + " has been assigned to the doctor. Doctor name is : "+DoctorName+". NOTE : You shall be notified once the doctor starts the appointment on the scheduled time.");
                        EmailHelper.Send(PatientEmail, "", "", "Doctor Assigned", body);                        
                        SendTextMessage("Your appointment for " + session.Item.DisplayDate + " at " + session.Item.DisplayTime + " has been assigned to the doctor.Doctor name is : " + DoctorName+".NOTE : You shall be notified once the doctor starts the appointment on the scheduled time.", session.Item.PatientPhone, "");
                        calenderbody = "<h4>Greetings from Sirona Tele Med.</h4><br/><p>Hello " + PatientName + ".</p><p>Your appointment for " + session.Item.DisplayDate + " at " + session.Item.DisplayTime + " has been assigned to the doctor. Doctor name is : " + DoctorName + ". NOTE : You shall be notified once the doctor starts the appointment on the scheduled time.</p>";
                        AdminEmailService.Send(PatientEmail, "", "", "Doctor Assigned", "Your appointment for " + session.Item.DisplayDate + " at " + session.Item.DisplayTime + " has been assigned to the doctor. Doctor name is : " + DoctorName + ". NOTE : You shall be notified once the doctor starts the appointment on the scheduled time.",
                        null, null, session.Item.SessionStart, session.Item.SessionStart.Value.AddMinutes(30), calenderbody);
                    }

                    doctorbody = doctorbody.Replace("#titlemsg1", "New Appointment Assigned");
                    doctorbody = doctorbody.Replace("#titlemsg2", "");
                    doctorbody = doctorbody.Replace("#name", DoctorName);
                    doctorbody = doctorbody.Replace("#mainmsg", "New appointment for " + session.Item.DisplayDate + " at " + session.Item.DisplayTime + " has been assigned you. For more information you can log into your account");
                    EmailHelper.Send(DoctorEmail, "", "", "New Appointment Assigned", doctorbody);
                    CommonHelper.NotificationSent(session.Item.DoctorDeviceToken, "New appointment for " + session.Item.DisplayDate + " at " + session.Item.DisplayTime + " has been assigned to you.", "New Appointment Assigned");
                    SendTextMessage("New appointment for " + session.Item.DisplayDate + " at " + session.Item.DisplayTime + " has been assigned you.", session.Item.DoctorPhone, "");
                }
            }
            if (StatusId == 4 && session.Item != null)
            {
                if (PatientEmail != "")
                {

                    body = body.Replace("#titlemsg1", "Appointment Cancelled");
                    body = body.Replace("#titlemsg2", "");
                    body = body.Replace("#name", PatientName);
                    if (CancelledReason != string.Empty)
                    {
                        body = body.Replace("#mainmsg", "Your appointment for " + session.Item.DisplayDate + " at " + session.Item.DisplayTime + " has been cancelled due to the following reason : " + CancelledReason);
                    }
                    else
                    {
                        body = body.Replace("#mainmsg", "Your appointment for " + session.Item.DisplayDate + " at " + session.Item.DisplayTime + " has been cancelled");
                    }
                    EmailHelper.Send(PatientEmail, "", "", "Appointment Cancelled", body);                    
                    CommonHelper.NotificationSent(session.Item.PatientDeviceToken, "Your appointment for " + session.Item.DisplayDate + " at " + session.Item.DisplayTime + " has been cancelled", "Appointment Cancelled");
                    SendTextMessage("Your appointment for " + session.Item.DisplayDate + " at " + session.Item.DisplayTime + " has been cancelled", session.Item.PatientPhone, "");
                }

                if (DoctorEmail != "")
                {
                    doctorbody = doctorbody.Replace("#titlemsg1", "Appointment Cancelled");
                    doctorbody = doctorbody.Replace("#titlemsg2", "");
                    doctorbody = doctorbody.Replace("#name", DoctorName);
                    if (CancelledReason != string.Empty)
                    {
                        doctorbody = doctorbody.Replace("#mainmsg", "Your appointment for " + session.Item.DisplayDate + " at " + session.Item.DisplayTime + " has been cancelled due to the following reason : " + CancelledReason);
                    }
                    else
                    {
                        doctorbody = doctorbody.Replace("#mainmsg", "Your appointment for " + session.Item.DisplayDate + " at " + session.Item.DisplayTime + " has been cancelled");
                    }
                    EmailHelper.Send(DoctorEmail, "", "", "Appointment Cancelled", doctorbody);
                    CommonHelper.NotificationSent(session.Item.DoctorDeviceToken, "Your appointment for " + session.Item.DisplayDate + " at " + session.Item.DisplayTime + " has been cancelled", "Appointment Cancelled");
                    SendTextMessage("Your appointment for " + session.Item.DisplayDate + " at " + session.Item.DisplayTime + " has been cancelled", session.Item.DoctorPhone, "");
                }

            }

            if (StatusId == 5 && session.Item != null)
            {
                if (PatientEmail != "")
                {
                    body = body.Replace("#titlemsg1", "Appointment Rejected");
                    body = body.Replace("#titlemsg2", "");
                    body = body.Replace("#name", PatientName);
                    if (RejectedReason != string.Empty)
                    {
                        body = body.Replace("#mainmsg", "Your appointment for " + session.Item.DisplayDate + " at " + session.Item.DisplayTime + " has been rejected due to the following reason : " + RejectedReason);
                    }
                    else
                    {
                        body = body.Replace("#mainmsg", "Your appointment for " + session.Item.DisplayDate + " at " + session.Item.DisplayTime + " has been rejected");
                    }

                    EmailHelper.Send(PatientEmail, "", "", "Appointment Rejected", body);
                    CommonHelper.NotificationSent(session.Item.PatientDeviceToken, "Your appointment for " + session.Item.DisplayDate + " at " + session.Item.DisplayTime + " has been rejected", "Appointment Rejected");
                    SendTextMessage("Your appointment for " + session.Item.DisplayDate + " at " + session.Item.DisplayTime + " has been rejected", session.Item.PatientPhone, "");
                }

            }

            return RedirectToAction(Actions.NewAppointments, Pages.Controllers.Appointment);
        }

        [HttpPost]
        public ActionResult SessionsScheduleUpdateByStatusId(string Id)
        {
            int decryptedId = Convert.ToInt32(ConvertTo.Base64Decode(Id));
            PageParam pageParam = new PageParam();

            var users = sessionsScheduleService.SesisonsScheduleIsAvailableUpdate(decryptedId);
            return RedirectToAction(Actions.CreateAppointment, Pages.Controllers.Appointment);
        }

        [HttpPost]
        public  JsonResult UpdateComments(Sessions sessions)
        {
            var result = usersService.UsersSelect(ProjectSession.UserID);
            if (sessions.IsSave == 1 && (sessions.vrfy_pass == result.Item.Password))
            {
                sessionsService.UpdateComments(sessions);
                ProjectSession.IsMsg = "1";
                return Json("", JsonRequestBehavior.AllowGet);
            }
            else if(sessions.IsSave == 0)
            {
                var session = sessionsService.UpdateComments(sessions);
                if(sessions.IsEndNow == 1)
                {
                    ProjectSession.UserID = session.Item.UserId;
                    string PatientEmail = session.Item.PatientEmail;
                    string PatientName = session.Item.PatientName;
                    string DoctorEmail = session.Item.DoctorEmail;
                    string DoctorName = session.Item.DoctorName;

                    string body = string.Empty;
                    string doctorbody = string.Empty;
                    using (StreamReader reader = new StreamReader(Server.MapPath("~/EmailTemplates/Telemed.html")))
                    {
                        body = reader.ReadToEnd();
                        doctorbody = body;
                    }
                    if (PatientEmail != "" && DoctorEmail != "")
                    {
                        body = body.Replace("#titlemsg1", "Completed Appointment");
                        body = body.Replace("#titlemsg2", "");
                        body = body.Replace("#name", PatientName);
                        body = body.Replace("#mainmsg", "Your appointment for " + session.Item.DisplayDate + " at " + session.Item.DisplayTime + " has been completed.");
                        EmailHelper.Send(PatientEmail, "", "", "Completed Appointment", body);
                        CommonHelper.NotificationSent(session.Item.PatientDeviceToken, "Your appointment for " + session.Item.DisplayDate + " at " + session.Item.DisplayTime + " has been completed.", "Completed Appointment");
                        SendTextMessage("Your appointment for " + session.Item.DisplayDate + " at " + session.Item.DisplayTime + " has been completed.", session.Item.PatientPhone, "");


                        doctorbody = doctorbody.Replace("#titlemsg1", "Completed Appointment");
                        doctorbody = doctorbody.Replace("#titlemsg2", "");
                        doctorbody = doctorbody.Replace("#name", DoctorName);
                        doctorbody = doctorbody.Replace("#mainmsg", "" + session.Item.PatientName + "appointment for " + session.Item.DisplayDate + " at " + session.Item.DisplayTime + " has been completed.");
                        EmailHelper.Send(DoctorEmail, "", "", "Completed Appointment", doctorbody);
                        CommonHelper.NotificationSent(session.Item.DoctorDeviceToken, "" + session.Item.PatientName + "'s appointment for " + session.Item.DisplayDate + " at " + session.Item.DisplayTime + " has been completed.", "Completed Appointment");
                        SendTextMessage("" + session.Item.PatientName + "'s appointment for " + session.Item.DisplayDate + " at " + session.Item.DisplayTime + " has been completed.", session.Item.DoctorPhone, "");

                       // string returnResult = await Epriscription();

                        
                    }
                }
                return Json("", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Not Auth", JsonRequestBehavior.AllowGet);
            }
            
        }

        public  async Task<string> Epriscription()
        {
            NCScript nCScript = new NCScript();

            nCScript.Credentials = new Credentials();
            nCScript.Credentials.PartnerName = "demo";
            nCScript.Credentials.Name = "demo";
            nCScript.Credentials.Password = "demo";
            nCScript.Credentials.ProductName = "SuperDuperSoftware";
            nCScript.Credentials.ProductVersion = "V5.3";

            nCScript.UserRole = new UserRole();
            nCScript.UserRole.User = "LicensedPrescriber";
            nCScript.UserRole.Role = "doctor";

            nCScript.Destination = new Destination();
            nCScript.Destination.RequestedPage = "compose";

            nCScript.Account = new Account();
            nCScript.Account.AccountName = "Demo";
            nCScript.Account.SiteID = "Demo";
            nCScript.Account.AccountAddress = new AccountAddress();
            nCScript.Account.AccountAddress.Address1 = "232323 Test";
            nCScript.Account.AccountAddress.Address2 = "Suite 420";
            nCScript.Account.AccountAddress.City = "Boston";
            nCScript.Account.AccountAddress.State = "MA";
            nCScript.Account.AccountAddress.Zip = "10409";
            nCScript.Account.AccountAddress.Zip = "1234";
            nCScript.Account.AccountAddress.Country = "US";
            nCScript.Account.AccountPrimaryPhoneNumber = "5555551212";
            nCScript.Account.AccountPrimaryFaxNumber = "5555551313";


            nCScript.Location = new Location();
            nCScript.Location.LocationName = "DEMO";
            nCScript.Location.LocationName = "DEMO";
            nCScript.Location.LocationAddress = new LocationAddress();
            nCScript.Location.LocationAddress.Address1 = "232323 Test";
            nCScript.Location.LocationAddress.Address2 = "Suite 240";
            nCScript.Location.LocationAddress.City = "Boston";
            nCScript.Location.LocationAddress.State = "MA";
            nCScript.Location.LocationAddress.Zip = "10409";
            nCScript.Location.LocationAddress.Zip4 = "1234";
            nCScript.Location.LocationAddress.Country = "US";
            nCScript.Location.PrimaryPhoneNumber = "5555551212";
            nCScript.Location.PrimaryPhoneNumber = "5555551212";
            nCScript.Location.PrimaryFaxNumber = "5555551213";
            nCScript.Location.PharmacyContactNumber = "5555551212";

            nCScript.LicensedPrescriber = new LicensedPrescriber();
            nCScript.LicensedPrescriber.LicensedPrescriberName = new LicensedPrescriberName();
            nCScript.LicensedPrescriber.LicensedPrescriberName.Last = "Smith";
            nCScript.LicensedPrescriber.LicensedPrescriberName.First = "Doctor";
            nCScript.LicensedPrescriber.LicensedPrescriberName.Middle = "J";
            nCScript.LicensedPrescriber.Dea = "AS1111111";
            nCScript.LicensedPrescriber.Upin = "12345678";
            nCScript.LicensedPrescriber.LicenseState = "TX";
            nCScript.LicensedPrescriber.LicenseNumber = "12345678";

            nCScript.Patient = new Patient();
            nCScript.Patient.PatientName = new PatientName();
            nCScript.Patient.PatientName.Last = "Wilson";
            nCScript.Patient.PatientName.First = "Patient";
            nCScript.Patient.PatientName.Middle = "J";
            nCScript.Patient.MedicalRecordNumber = "123456";
            nCScript.Patient.Memo = "Picks Up Meds At VA";

            nCScript.Patient.PatientAddress = new PatientAddress();
            nCScript.Patient.PatientAddress.Address1 = "23233 Test";
            nCScript.Patient.PatientAddress.Address2 = "Suite 240";
            nCScript.Patient.PatientAddress.City = "Boston";
            nCScript.Patient.PatientAddress.State = "MA";
            nCScript.Patient.PatientAddress.Zip = "10455";
            nCScript.Patient.PatientAddress.Country = "US";

            nCScript.Patient.PatientContact = new PatientContact();
            nCScript.Patient.PatientContact.HomeTelephone = "1234567890";

            nCScript.Patient.PatientCharacteristics = new PatientCharacteristics();
            nCScript.Patient.PatientCharacteristics.Dob = "19800115";
            nCScript.Patient.PatientCharacteristics.Gender = "M";






            string xml = GetXMLFromObject(nCScript);

            using (StreamReader reader = new StreamReader(Server.MapPath("~/EmailTemplates/SampleXml.txt")))
            {
                xml = reader.ReadToEnd();
            }

            var request = (HttpWebRequest)WebRequest.Create("http://preproduction.newcropaccounts.com/interfaceV7/rxentry.aspx");

            //var postData = "thing1=" + Uri.EscapeDataString("hello");
            //postData += "&thing2=" + Uri.EscapeDataString("world");
            var data = Encoding.ASCII.GetBytes(xml);

            request.Method = "POST";
            request.ContentType = "application/xml";
            request.ContentLength = data.Length;

            using (var stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }

            var response = (HttpWebResponse)request.GetResponse();

            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

            //var httpContent = new StringContent(xml, Encoding.UTF8, "application/xml");
            //HttpClient client1 = new HttpClient();
            //var response = await client1.PostAsync("http://preproduction.newcropaccounts.com/interfaceV7/rxentry.aspx", httpContent);
            //if (!response.IsSuccessStatusCode)
            //{
            //    //throw new InvalidUriException(string.Format("Invalid uri: {0}", requestUri));
            //}

            return "";
        }
        
        [HttpPost]
        public ActionResult StartCall(string sid = "0")
        {
            try
            {
                int Id = Convert.ToInt32(ConvertTo.Base64Decode(sid));
                if (Id > 0)
                {
                    SuccessResult<AbstractSessions> successResult = sessionsService.SessionsSelect(Id);
                    TwilioClient.Init(Configurations.TwilioAccountSid, Configurations.AuthToken);

                    CreateRoomOptions crr = new CreateRoomOptions();
                    crr.Type = "group";
                    crr.UniqueName = "room" + Id;
                    crr.MaxParticipants = 4;

                    try
                    {
                        RoomResource.Create(crr);
                        var session = sessionsService.SessionUpdateByStatusId(Id, 9, "", "", "0");
                        if (session.Item != null)
                        {
                            if (successResult.Item.UserType != 4)
                            {
                                EmailHelper.Send(successResult.Item.PatientEmail, "", "", "Appointment Started",
                               "Hello " + successResult.Item.PatientName + ", your appointment with Dr." + successResult.Item.DoctorName + " for " + successResult.Item.DisplayDate + " at " + successResult.Item.DisplayTime + " has started");

                                CommonHelper.NotificationSent(successResult.Item.PatientDeviceToken,
                                    "Hello " + successResult.Item.PatientName + ", your appointment with Dr." + successResult.Item.DoctorName + " for " + successResult.Item.DisplayDate + " at " + successResult.Item.DisplayTime + " has started", "Appointment Started", Convert.ToString(Id));

                                SendTextMessage("Hello " + successResult.Item.PatientName + ", your appointment with Dr." + successResult.Item.DoctorName + " for " + successResult.Item.DisplayDate + " at " + successResult.Item.DisplayTime + " has started", successResult.Item.PatientPhone, "");
                            }
                            else
                            {
                                string link = Configurations.ClientUrl+"Register/JoinCall/"+ConvertTo.Base64Encode(successResult.Item.Id.ToString()); 
                                string html = "<html><head></head><body><p>Hello " + successResult.Item.PatientName + ", your appointment with Dr." + successResult.Item.DoctorName + " for " + successResult.Item.DisplayDate + " at " + successResult.Item.DisplayTime + " has started.Kindly click on the Join Appointment link below.</p><br/><a href='"+link+"'>CLICK HERE TO JOIN APPOINTMENT</a></body></html>";
                                EmailHelper.Send(successResult.Item.PatientEmail, "", "", "Appointment Started",html);
                                
                                SendTextMessage("Hello " + successResult.Item.PatientName + ", your appointment with Dr." + successResult.Item.DoctorName + " for " + successResult.Item.DisplayDate + " at " + successResult.Item.DisplayTime + " has started.Click the following link to join the appointment."+link, successResult.Item.PatientPhone, "");
                            }
                        }
                        else
                        {
                            return Json(null, JsonRequestBehavior.AllowGet);
                        }

                    }
                    catch (Exception exx)
                    {
                        var session = sessionsService.SessionUpdateByStatusId(Id, 9, "", "", "0");
                        if (session.Item != null)
                        {
                            EmailHelper.Send(successResult.Item.PatientEmail, "", "", "Appointment Started",
                            "Hello " + successResult.Item.PatientName + ", your appointment with Dr." + successResult.Item.DoctorName + " for " + successResult.Item.DisplayDate + " at " + successResult.Item.DisplayTime + " has started");

                            CommonHelper.NotificationSent(successResult.Item.PatientDeviceToken,
                                "Hello " + successResult.Item.PatientName + ", your appointment with Dr." + successResult.Item.DoctorName + " for " + successResult.Item.DisplayDate + " at " + successResult.Item.DisplayTime + " has started", "Appointment Started", Convert.ToString(Id));

                            SendTextMessage("Hello " + successResult.Item.PatientName + ", your appointment with Dr." + successResult.Item.DoctorName + " for " + successResult.Item.DisplayDate + " at " + successResult.Item.DisplayTime + " has started", successResult.Item.PatientPhone, "");
                        }
                        else
                        {
                            return Json(null, JsonRequestBehavior.AllowGet);
                        }
                    }
                    return Json("", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(null, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult JoinCall(string id)
        {
            ViewBag.LinkableId = Configurations.VideoCall + Convert.ToString(ConvertTo.Base64Decode(id));
            return View();
        }

        public static string GetXMLFromObject(object o)
        {
            StringWriter sw = new StringWriter();
            XmlTextWriter tw = null;
            try
            {
                XmlSerializer serializer = new XmlSerializer(o.GetType());
                tw = new XmlTextWriter(sw);
                serializer.Serialize(tw, o);
            }
            catch (Exception ex)
            {
                //Handle Exception Code
            }
            finally
            {
                sw.Close();
                if (tw != null)
                {
                    tw.Close();
                }
            }
            return sw.ToString();
        }

        [HttpGet]
        public ActionResult ViewCompletedAppointment(int id, int SSid = 0)
        {
            AbstractSessions sessions = new Sessions();
            if (id == 0)
            {
                PageParam pageParam = new PageParam();
                sessions.SessionScheduleId = SSid;
                var sessionschedule = sessionsScheduleService.SessionScheduleSelect(sessions.SessionScheduleId);
                if (sessionschedule != null)
                {
                    sessions.SessionStart = sessionschedule.Item.SessionStart;
                }
                return View(sessions);
            }
            else
            {
                sessions = sessionsService.SessionsSelect(id).Item;
                sessions.UserDescription = sessions.UUserDescription;
                sessions.SpecialRequests = sessions.SSpecialRequests;
                sessions.DoctorComments = sessions.DDoctorComments;
                sessions.DoctorComments2 = sessions.DDoctorComments2;
                sessions.DoctorComments3 = sessions.DDoctorComments3;
                sessions.DoctorComments4 = sessions.DDoctorComments4;
                if (sessions == null)
                {
                    return HttpNotFound();
                }
                return View(sessions);
            }
        }
    }
       
    #region E-Prescription Class
    [XmlRoot(ElementName = "Credentials", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
    public class Credentials
    {
        [XmlElement(ElementName = "partnerName", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public string PartnerName { get; set; }
        [XmlElement(ElementName = "name", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public string Name { get; set; }
        [XmlElement(ElementName = "password", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public string Password { get; set; }
        [XmlElement(ElementName = "productName", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public string ProductName { get; set; }
        [XmlElement(ElementName = "productVersion", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public string ProductVersion { get; set; }
    }

    [XmlRoot(ElementName = "UserRole", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
    public class UserRole
    {
        [XmlElement(ElementName = "user", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public string User { get; set; }
        [XmlElement(ElementName = "role", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public string Role { get; set; }
    }

    [XmlRoot(ElementName = "Destination", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
    public class Destination
    {
        [XmlElement(ElementName = "requestedPage", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public string RequestedPage { get; set; }
    }

    [XmlRoot(ElementName = "AccountAddress", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
    public class AccountAddress
    {
        [XmlElement(ElementName = "address1", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public string Address1 { get; set; }
        [XmlElement(ElementName = "address2", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public string Address2 { get; set; }
        [XmlElement(ElementName = "city", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public string City { get; set; }
        [XmlElement(ElementName = "state", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public string State { get; set; }
        [XmlElement(ElementName = "zip", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public string Zip { get; set; }
        [XmlElement(ElementName = "zip4", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public string Zip4 { get; set; }
        [XmlElement(ElementName = "country", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public string Country { get; set; }
    }

    [XmlRoot(ElementName = "Account", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
    public class Account
    {
        [XmlElement(ElementName = "accountName", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public string AccountName { get; set; }
        [XmlElement(ElementName = "siteID", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public string SiteID { get; set; }
        [XmlElement(ElementName = "AccountAddress", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public AccountAddress AccountAddress { get; set; }
        [XmlElement(ElementName = "accountPrimaryPhoneNumber", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public string AccountPrimaryPhoneNumber { get; set; }
        [XmlElement(ElementName = "accountPrimaryFaxNumber", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public string AccountPrimaryFaxNumber { get; set; }
        [XmlAttribute(AttributeName = "ID")]
        public string ID { get; set; }
    }

    [XmlRoot(ElementName = "LocationAddress", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
    public class LocationAddress
    {
        [XmlElement(ElementName = "address1", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public string Address1 { get; set; }
        [XmlElement(ElementName = "address2", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public string Address2 { get; set; }
        [XmlElement(ElementName = "city", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public string City { get; set; }
        [XmlElement(ElementName = "state", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public string State { get; set; }
        [XmlElement(ElementName = "zip", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public string Zip { get; set; }
        [XmlElement(ElementName = "zip4", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public string Zip4 { get; set; }
        [XmlElement(ElementName = "country", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public string Country { get; set; }
    }

    [XmlRoot(ElementName = "Location", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
    public class Location
    {
        [XmlElement(ElementName = "locationName", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public string LocationName { get; set; }
        [XmlElement(ElementName = "LocationAddress", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public LocationAddress LocationAddress { get; set; }
        [XmlElement(ElementName = "primaryPhoneNumber", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public string PrimaryPhoneNumber { get; set; }
        [XmlElement(ElementName = "primaryFaxNumber", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public string PrimaryFaxNumber { get; set; }
        [XmlElement(ElementName = "pharmacyContactNumber", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public string PharmacyContactNumber { get; set; }
        [XmlAttribute(AttributeName = "ID")]
        public string ID { get; set; }
    }

    [XmlRoot(ElementName = "LicensedPrescriberName", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
    public class LicensedPrescriberName
    {
        [XmlElement(ElementName = "last", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public string Last { get; set; }
        [XmlElement(ElementName = "first", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public string First { get; set; }
        [XmlElement(ElementName = "middle", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public string Middle { get; set; }
    }

    [XmlRoot(ElementName = "LicensedPrescriber", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
    public class LicensedPrescriber
    {
        [XmlElement(ElementName = "LicensedPrescriberName", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public LicensedPrescriberName LicensedPrescriberName { get; set; }
        [XmlElement(ElementName = "dea", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public string Dea { get; set; }
        [XmlElement(ElementName = "upin", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public string Upin { get; set; }
        [XmlElement(ElementName = "licenseState", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public string LicenseState { get; set; }
        [XmlElement(ElementName = "licenseNumber", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public string LicenseNumber { get; set; }
        [XmlElement(ElementName = "npi", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public string Npi { get; set; }
        [XmlAttribute(AttributeName = "ID")]
        public string ID { get; set; }
    }

    [XmlRoot(ElementName = "PatientName", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
    public class PatientName
    {
        [XmlElement(ElementName = "last", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public string Last { get; set; }
        [XmlElement(ElementName = "first", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public string First { get; set; }
        [XmlElement(ElementName = "middle", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public string Middle { get; set; }
    }

    [XmlRoot(ElementName = "PatientAddress", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
    public class PatientAddress
    {
        [XmlElement(ElementName = "address1", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public string Address1 { get; set; }
        [XmlElement(ElementName = "address2", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public string Address2 { get; set; }
        [XmlElement(ElementName = "city", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public string City { get; set; }
        [XmlElement(ElementName = "state", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public string State { get; set; }
        [XmlElement(ElementName = "zip", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public string Zip { get; set; }
        [XmlElement(ElementName = "country", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public string Country { get; set; }
    }

    [XmlRoot(ElementName = "PatientContact", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
    public class PatientContact
    {
        [XmlElement(ElementName = "homeTelephone", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public string HomeTelephone { get; set; }
    }

    [XmlRoot(ElementName = "PatientCharacteristics", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
    public class PatientCharacteristics
    {
        [XmlElement(ElementName = "dob", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public string Dob { get; set; }
        [XmlElement(ElementName = "gender", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public string Gender { get; set; }
    }

    [XmlRoot(ElementName = "Patient", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
    public class Patient
    {
        [XmlElement(ElementName = "PatientName", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public PatientName PatientName { get; set; }
        [XmlElement(ElementName = "medicalRecordNumber", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public string MedicalRecordNumber { get; set; }
        [XmlElement(ElementName = "memo", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public string Memo { get; set; }
        [XmlElement(ElementName = "PatientAddress", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public PatientAddress PatientAddress { get; set; }
        [XmlElement(ElementName = "PatientContact", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public PatientContact PatientContact { get; set; }
        [XmlElement(ElementName = "PatientCharacteristics", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public PatientCharacteristics PatientCharacteristics { get; set; }
        [XmlAttribute(AttributeName = "ID")]
        public string ID { get; set; }
    }

    [XmlRoot(ElementName = "NCScript", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
    public class NCScript
    {
        [XmlElement(ElementName = "Credentials", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public Credentials Credentials { get; set; }
        [XmlElement(ElementName = "UserRole", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public UserRole UserRole { get; set; }
        [XmlElement(ElementName = "Destination", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public Destination Destination { get; set; }
        [XmlElement(ElementName = "Account", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public Account Account { get; set; }
        [XmlElement(ElementName = "Location", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public Location Location { get; set; }
        [XmlElement(ElementName = "LicensedPrescriber", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public LicensedPrescriber LicensedPrescriber { get; set; }
        [XmlElement(ElementName = "Patient", Namespace = "http://secure.newcropaccounts.com/interfaceV7")]
        public Patient Patient { get; set; }
        [XmlAttribute(AttributeName = "xmlns")]
        public string Xmlns { get; set; }
        [XmlAttribute(AttributeName = "NCStandard", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string NCStandard { get; set; }
        [XmlAttribute(AttributeName = "xsi", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Xsi { get; set; }
    }

    #endregion
}