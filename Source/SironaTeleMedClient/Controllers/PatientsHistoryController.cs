﻿using DataTables.Mvc;
using SironaTeleMed.Common;
using SironaTeleMed.Common.Paging;
using SironaTeleMed.Entities.Contract;
using SironaTeleMed.Entities.V1;
using SironaTeleMed.Services.Contract;
using SironaTeleMedClient.Infrastructure;
using SironaTeleMedClient.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SironaTeleMedClient.Controllers
{
    public class PatientsHistoryController : Controller
    {
        private readonly AbstractUsersService usersService;
        private readonly AbstractSessionsServices sessionsService;        
        private readonly AbstractMasterStatusServices masterStatusServices;
        // GET: PatientsHistory

        public PatientsHistoryController(AbstractUsersService usersService, AbstractSessionsServices sessionsService, AbstractMasterStatusServices masterStatusServices)
        {
            this.usersService = usersService;
            this.sessionsService = sessionsService;        
            this.masterStatusServices = masterStatusServices;
        }

        public ActionResult Index()
        {
            ViewBag.Users = BindPatientsDdp();
            ViewBag.Status = BindStatusDdp();
            return View();
        }

        [HttpPost]
        [ActionName(Actions.BindPatientHistory)]
        public JsonResult BindPatientHistory([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel,string StatusId,string UserId,string DateSearch)
        {
            try
            {
                int decryptedStatusId = Convert.ToInt32(ConvertTo.Base64Decode(Convert.ToString(StatusId))); 
                int decryptedUserId = Convert.ToInt32(ConvertTo.Base64Decode(Convert.ToString(UserId)));

                int totalRecord = 0;
                int filteredRecord = 0;

                PageParam pageParam = new PageParam();
                pageParam.Offset = requestModel.Start;
                pageParam.Limit = requestModel.Length;

                var model = sessionsService.SearchPatientsHistory(pageParam, decryptedStatusId,decryptedUserId,DateSearch);
                
                totalRecord = (int)model.TotalRecords;
                filteredRecord = (int)model.TotalRecords;

                return Json(new DataTablesResponse(requestModel.Draw, model.Values, filteredRecord, totalRecord), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new object[] { Enums.MessageType.danger.GetHashCode(), Enums.MessageType.danger.ToString(), Messages.CommonErrorMessage }, JsonRequestBehavior.AllowGet);
            }
        }

        public IList<SelectListItem> BindPatientsDdp()
        {
            PagedList<AbstractUsers> pagedList = null;
            PageParam pageParam = new PageParam();

            if (ProjectSession.UserType == 3)
            {
                pagedList = usersService.UsersSelectByAssignId(pageParam, null);
            }
            else if(ProjectSession.UserType == 1)
            {
                pagedList = usersService.UsersSelectByAssignId(pageParam, ProjectSession.UserID);                
            }


            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem() { Text = "Select Patients", Value = ConvertTo.Base64Encode("0") });

            foreach (var usersService in pagedList.Values)
            {
                string encryptId = ConvertTo.Base64Encode(usersService.Id.ToString());
                items.Add(new SelectListItem() { Text = Convert.ToString(usersService.FirstName) + " " + Convert.ToString(usersService.LastName), Value = encryptId });
            }

            return items;
        }

        public IList<SelectListItem> BindStatusDdp()
        {
            PageParam pageParam = new PageParam();

            var model = masterStatusServices.MasterStatusSelectAll(pageParam);

            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem() { Text = "Select Status", Value = ConvertTo.Base64Encode("0") });
            model.Values = model.Values.Where(x => x.Id != 7 && x.Id !=8).ToList();
            foreach (var masterStatusServices in model.Values)
            {
                string encryptId = ConvertTo.Base64Encode(masterStatusServices.Id.ToString());
                items.Add(new SelectListItem() { Text = masterStatusServices.Status.ToString(), Value = encryptId });
            }
            return items;
        }
    }
}