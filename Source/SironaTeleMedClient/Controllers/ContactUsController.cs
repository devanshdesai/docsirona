﻿using SironaTeleMed.Common;
using SironaTeleMed.Entities.V1;
using SironaTeleMedClient.Pages;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SironaTeleMedClient.Controllers
{
    public class ContactUsController : Controller
    {

        // GET: ContactUs
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ThankYou()
        {
            return View();
        }

        [HttpPost]
        public ActionResult WebContact(WebContactUs webContactUs)
        {
            if (ModelState.IsValid)
            {
                string staticEmail = "devplexoc@gmail.com";

                if (webContactUs.Email != "")
                {
                    string body = string.Empty;
                    body = "<html>" +
                            "<head></head>" +
                            "<body>" +
                            "<h4>Name : " + webContactUs.Name + "</h4>" +
                            "<h4>Email : " + webContactUs.Email + "</h4>" +
                            "<h4>Phone : " + webContactUs.Phone + "</h4>" +
                            "<h4>Subject : " + webContactUs.Subject + "</h4>" +
                            "<h4>Messsage : " + webContactUs.Message + "</h4>" +
                            "</body>" +
                            "</html>";
                    EmailHelper.Send(staticEmail, "", "", "New Inquiry", body);
                }
                return RedirectToAction(Actions.ThankYou, Pages.Controllers.ContactUs);
            }
            else
            {
                return PartialView("Index");
            }
        }
    }
}