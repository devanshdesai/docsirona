﻿using SironaTeleMed.Common;
using SironaTeleMed.Services.Contract;
using SironaTeleMedClient.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SironaTeleMedClient.Controllers
{
    public class LogoutController : Controller
    {
        #region OldCode
        
        [AllowAnonymous]
        [ActionName(Actions.Logout)]
        public ActionResult Logout()
        {
            ProjectSession.UserID = 0;
            ProjectSession.SubscriptionId = 0;
            Session.Clear();
            Session.Abandon();

            string[] ck = Request.Cookies.AllKeys;
            foreach (string cookie in ck)
            {
                Response.Cookies[cookie].Expires = DateTime.Now.AddDays(-1);
            }

            return RedirectToAction(Actions.Index, Pages.Controllers.Login);
        }

        #endregion
    }
}