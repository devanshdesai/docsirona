﻿using Autofac;
using SironaTeleMed.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace SironaTeleMedClient
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            //try
            //{
            //    bool isSecureConnectionLoadBalanced = String.Equals(Request.ServerVariables["HTTP_X_FORWARDED_PROTO"], "https", StringComparison.OrdinalIgnoreCase);
            //    if (!isSecureConnectionLoadBalanced)
            //    {
            //        Response.Redirect(Request.Url.AbsoluteUri.Replace("http://", "https://"));
            //    }
            //}
            //catch (Exception ex)
            //{
            //    CommonHelper.LogError(Server.MapPath("~/ErrorLog/GlobalErrorLog.txt"), ex);
            //}
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //NLog.Config.ConfigurationItemFactory.Default.LayoutRenderers.RegisterDefinition("mdlc", typeof(MdlcLayoutRenderer));

            //GlobalConfiguration.Configure(WebApiConfig.Register);           

            AntiForgeryConfig.UniqueClaimTypeIdentifier = ClaimTypes.NameIdentifier;
            ModelMetadataProviders.Current = new CachedDataAnnotationsModelMetadataProvider();
            ContainerBuilder builder = new ContainerBuilder();

            Bootstrapper.Resolve(builder);
        }

        protected void Application_Error()
        {
            try
            {


                //Code that runs when an unhandled error occurs
                Exception ErrorInfo = Server.GetLastError().GetBaseException();
                CommonHelper.LogError(Server.MapPath("~/ErrorLog/ErrorLog.txt"), ErrorInfo);
                Server.ClearError();
                //if (Request.RequestContext.RouteData.DataTokens["area"] != null && Request.RequestContext.RouteData.DataTokens["area"].ToString().ToLower() == Pages.Areas.Admin.ToLower())
                //    Response.Redirect(CommonHelper.UrlBase + Pages.Areas.Admin + "/" + Pages.Controllers.Account + "/" + Pages.Actions.Error);
                //else
                //    Response.Redirect(CommonHelper.UrlBase + Pages.Controllers.Account + "/" + Pages.Actions.Error);
            }
            catch (Exception ex)
            {

            }
        }

        /// <summary>
        /// This method is called on application end request
        /// </summary>
        public void Application_EndRequest()
        {
            //HttpContextLifecycle.DisposeAndClearAll();            
            //EngineContext.Container.Dispose();
            //EngineContext.Container = null;            
        }
    }
}
