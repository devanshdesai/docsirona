/* ========================================================================

Sweet Alert Init

=========================================================================
 */


"use strict";


/*======== Doucument Ready Function =========*/
jQuery(document).ready(function ($) {

    // Success Alert
    $(".sweet_success_btn").on("click", function () {
        swal({
            type: "success",
            title: "Success!",
            text: "Appointment has been Assigned",
            buttonsStyling: !1 ,
            confirmButtonClass: "btn btn-success"
        }).then((result) => {
            if (result.value) {               
                window.location.reload();
            }
        })
    });

    // create Alert
    $(".sweet_create_success_btn").on("click", function () {
        swal({
            type: "success",
            title: "Success!",
            text: "Your Appointment has been booked",
            buttonsStyling: !1,
            confirmButtonClass: "btn btn-success"
        }).then((result) => {
            if (result.value) {
                window.location.replace('/Dashboard/index');
            }
        })
    });
    // Appointment save as draft success    $(".sweet_create_saveasdraft_btn").on("click", function () {        swal({            type: "success",            title: "Success!",            text: "Drafted Saved Successfully",            buttonsStyling: !1,            confirmButtonClass: "d-none"        }).then((result) => {            if (result.value) {                //window.location.replace('/Dashboard/index');            }        })    });

    // Reject Sucess Alert
    $(".sweet_success_reject_sucess").on("click", function () {
        swal({
            type: "success",
            title: "Success!",
            text: "Appointment Rejected Successfully",
            buttonsStyling: !1,
            confirmButtonClass: "btn btn-success"
        }).then((result) => {
            if (result.value) {
                window.location.reload();
            }
        })
    });



    // Cancel Sucess Alert
    $(".sweet_success_cancel_sucess").on("click", function () {
        swal({
            type: "success",
            title: "Success!",
            text: "Appointment Cancelled Successfully",
            buttonsStyling: !1,
            confirmButtonClass: "btn btn-success"
        }).then((result) => {
            if (result.value) {
                window.location.reload();
            }
        })
    });

    // Request Alert
    $(".sweet_alert_request_accept").on("click", function () {
        swal({
            type: "success",
            title: "Success!",
            text: "Request accepted successfully",
            buttonsStyling: !1,
            confirmButtonClass: "btn btn-success"
        }).then((result) => {
            if (result.value) {

                window.location.reload();
            }
        })
    });

    // Info Alert
    $(".sweet_info_btn").on("click", function () {
        swal({
            type: "info",
            title: "Info Alert!",
            text: "Here is the info alert text",
            buttonsStyling: !1,
            confirmButtonClass: "btn btn-info"
        })

    });

    // Appointment Alert with Confirmation
    $(".sweet_info_appointment_available").on("click", function () {
        swal({
            title: "Alert!",
            text: "Are you sure you want to change the status to Not Available",
            type: "info",
            showCancelButton: !0,
            cancelButtonText: "No",
            confirmButtonText: "Yes",
            confirmButtonClass: "btn btn-info mr-5",
            cancelButtonClass: "btn btn-danger",
            buttonsStyling: !1
        }).then((result) => {
            if (result.value) {
                swal("Success!", "Doctor Status Changed", "success", "btn btn-success")
            } else if (
                // Read more about handling dismissals
                result.dismiss === Swal.DismissReason.cancel
            ) {
                // swal("Cancelled", "Your imaginary file is safe :)", "error")
            }
        })
    });

    // Appointment Alert with Confirmation
    $(".sweet_info_appointment_notavailable").on("click", function () {
        swal({
            title: "Alert!",
            text: "Are you sure you want to change the status to Available",
            type: "info",
            showCancelButton: !0,
            cancelButtonText: "No",
            confirmButtonText: "Yes",
            confirmButtonClass: "btn btn-info mr-5",
            cancelButtonClass: "btn btn-danger",
            buttonsStyling: !1
        }).then((result) => {
            if (result.value) {
                swal("Success!", "Doctor Status Changed", "success", "btn btn-success")
            } else if (
                // Read more about handling dismissals
                result.dismiss === Swal.DismissReason.cancel
            ) {
                // swal("Cancelled", "Your imaginary file is safe :)", "error")
            }
        })
    });

    // Warning Alert
    $(".sweet_warning_btn").on("click", function () {
       
        swal({
            type: "warning",
            title: "Warning",
            text: "Here is the warning alert text",
            buttonsStyling: !1,
            confirmButtonClass: "btn btn-warning"
        })
    });

    // Error Alert
    $(".sweet_danger_btn").on("click", function () {
        swal({
            type: "error",
            title: "Error!",
            text: "Something went wrong!",
            confirmButtonText: "Dismiss",
            buttonsStyling: !1,
            confirmButtonClass: "btn btn-danger"
        })
    });

    // Basic Alert
    $(".sweet_basic_btn").on("click", function () {
        swal("There is Something going.")
    });

    // Alert With Title
    $(".alert_title").on("click", function () {
        swal("The Internet?", "That thing is still around?")
    });

    // Alert with Timer
    $(".sweet_alert_timer").on("click", function () {
        swal({
            title: "Auto close alert!", html: "I will close in <strong>2</strong> seconds.", timer: 2e3
        }).then(t => {
            t.dismiss === swal.DismissReason.timer && console.log("I was closed by the timer")
        })
    });
    

    // Alert with Confirmation
    $(".sweet_alert_confirm_reject").on("click", function () {
        swal({
            title: "Are you sure?",
            text: "Are you sure you want to reject this appointment",
            type: "error",
            showCancelButton: !0,
            cancelButtonText: "No",
            confirmButtonText: "Yes",
            confirmButtonClass: "btn btn-info mr-5",
            cancelButtonClass: "btn btn-danger",
            buttonsStyling: !1
        }).then((result) => {
            if (result.value) {
                $('#RejectedReason').modal();
                //swal("Success!", "This Appointment has been Rejected", "success", "btn btn-success")
            } else if ( 
                // Read more about handling dismissals
                result.dismiss === Swal.DismissReason.cancel
            ) {
                // swal("Cancelled", "Your imaginary file is safe :)", "error")
            }
        })
    });


    // Alert with Confirmation
    $(".sweet_alert_confirm_cancelled").on("click", function () {
        swal({
            title: "Are you sure?",
            text: "Are you sure you want to cancel this appointment",
            type: "error",
            showCancelButton: !0,
            cancelButtonText: "No",
            confirmButtonText: "Yes",
            confirmButtonClass: "btn btn-info mr-5",
            cancelButtonClass: "btn btn-danger",
            buttonsStyling: !1
        }).then((result) => {
            if (result.value) {
                $('#CancelledReason').modal();
                //swal("Success!", "This Appointment has been Cancelled", "success", "btn btn-success")
            } else if (
                // Read more about handling dismissals
                result.dismiss === Swal.DismissReason.cancel
            ) {
                // swal("Cancelled", "Your imaginary file is safe :)", "error")
            }
        })
    });

    // Alert with Confirmation
    $(".sweet_alert_confirm").on("click", function () {
        swal({
            title: "Info!",
            text: "By booking this appointment an amount of $45 shall be chanrged. Are you sure you want to continue?",
            type: "info",
            showCancelButton: !0,
            cancelButtonText: "No",
            confirmButtonText: "Yes",
            confirmButtonClass: "btn btn-info mr-5",
            cancelButtonClass: "btn btn-danger",
            buttonsStyling: !1
        }).then((result) => {
            if (result.value) {
                swal("Success!", "Your Appointment has been booked", "success")
            } else if (
                result.dismiss === Swal.DismissReason.cancel
            ) {
               // swal("Cancelled", "Your imaginary file is safe :)", "error")
            }
        })
    });

    //alert apoointment booked
    $(".sweet_alert_appointment_booked").on("click", function () {
        swal({
            type: "success",
            title: "Success!",
            text: "Your Appointment has been booked",
            buttonsStyling: !1,
            confirmButtonClass: "btn btn-success"
        }).then((result) => {
            if (result.value) {
                type: "POST",
                window.location.replace('/Appointment/NewAppointments');
            }
        })
    });


    // Alert With RTL
    $(".alert_rtl").on("click", function () {
        swal({
            title: 'هل تريد الاستمرار؟',
            confirmButtonText:  'نعم',
            cancelButtonText:  'لا',
            showCancelButton: true,
            showCloseButton: true,
            target: document.getElementById('rtl-container')
        })
    });

    // Alert With Ajax Request
    $(".alert_ajax").on("click", function () {
        swal({
            title: 'Submit your Github username',
            input: 'text',
            inputAttributes: {
                autocapitalize: 'off'
            },
            showCancelButton: true,
            confirmButtonText: 'Look up',
            showLoaderOnConfirm: true,
            preConfirm: (login) => {
                return fetch(`//api.github.com/users/${login}`)
                    .then(response => {
                        if (!response.ok) {
                            throw new Error(response.statusText)
                        }
                        return response.json()
                    })
                    .catch(error => {
                        Swal.showValidationMessage(
                            `Request failed: ${error}`
                        )
                    })
            },
            allowOutsideClick: () => !Swal.isLoading()
        }).then((result) => {
            if (result.value) {
                Swal.fire({
                    title: `${result.value.login}'s avatar`,
                    imageUrl: result.value.avatar_url
                })
            }
        })
    });

    // Alert With QUESTIONNAIRE
    $(".alert_question").on("click", function () {
        Swal.mixin({
            input: 'text',
            confirmButtonText: 'Next &rarr;',
            showCancelButton: true,
            progressSteps: ['1', '2', '3']
        }).queue([
            {
                title: 'Question 1',
                text: 'Chaining swal2 modals is easy'
            },
            'Question 2',
            'Question 3'
        ]).then((result) => {
            if (result.value) {
                Swal.fire({
                    title: 'All done!',
                    html:
                        'Your answers: <pre><code>' +
                        JSON.stringify(result.value) +
                        '</code></pre>',
                    confirmButtonText: 'Lovely!'
                })
            }
        })
    });

    // Alert With Custom image
    $(".alert_image").on("click", function () {
        Swal.fire({
            title: 'Sweet!',
            text: 'Modal with a custom image.',
            imageUrl: 'images/blog-listing/03.jpg',
            imageWidth: 400,
            imageHeight: 200,
            imageAlt: 'Custom image',
            animation: false
        })
    });
});
/*======== End Doucument Ready Function =========*/