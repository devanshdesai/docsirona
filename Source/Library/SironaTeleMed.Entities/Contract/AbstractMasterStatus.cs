﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SironaTeleMed.Entities.Contract
{
    public abstract class AbstractMasterStatus
    {
           public int Id { get; set; }
           public string Status { get; set; }
           public DateTime CreatedAt { get; set; }
           public int CreatedBy { get; set; }
           public DateTime UpdatedAt { get; set; }
           public int UpdatedBy { get; set; }

    }
}
