﻿using SironaTeleMed.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SironaTeleMed.Entities.Contract
{
    public abstract class AbstractSessions
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int UserType { get; set; }

        [Required]
        public string UserDescription { get; set; }

        [Required]
        public string SpecialRequests { get; set; }
        public int SessionScheduleId { get; set; }
        public string TwilioUrl { get; set; }
        public string AcceptedDateTime { get; set; }
        public string DoctorComments { get; set; }
        public string Email { get; set; }
        public string DoctorComments2 { get; set; }
        public string DoctorComments3 { get; set; }
        public string DoctorComments4 { get; set; }
        public int IsSave { get; set; }
        public string RejectedReason { get; set; }
        public string CancelledReason { get; set; }
        public string CommentDateTime { get; set; }
        public int AssignedTo { get; set; }
        public string PatientDeviceToken { get; set; }
        public string DoctorDeviceToken { get; set; }
        public DateTime CreatedAt { get; set; }
        public string DateSearch { get; set; }
        public int CreatedBy { get; set; }
        public DateTime UpdatedAt { get; set; }
        public int UpdatedBy { get; set; }
        public int IsActive { get; set; }
        public string Status { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int StatusId { get; set; }
        public DateTime? SessionStart { get; set; }
        public DateTime? SessionEnd { get; set; }
        public string PatientName { get; set; }
        public string DoctorName { get; set; }
        public string PatientEmail { get; set; }
        public string DoctorEmail { get; set; }
        public string PatientPhone { get; set; }
        public string DoctorPhone { get; set; }
        public string TransactionId { get; set; }
        public string StripeId { get; set; }
        public string vrfy_pass { get; set; }
        public int IsEndNow { get; set; }
        public bool HasInsurance { get; set; }
        public string InsuranceCompanyName { get; set; }
        public string InsuranceCompanyContact { get; set; }
        [NotMapped]
        public string DisplayDate => SessionStart != null ? SessionStart.Value.ToString("MMM/dd/yyyy") : "-";
        [NotMapped]
        public string DisplayTime => SessionStart != null ? SessionStart.Value.ToString("hh:mm tt") : "-";

        public string PatientDOB { get; set; }
        public string PatientAddress { get; set; }
        public string MedicalRecordNumber { get; set; }
        public string MedicalHistory { get; set; }
        public string SurgicalHistory { get; set; }
        public string Allergy { get; set; }
        public string Medications { get; set; }
        public string Temp { get; set; }
        public string BP { get; set; }
        public string HR { get; set; }
        public string RR { get; set; }
        public string SaO2 { get; set; }
        public string InsuranceId { get; set; }
        public string InsuranceType { get; set; }
        public string GroupNumber { get; set; }


         [NotMapped]
        public string PPatientDOB => ConvertTo.Base64Decode(PatientDOB);
        [NotMapped]
        public string PPatientAddress => ConvertTo.Base64Decode(PatientAddress);
        [NotMapped]
        public string MMedicalRecordNumber => ConvertTo.Base64Decode(MedicalRecordNumber);
        [NotMapped]
        public string MMedicalHistory => ConvertTo.Base64Decode(MedicalHistory);
        [NotMapped]
        public string SSurgicalHistory => ConvertTo.Base64Decode(SurgicalHistory);
        [NotMapped]
        public string AAllergy => ConvertTo.Base64Decode(Allergy);
        [NotMapped]
        public string MMedications => ConvertTo.Base64Decode(Medications);
        [NotMapped]
        public string TTemp => ConvertTo.Base64Decode(Temp);
        [NotMapped]
        public string BBP => ConvertTo.Base64Decode(BP);
        [NotMapped]
        public string HHR => ConvertTo.Base64Decode(HR);
        [NotMapped]
        public string RRR => ConvertTo.Base64Decode(RR);
        [NotMapped]
        public string SSaO2 => ConvertTo.Base64Decode(SaO2);


        //public List<AbstractSessionSchedule> abstractSessionSchedules { get; set; }
        [NotMapped]
        public string UUserDescription => ConvertTo.Base64Decode(UserDescription);
        [NotMapped]
        public string SSpecialRequests => ConvertTo.Base64Decode(SpecialRequests);
        [NotMapped]
        public string DDoctorComments => ConvertTo.Base64Decode(DoctorComments);
        [NotMapped]
        public string DDoctorComments2 => ConvertTo.Base64Decode(DoctorComments2);
        [NotMapped]
        public string DDoctorComments3 => ConvertTo.Base64Decode(DoctorComments3);
        [NotMapped]
        public string DDoctorComments4 => ConvertTo.Base64Decode(DoctorComments4);
        [NotMapped]
        public string StripeToken { get; set; }

        public string RxInput { get; set; }
    }
}
