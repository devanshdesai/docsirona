﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SironaTeleMed.Entities.Contract
{
    public abstract class AbstractNotifications
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "NotificationTo is required.")]
        public int NotificationTo { get; set; }
        [Required(ErrorMessage = "Message is required.")]
        public string Message { get; set; }
        public DateTime CreatedAt { get; set; }
        public int CreatedBy { get; set; }
        public DateTime UpdatedAt { get; set; }
        public int UpdatedBy { get; set; }
        
    }
}
