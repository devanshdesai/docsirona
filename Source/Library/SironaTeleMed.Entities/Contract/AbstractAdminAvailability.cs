﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SironaTeleMed.Entities.Contract
{
    public abstract class AbstractAdminAvailability
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string AvailableDate { get; set; }
        public string AvailableStartTime { get; set; }
        public string AvailableEndTime { get; set; }
        public double TimeSlot { get; set; }
        public decimal Charge { get; set; }
        public int StatusId { get; set; }
        public DateTime CreatedAt { get; set; }
        public int CreatedBy { get; set; }
        public DateTime UpdatedAt { get; set; }
        public int UpdatedBy { get; set; }
        
    }
}
