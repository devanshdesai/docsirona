﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SironaTeleMed.Entities.Contract
{
    public abstract class AbstractIndexMaster
    {
        public List<AbstractSessionSchedule> abstractSessionSchedules { get; set; }

        public List<AbstractSessions> abstractSessions { get; set; }

        public List<AbstractSessions> abstractNewSessions { get; set; }

        [NotMapped]
        public string IsMessage { get; set; }
    }
}
