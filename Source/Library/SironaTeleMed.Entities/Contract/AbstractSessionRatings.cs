﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SironaTeleMed.Entities.Contract
{
    public abstract class AbstractSessionRatings
    {
           public int Id { get; set; }
           public int SessionId { get; set; }
           public int UserId { get; set; }
           public float Ratings { get; set; }
           public string UserDescription { get; set; }
           public DateTime CreatedAt { get; set; }
           public int CreatedBy { get; set; }
           public DateTime UpdatedAt { get; set; }
           public int UpdatedBy { get; set; }

    }
}
