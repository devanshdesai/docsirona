﻿using SironaTeleMed.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SironaTeleMed.Entities.Contract
{
    public abstract class AbstractUsers
    {

        public int Id { get; set; }

        public int UserStatusUpdate { get; set; }
        [Required(ErrorMessage = "First Name is required.")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Last Name is required.")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Email is required.")]
        //[EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Password is required")]
        //[MinLength(6)]
        public string Password { get; set; }

        //[Required(ErrorMessage = "Field can't be empty")]
        //[MinLength(10)]
        //[MaxLength(12)]
        public string Phone { get; set; }

        [Required(ErrorMessage = "Address is required")]
        public string Address { get; set; }

        public string AddressLine2 { get; set; }
        public int UserType { get; set; }
        public string Dateofbirth { get; set; }
        public string ProfileUrl { get; set; }

        [Required(ErrorMessage = "State is required")]
        public int StateId { get; set; }

        [Required(ErrorMessage = "City is required")]
        public string City { get; set; }

        [Required(ErrorMessage = "Country is required")]
        public string CountryId { get; set; }
        public int TimeZone { get; set; }
        public DateTime CreatedAt { get; set; }
        public int CreatedBy { get; set; }
        public string DeviceToken { get; set; }
        public DateTime UpdatedAt { get; set; }
        public int UpdatedBy { get; set; }
        public int IsActive { get; set; }
        public DateTime? SessionStart { get; set; }
        public DateTime? SessionEnd { get; set; }
        public string UserDescription { get; set; }
        public string SpecialRequests { get; set; }
        public int SessionScheduleId { get; set; }

        [NotMapped]
        public string OldPassword { get; set; }

        //[Required(ErrorMessage = "Confirm Password is required")]
        public string ConfirmPassword { get; set; }

        public string StripeId { get; set; }

        public bool HasInsurance { get; set; }

        public string InsuranceCompanyName { get; set; }
        public string InsuranceId { get; set; }
        public string InsuranceType { get; set; }
        public string GroupNumber { get; set; }

        public string InsuranceCompanyContact { get; set; }

        [NotMapped]
        public string FullName => FirstName + " " + LastName;
        [NotMapped]
        public string DisplayDate => SessionStart != null ? SessionStart.Value.ToString("MMM/dd/yyyy") : "-";
        [NotMapped]
        public string DisplayTime => SessionStart != null ? SessionStart.Value.ToString("hh:mm tt") : "-";
        [NotMapped]
        public string UUserDescription => ConvertTo.Base64Decode(UserDescription);
        [NotMapped]
        public string SSpecialRequests => ConvertTo.Base64Decode(SpecialRequests);  
        [NotMapped]
        public string StripeToken { get; set; }
    }
}
