﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SironaTeleMed.Entities.Contract
{
    public abstract class AbstractSessionDocument
    {
        public int Id { get; set; }
        public int SessionId { get; set; }
        public string DocumentPath { get; set; }
        public DateTime CreatedAt { get; set; }
        public int CreatedBy { get; set; }
    }
}
