﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SironaTeleMed.Entities.Contract
{
    public abstract class AbstractSessionSchedule
    {
        public int Id { get; set; }
        public string SesionStart { get; set; }
        public string SesionEnd { get; set; }
        public DateTime SessionStart { get; set; }
        public DateTime SessionEnd { get; set; }
        public int StatusId { get; set; }
        public int UserId{ get; set; }
        public DateTime CreatedAt { get; set; }
        public int IsAvailable { get; set; }
        public int CreatedBy { get; set; }
        public DateTime UpdatedAt { get; set; }
        public int UpdatedBy { get; set; }
        public string Date { get; set; }

        [NotMapped]
        public string AppointMentDate => SessionStart.ToString("hh:mm tt");
        [NotMapped]
        public string AppointmentDateStr => SessionStart.ToString("MM/dd/yyyy");               
    }
}
