﻿//-----------------------------------------------------------------------
// <copyright file="DataModule.cs" company="Dexoc Solutions">
//     Copyright Dexoc Solutions. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace SironaTeleMed.Data
{
    using Autofac;
    using Contract;

    /// <summary>
    /// Contract Class for DataModule.
    /// </summary>
    public class DataModule : Module
    {
        /// <summary>
        /// Override to add registrations to the container.
        /// </summary>
        /// <param name="builder">The builder through which components can be
        /// registered.</param>
        /// <remarks>
        /// Note that the ContainerBuilder parameter is unique to this module.
        /// </remarks>
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<V1.UsersDao>().As<AbstractUsersDao>().InstancePerDependency();
            builder.RegisterType<V1.SessionRatingsDao>().As<AbstractSessionRatingsDao>().InstancePerDependency();
            builder.RegisterType<V1.SessionScheduleDao>().As<AbstractSessionScheduleDao>().InstancePerDependency();
            builder.RegisterType<V1.SessionsDao>().As<AbstractSessionsDao>().InstancePerDependency();
            builder.RegisterType<V1.SessionDocumentDao>().As<AbstractSessionDocumentDao>().InstancePerDependency();
            builder.RegisterType<V1.MasterStatusDao>().As<AbstractMasterStatusDao>().InstancePerDependency();
            builder.RegisterType<V1.NotificationsDao>().As<AbstractNotificationsDao>().InstancePerDependency();
            builder.RegisterType<V1.MasterCountryDao>().As<AbstractMasterCountryDao>().InstancePerDependency();
            builder.RegisterType<V1.MasterStateDao>().As<AbstractMasterStateDao>().InstancePerDependency();

            base.Load(builder);
        }
    }
}
