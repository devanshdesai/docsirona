﻿using Dapper;
using SironaTeleMed.Common;
using SironaTeleMed.Common.Paging;
using SironaTeleMed.Data.Contract;
using SironaTeleMed.Entities.Contract;
using SironaTeleMed.Entities.V1;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SironaTeleMed.Data.V1
{
    public class SessionDocumentDao: AbstractSessionDocumentDao
    {
        public override PagedList<AbstractSessionDocument> SessionDocumentBySessionId(int id)
        {
            PagedList<AbstractSessionDocument> MasterState = new PagedList<AbstractSessionDocument>();

            var param = new DynamicParameters();
            param.Add("@Id", id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.SessionDocumentBySessionId, param, commandType: CommandType.StoredProcedure);
                MasterState.Values.AddRange(task.Read<SessionDocument>());
                MasterState.TotalRecords = task.Read<long>().SingleOrDefault();
            }

            return MasterState;
        }

        public override SuccessResult<AbstractSessionDocument> InsertSessionDocument(AbstractSessionDocument abstractSessionDocument)
        {
            SuccessResult<AbstractSessionDocument> MasterState = null;
            var param = new DynamicParameters();
            param.Add("@SessionId", abstractSessionDocument.SessionId, DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@DocumentPath", abstractSessionDocument.DocumentPath, DbType.String, direction: ParameterDirection.Input);
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                param.Add("@CreatedBy", ProjectSession.UserID, DbType.Int32, direction: ParameterDirection.Input);
                var task = con.QueryMultiple(SQLConfig.SessionDocumentInsert, param, commandType: CommandType.StoredProcedure);
                MasterState = task.Read<SuccessResult<AbstractSessionDocument>>().SingleOrDefault();
                MasterState.Item = task.Read<SessionDocument>().SingleOrDefault();
            }

            return MasterState;
        }

    }
}
