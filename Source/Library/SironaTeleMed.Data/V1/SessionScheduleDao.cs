﻿using Dapper;
using SironaTeleMed.Common;
using SironaTeleMed.Common.Paging;
using SironaTeleMed.Data.Contract;
using SironaTeleMed.Entities.Contract;
using SironaTeleMed.Entities.V1;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

using System.Text;
using System.Threading.Tasks;


namespace SironaTeleMed.Data.V1
{
    public class SessionScheduleDao : AbstractSessionScheduleDao
    {
        public override PagedList<AbstractSessionSchedule> SessionScheduleSelectAll(PageParam pageParam)
        {
            PagedList<AbstractSessionSchedule> SessionSchedule = new PagedList<AbstractSessionSchedule>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.SessionScheduleSelectAll, param, commandType: CommandType.StoredProcedure);
                SessionSchedule.Values.AddRange(task.Read<SessionSchedule>());
                SessionSchedule.TotalRecords = task.Read<long>().SingleOrDefault();
            }

            return SessionSchedule;
        }

        public override SuccessResult<AbstractSessionSchedule> SessionScheduleSelect(int id)
        {
            SuccessResult<AbstractSessionSchedule> SessionSchedule = null;
            var param = new DynamicParameters();
            param.Add("@Id", id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.SessionScheduleSelect, param, commandType: CommandType.StoredProcedure);
                SessionSchedule = task.Read<SuccessResult<AbstractSessionSchedule>>().SingleOrDefault();
                SessionSchedule.Item = task.Read<SessionSchedule>().SingleOrDefault();
            }
            return SessionSchedule;
        }

        public override bool SesisonsScheduleIsAvailableUpdate(int id)
        {
            bool updateStatus = false;
            var param = new DynamicParameters();
            param.Add("@Id", id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.Query<bool>(SQLConfig.SesisonScheduleIsAvailableUpdate, param, commandType: CommandType.StoredProcedure);
                updateStatus = task.SingleOrDefault<bool>();
            }
            return updateStatus;
        }

        public override SuccessResult<AbstractSessionSchedule> SesisonsIsAvailableUpdate (int id)
        {
            SuccessResult<AbstractSessionSchedule> SessionSchedule = null;
            var param = new DynamicParameters();
            param.Add("@Id", id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.SessionScheduleSelect, param, commandType: CommandType.StoredProcedure);
                SessionSchedule = task.Read<SuccessResult<AbstractSessionSchedule>>().SingleOrDefault();
                SessionSchedule.Item = task.Read<SessionSchedule>().SingleOrDefault();
            }
            return SessionSchedule;
        }

        public override PagedList<AbstractSessionSchedule> SearchSessionsSelectByDate(string Date, int type=0)
        {
            PagedList<AbstractSessionSchedule> SessionSchedule = new PagedList<AbstractSessionSchedule>();
            var param = new DynamicParameters();
            param.Add("@Date", Date.Replace("-","/"), dbType: DbType.String, direction: ParameterDirection.Input);
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.SessionScheduleSelectByDate, param, commandType: CommandType.StoredProcedure);
                SessionSchedule.Values.AddRange(task.Read<SessionSchedule>());                
                SessionSchedule.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return SessionSchedule;
        }

        
        public override PagedList<AbstractSessionSchedule> SessionScheduleSelectAllByDate(PageParam pageParam, string Date)
        {
            PagedList<AbstractSessionSchedule> SessionSchedule = new PagedList<AbstractSessionSchedule>();
            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Date", Date.Replace("-", "/"), dbType: DbType.String, direction: ParameterDirection.Input);
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.SessionScheduleSelectAllByDate, param, commandType: CommandType.StoredProcedure);
                SessionSchedule.Values.AddRange(task.Read<SessionSchedule>());
                SessionSchedule.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return SessionSchedule;
        }

        public override SuccessResult<AbstractSessionSchedule> InsertUpdateSessionSchedule(AbstractSessionSchedule abstractSessionSchedule)
        {

            SuccessResult<AbstractSessionSchedule> SessionSchedule = null;
            var param = new DynamicParameters();

            param.Add("@SessionStart", abstractSessionSchedule.SesionStart, DbType.String, direction: ParameterDirection.Input);
            param.Add("@SessionEnd", abstractSessionSchedule.SesionEnd, DbType.String, direction: ParameterDirection.Input);
            param.Add("@Date", abstractSessionSchedule.Date, DbType.String, direction: ParameterDirection.Input);
            param.Add("@StatusId", abstractSessionSchedule.StatusId, DbType.Int32, direction: ParameterDirection.Input);
            

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                if (abstractSessionSchedule.Id > 0)
                {
                    param.Add("@Id", abstractSessionSchedule.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
                    param.Add("@UpdatedBy", abstractSessionSchedule.UpdatedBy, DbType.Int32, direction: ParameterDirection.Input);
                    var task = con.QueryMultiple(SQLConfig.SessionScheduleUpdate, param, commandType: CommandType.StoredProcedure);
                    SessionSchedule = task.Read<SuccessResult<AbstractSessionSchedule>>().SingleOrDefault();
                    SessionSchedule.Item = task.Read<SessionSchedule>().SingleOrDefault();
                }
                else
                {
                    param.Add("@CreatedBy", abstractSessionSchedule.CreatedBy, DbType.Int32, direction: ParameterDirection.Input);
                    var task = con.QueryMultiple(SQLConfig.SessionScheduleInsert, param, commandType: CommandType.StoredProcedure);
                    SessionSchedule = task.Read<SuccessResult<AbstractSessionSchedule>>().SingleOrDefault();
                    SessionSchedule.Item = task.Read<SessionSchedule>().SingleOrDefault();
                }
            }

            return SessionSchedule;
        }

        public override bool SessionScheduleDelete(int id)
        {
            bool isDelete = false;
            var param = new DynamicParameters();
            param.Add("@Id", id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.Query<bool>(SQLConfig.SessionScheduleDelete, param, commandType: CommandType.StoredProcedure);
                isDelete = task.SingleOrDefault<bool>();
            }

            return isDelete;
        }

        public override SuccessResult<AbstractSessionSchedule> SessionScheduleUpdateByStatusId(int id, int statusid)
        {

            SuccessResult<AbstractSessionSchedule> SessionSchedule = null;
            var param = new DynamicParameters();

            param.Add("@Id", id, DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@StatusId",statusid, DbType.Int32, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                    var task = con.QueryMultiple(SQLConfig.SessionScheduleUpdateByStatusId, param, commandType: CommandType.StoredProcedure);
                    SessionSchedule = task.Read<SuccessResult<AbstractSessionSchedule>>().SingleOrDefault();
                    SessionSchedule.Item = task.Read<SessionSchedule>().SingleOrDefault();
            }

            return SessionSchedule;
        }
    }
}
