﻿using Dapper;
using SironaTeleMed.Common;
using SironaTeleMed.Common.Paging;
using SironaTeleMed.Data.Contract;
using SironaTeleMed.Entities.Contract;
using SironaTeleMed.Entities.V1;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

using System.Text;
using System.Threading.Tasks;


namespace SironaTeleMed.Data.V1
{
    public class SessionsDao : AbstractSessionsDao
    {
        public override PagedList<AbstractSessions> SessionsSelectAll(PageParam pageParam)
        {
            PagedList<AbstractSessions> Sessions = new PagedList<AbstractSessions>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.SessionsSelectAll, param, commandType: CommandType.StoredProcedure);
                Sessions.Values.AddRange(task.Read<Sessions>());
                Sessions.TotalRecords = task.Read<long>().SingleOrDefault();
            }

            return Sessions;
        }

        /// <summary>
        /// Connects to the database and attempts to apply completed 
        /// </summary>
        /// <param name="abstractSessions">abstractSessions for doctor 'save' or 'save as draft' appointment completed</param>
        /// <returns></returns>
        public override SuccessResult<AbstractSessions> UpdateComments(AbstractSessions abstractSessions)
        {
            SuccessResult<AbstractSessions> Sessions = new SuccessResult<AbstractSessions>();

            var param = new DynamicParameters();
            param.Add("@Id", abstractSessions.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@StatusId", abstractSessions.StatusId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@doctorComments", !string.IsNullOrWhiteSpace(abstractSessions.DoctorComments) ? ConvertTo.Base64Encode(abstractSessions.DoctorComments) : (object)DBNull.Value, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@doctorComments2", !string.IsNullOrWhiteSpace(abstractSessions.DoctorComments2) ? ConvertTo.Base64Encode(abstractSessions.DoctorComments2) : (object)DBNull.Value, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@doctorComments3", !string.IsNullOrWhiteSpace(abstractSessions.DoctorComments3) ? ConvertTo.Base64Encode(abstractSessions.DoctorComments3) : (object)DBNull.Value, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@doctorComments4", !string.IsNullOrWhiteSpace(abstractSessions.DoctorComments4) ? ConvertTo.Base64Encode(abstractSessions.DoctorComments4) : (object)DBNull.Value, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@MedicalHistory", abstractSessions.MedicalHistory, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@SurgicalHistory", abstractSessions.SurgicalHistory, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Temp", abstractSessions.Temp, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@BP", abstractSessions.BP, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@HR", abstractSessions.HR, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@RR", abstractSessions.RR, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@SaO2", abstractSessions.SaO2, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Allergy", abstractSessions.Allergy, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Medications", abstractSessions.Medications, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@IsSave", abstractSessions.IsSave, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.DoctorCommentsUpdateBySessionsId, param, commandType: CommandType.StoredProcedure);
                Sessions = task.Read<SuccessResult<AbstractSessions>>().SingleOrDefault();
                Sessions.Item = task.Read<Sessions>().SingleOrDefault();
            }
            return Sessions;
        }

        /// <summary>
        /// Connects to the database and attempts to apply cancelled, rejected 
        /// </summary>
        /// <param name="Id">id for SessionsId</param>
        /// <param name="StatusId">statusid for updating</param>
        /// <param name="RejectedReason">RejectedReason for reject appointment time rejected reason</param>
        /// <param name="CancelledReason">CancelledReason for cancel appointment time cancel reason</param>
        /// <param name="AssignedTo">AssignTo for Doctor assign by UserIs</param>
        /// <returns></returns>
        public override SuccessResult<AbstractSessions> SessionUpdateByStatusId(int Id, int StatusId, string RejectedReason, string CancelledReason, string AssignedTo)
        {
            SuccessResult<AbstractSessions> Sessions = new SuccessResult<AbstractSessions>();

            var param = new DynamicParameters();
            param.Add("@Id", Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@StatusId", StatusId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@RejectedReason", RejectedReason, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@CancelledReason", CancelledReason, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@AssignedTo", AssignedTo, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.SessionUpdateByStatusId, param, commandType: CommandType.StoredProcedure);
                Sessions = task.Read<SuccessResult<AbstractSessions>>().SingleOrDefault();
                Sessions.Item = task.Read<Sessions>().SingleOrDefault();
            }

            return Sessions;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pageParam">pageParam</param>
        /// <param name="StatusId">StatusId</param>
        /// <param name="UserId">UserId</param>
        /// <param name="DateSearch">DateSearch</param>
        /// <returns></returns>
        public override PagedList<AbstractSessions> SessionScheduleSelectByUserIdStatusId(PageParam pageParam, string StatusId, int? UserId, string DateSearch)
        {
            PagedList<AbstractSessions> Sessions = new PagedList<AbstractSessions>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            if(UserId == 0)
            {
                param.Add("@UserId", null, dbType: DbType.Int32, direction: ParameterDirection.Input);
            }
            else
            {
                param.Add("@UserId", UserId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            }
            param.Add("@StatusId", StatusId, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Date", DateSearch == null ? "" : DateSearch, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.SessionScheduleSelectByUserIdStatusId, param, commandType: CommandType.StoredProcedure);
                Sessions.Values.AddRange(task.Read<Sessions>());
                Sessions.TotalRecords = task.Read<long>().SingleOrDefault();
            }

            return Sessions;
        }

        public override PagedList<AbstractSessions>Top5UpcomingSessionSelectedRecords(PageParam pageParam, string StatusId, int? UserId)
        {
            PagedList<AbstractSessions> Sessions = new PagedList<AbstractSessions>();

            var param = new DynamicParameters();
            param.Add("@UserId", UserId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@StatusId", StatusId, dbType: DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Top5UpcomingSessionSelectedRecords, param, commandType: CommandType.StoredProcedure);
                Sessions.Values.AddRange(task.Read<Sessions>());
                Sessions.TotalRecords = task.Read<long>().SingleOrDefault();
            }

            return Sessions;
        }

        public override SuccessResult<AbstractSessions>  SessionsSelect(int id,int UserTypeId = 0,int UsrId = 0)
        {
            int UserId = 0;
            int AssignedTo = 0;
            SuccessResult<AbstractSessions> Sessions = null;
            var param = new DynamicParameters();
            param.Add("@Id", id, dbType: DbType.Int32, direction: ParameterDirection.Input); 
            if(UserTypeId != 0 && UsrId != 0)
            {
                if (UserTypeId == 1)
                {
                    AssignedTo = UsrId;
                }
                else if (UserTypeId == 2)
                {
                    UserId = UsrId;
                }
            }
            else
            {
                if (ProjectSession.UserType == 1)
                {
                    AssignedTo = ProjectSession.UserID;
                }
                else if (ProjectSession.UserType == 2)
                {
                    UserId = ProjectSession.UserID;
                }
            }
            
            param.Add("@UserId", UserId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@AssignedTo", AssignedTo, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.SessionsSelect, param, commandType: CommandType.StoredProcedure);
                Sessions = task.Read<SuccessResult<AbstractSessions>>().SingleOrDefault();
                Sessions.Item = task.Read<Sessions>().SingleOrDefault();
            }
            return Sessions;
        }

        public override SuccessResult<AbstractSessions> InsertUpdateSessions(AbstractSessions abstractSessions)
        {
            SuccessResult<AbstractSessions> Sessions = null;
            var param = new DynamicParameters();

            param.Add("@UserId", abstractSessions.UserId, DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UserDescription", !string.IsNullOrWhiteSpace(abstractSessions.UserDescription) ? ConvertTo.Base64Encode(abstractSessions.UserDescription) : (object)DBNull.Value, DbType.String, direction: ParameterDirection.Input);            
            param.Add("@SessionScheduleId", abstractSessions.SessionScheduleId, DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@TwilioUrl", abstractSessions.TwilioUrl, DbType.String, direction: ParameterDirection.Input);
            param.Add("@AcceptedDateTime", abstractSessions.AcceptedDateTime, DbType.String, direction: ParameterDirection.Input);
            param.Add("@doctorComments", !string.IsNullOrWhiteSpace(abstractSessions.DoctorComments) ? ConvertTo.Base64Encode(abstractSessions.DoctorComments) : (object)DBNull.Value, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@doctorComments2", !string.IsNullOrWhiteSpace(abstractSessions.DoctorComments2) ? ConvertTo.Base64Encode(abstractSessions.DoctorComments2) : (object)DBNull.Value, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@doctorComments3", !string.IsNullOrWhiteSpace(abstractSessions.DoctorComments3) ? ConvertTo.Base64Encode(abstractSessions.DoctorComments3) : (object)DBNull.Value, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@doctorComments4", !string.IsNullOrWhiteSpace(abstractSessions.DoctorComments4) ? ConvertTo.Base64Encode(abstractSessions.DoctorComments4) : (object)DBNull.Value, dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@RejectedReason", abstractSessions.DoctorComments, DbType.String, direction: ParameterDirection.Input);
            param.Add("@CancelledReason", abstractSessions.DoctorComments, DbType.String, direction: ParameterDirection.Input);
            param.Add("@CommentDateTime", abstractSessions.CommentDateTime, DbType.String, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                if (abstractSessions.Id > 0)
                {
                    param.Add("@Id", abstractSessions.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
                    param.Add("@UpdatedBy", abstractSessions.UpdatedBy, DbType.Int32, direction: ParameterDirection.Input);
                    var task = con.QueryMultiple(SQLConfig.SessionsUpdate, param, commandType: CommandType.StoredProcedure);
                    Sessions = task.Read<SuccessResult<AbstractSessions>>().SingleOrDefault();
                    Sessions.Item = task.Read<Sessions>().SingleOrDefault();
                }
                else
                {
                    param.Add("@SpecialRequests", !string.IsNullOrWhiteSpace(abstractSessions.SpecialRequests) ? ConvertTo.Base64Encode(abstractSessions.SpecialRequests) : (object)DBNull.Value, DbType.String, direction: ParameterDirection.Input);
                    param.Add("@TransactionId", abstractSessions.TransactionId, DbType.String, direction: ParameterDirection.Input);
                    param.Add("@CreatedBy", abstractSessions.CreatedBy, DbType.Int32, direction: ParameterDirection.Input);
                    param.Add("@HasInsurance", abstractSessions.HasInsurance, DbType.Boolean, direction: ParameterDirection.Input);
                    param.Add("@InsuranceCompanyName", abstractSessions.InsuranceCompanyName, DbType.String, direction: ParameterDirection.Input);
                    param.Add("@InsuranceCompanyContact", abstractSessions.InsuranceCompanyContact, DbType.String, direction: ParameterDirection.Input);
                    param.Add("@InsuranceId", abstractSessions.InsuranceId, DbType.String, direction: ParameterDirection.Input);
                    param.Add("@InsuranceType", abstractSessions.InsuranceType, DbType.String, direction: ParameterDirection.Input);
                    param.Add("@GroupNumber", abstractSessions.GroupNumber, DbType.String, direction: ParameterDirection.Input);

                    var task = con.QueryMultiple(SQLConfig.SessionsInsert, param, commandType: CommandType.StoredProcedure);
                    Sessions = task.Read<SuccessResult<AbstractSessions>>().SingleOrDefault();
                    Sessions.Item = task.Read<Sessions>().SingleOrDefault();
                }
            }
            return Sessions;
        }

        public override bool SessionsDelete(int id)
        {
            bool isDelete = false;
            var param = new DynamicParameters();
            param.Add("@Id", id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.Query<bool>(SQLConfig.SessionsDelete, param, commandType: CommandType.StoredProcedure);
                isDelete = task.SingleOrDefault<bool>();
            }

            return isDelete;
        }


        public override PagedList<AbstractSessions> SessionSelectByUserId(int UserId)
        {
            PagedList<AbstractSessions> Sessions = new PagedList<AbstractSessions>();

            var param = new DynamicParameters();
            param.Add("@UserId", UserId, dbType: DbType.Int32, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.SessionSelectByUserId, param, commandType: CommandType.StoredProcedure);
                Sessions.Values.AddRange(task.Read<Sessions>());
                Sessions.TotalRecords = task.Read<long>().SingleOrDefault();
            }

            return Sessions;
        }


        public override SuccessResult<AbstractSessions> SessionsSelectById(int id)
        {
            SuccessResult<AbstractSessions> Sessions = null;
            var param = new DynamicParameters();
            param.Add("@id", id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.SessionsSelectById, param, commandType: CommandType.StoredProcedure);
                Sessions = task.Read<SuccessResult<AbstractSessions>>().SingleOrDefault();
                Sessions.Item = task.Read<Sessions>().SingleOrDefault();
            }
            return Sessions;
        }

        public override SuccessResult<AbstractSessionSchedule> SearchSessionsSelectByDate(string Date)
        {
            throw new NotImplementedException();
        }

        public override PagedList<AbstractSessions> SearchPatientsHistory(PageParam pageParam, int StatusId, int? UserId, string DateSearch)
        {
            PagedList<AbstractSessions> Sessions = new PagedList<AbstractSessions>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UserId", UserId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@StatusId", StatusId, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Date", DateSearch, dbType: DbType.String, direction: ParameterDirection.Input);
            if(ProjectSession.UserType == 1)
            {
                param.Add("@AssignedTo", ProjectSession.UserID, dbType: DbType.Int32, direction: ParameterDirection.Input);
            }
            else
            {
                param.Add("@AssignedTo", 0, dbType: DbType.Int32, direction: ParameterDirection.Input);
            }

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.SearchPatientsHistory, param, commandType: CommandType.StoredProcedure);
                Sessions.Values.AddRange(task.Read<Sessions>());
                Sessions.TotalRecords = task.Read<long>().SingleOrDefault();
            }

            return Sessions;
        }
    }
}
