﻿using Dapper;
using SironaTeleMed.Common;
using SironaTeleMed.Common.Paging;
using SironaTeleMed.Data.Contract;
using SironaTeleMed.Entities.Contract;
using SironaTeleMed.Entities.V1;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

using System.Text;
using System.Threading.Tasks;


namespace SironaTeleMed.Data.V1
{
    public class MasterStateDao : AbstractMasterStateDao
    {
        public override PagedList<AbstractMasterState> MasterStateSelectAll(PageParam pageParam)
        {
            PagedList<AbstractMasterState> MasterState = new PagedList<AbstractMasterState>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MasterStateSelectAll, param, commandType: CommandType.StoredProcedure);
                MasterState.Values.AddRange(task.Read<MasterState>());
                MasterState.TotalRecords = task.Read<long>().SingleOrDefault();
            }

            return MasterState;
        }

        public override SuccessResult<AbstractMasterState> MasterStateSelect(int id)
        {
            SuccessResult<AbstractMasterState> MasterState = null;
            var param = new DynamicParameters();
            param.Add("@Id", id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MasterStateSelect, param, commandType: CommandType.StoredProcedure);
                MasterState = task.Read<SuccessResult<AbstractMasterState>>().SingleOrDefault();
                MasterState.Item = task.Read<MasterState>().SingleOrDefault();
            }
            return MasterState;
        }

        public override SuccessResult<AbstractMasterState> InsertUpdateMasterState(AbstractMasterState abstractMasterState)
        {

            SuccessResult<AbstractMasterState> MasterState = null;
            var param = new DynamicParameters();

            param.Add("@CountryId", abstractMasterState.CountryId, DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@State", abstractMasterState.State, DbType.String, direction: ParameterDirection.Input);
            

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                if (abstractMasterState.Id > 0)
                {
                    param.Add("@Id", abstractMasterState.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
                    param.Add("@UpdatedBy", abstractMasterState.UpdatedBy, DbType.Int32, direction: ParameterDirection.Input);
                    var task = con.QueryMultiple(SQLConfig.MasterStateUpdate, param, commandType: CommandType.StoredProcedure);
                    MasterState = task.Read<SuccessResult<AbstractMasterState>>().SingleOrDefault();
                    MasterState.Item = task.Read<MasterState>().SingleOrDefault();
                }
                else
                {
                    param.Add("@CreatedBy", abstractMasterState.CreatedBy, DbType.Int32, direction: ParameterDirection.Input);
                    var task = con.QueryMultiple(SQLConfig.MasterStateInsert, param, commandType: CommandType.StoredProcedure);
                    MasterState = task.Read<SuccessResult<AbstractMasterState>>().SingleOrDefault();
                    MasterState.Item = task.Read<MasterState>().SingleOrDefault();
                }
            }

            return MasterState;
        }

        public override bool MasterStateDelete(int id)
        {
            bool isDelete = false;
            var param = new DynamicParameters();
            param.Add("@Id", id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.Query<bool>(SQLConfig.MasterStateDelete, param, commandType: CommandType.StoredProcedure);
                isDelete = task.SingleOrDefault<bool>();
            }

            return isDelete;
        }

        public override PagedList<AbstractMasterState> MasterStateSelectByCountryId(PageParam pageParam, int CountryId)
        {
            PagedList<AbstractMasterState> MasterState = new PagedList<AbstractMasterState>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@CountryId", CountryId, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MasterStateSelectByCountryId, param, commandType: CommandType.StoredProcedure);
                MasterState.Values.AddRange(task.Read<MasterState>());
                MasterState.TotalRecords = task.Read<long>().SingleOrDefault();
            }

            return MasterState;
        }
        
        public override bool MasterStateDeleteByCountryId(int CountryId)
        {
            bool isDelete = false;
            var param = new DynamicParameters();
            param.Add("@CountryId", CountryId, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.Query<bool>(SQLConfig.MasterStateDeleteByCountryId, param, commandType: CommandType.StoredProcedure);
                isDelete = task.SingleOrDefault<bool>();
            }

            return isDelete;
        }
        
    }
}
