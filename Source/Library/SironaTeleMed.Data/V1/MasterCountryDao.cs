﻿using Dapper;
using SironaTeleMed.Common;
using SironaTeleMed.Common.Paging;
using SironaTeleMed.Data.Contract;
using SironaTeleMed.Entities.Contract;
using SironaTeleMed.Entities.V1;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

using System.Text;
using System.Threading.Tasks;


namespace SironaTeleMed.Data.V1
{
    public class MasterCountryDao : AbstractMasterCountryDao
    {
        public override PagedList<AbstractMasterCountry> MasterCountrySelectAll(PageParam pageParam)
        {
            PagedList<AbstractMasterCountry> MasterCountry = new PagedList<AbstractMasterCountry>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MasterCountrySelectAll, param, commandType: CommandType.StoredProcedure);
                MasterCountry.Values.AddRange(task.Read<MasterCountry>());
                MasterCountry.TotalRecords = task.Read<long>().SingleOrDefault();
            }

            return MasterCountry;
        }

        public override SuccessResult<AbstractMasterCountry> MasterCountrySelect(int id)
        {
            SuccessResult<AbstractMasterCountry> MasterCountry = null;
            var param = new DynamicParameters();
            param.Add("@Id", id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MasterCountrySelect, param, commandType: CommandType.StoredProcedure);
                MasterCountry = task.Read<SuccessResult<AbstractMasterCountry>>().SingleOrDefault();
                MasterCountry.Item = task.Read<MasterCountry>().SingleOrDefault();
            }
            return MasterCountry;
        }

        public override SuccessResult<AbstractMasterCountry> InsertUpdateMasterCountry(AbstractMasterCountry abstractMasterCountry)
        {

            SuccessResult<AbstractMasterCountry> MasterCountry = null;
            var param = new DynamicParameters();

            param.Add("@Country", abstractMasterCountry.Country, DbType.String, direction: ParameterDirection.Input);
            param.Add("@CountryCode", abstractMasterCountry.CountryCode, DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                if (abstractMasterCountry.Id > 0)
                {
                    param.Add("@Id", abstractMasterCountry.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
                    param.Add("@UpdatedBy", abstractMasterCountry.UpdatedBy, DbType.Int32, direction: ParameterDirection.Input);
                    var task = con.QueryMultiple(SQLConfig.MasterCountryUpdate, param, commandType: CommandType.StoredProcedure);
                    MasterCountry = task.Read<SuccessResult<AbstractMasterCountry>>().SingleOrDefault();
                    MasterCountry.Item = task.Read<MasterCountry>().SingleOrDefault();
                }
                else
                {
                    param.Add("@CreatedBy", abstractMasterCountry.CreatedBy, DbType.Int32, direction: ParameterDirection.Input);
                    var task = con.QueryMultiple(SQLConfig.MasterCountryInsert, param, commandType: CommandType.StoredProcedure);
                    MasterCountry = task.Read<SuccessResult<AbstractMasterCountry>>().SingleOrDefault();
                    MasterCountry.Item = task.Read<MasterCountry>().SingleOrDefault();
                }
            }

            return MasterCountry;
        }

        public override bool MasterCountryDelete(int id)
        {
            bool isDelete = false;
            var param = new DynamicParameters();
            param.Add("@Id", id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.Query<bool>(SQLConfig.MasterCountryDelete, param, commandType: CommandType.StoredProcedure);
                isDelete = task.SingleOrDefault<bool>();
            }

            return isDelete;
        }
        
    }
}
