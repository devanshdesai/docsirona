﻿using Dapper;
using SironaTeleMed.Common;
using SironaTeleMed.Common.Paging;
using SironaTeleMed.Data.Contract;
using SironaTeleMed.Entities.Contract;
using SironaTeleMed.Entities.V1;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

using System.Text;
using System.Threading.Tasks;


namespace SironaTeleMed.Data.V1
{
    public class SessionRatingsDao : AbstractSessionRatingsDao
    {
        public override PagedList<AbstractSessionRatings> SessionRatingsSelectAll(PageParam pageParam)
        {
            PagedList<AbstractSessionRatings> SessionRatings = new PagedList<AbstractSessionRatings>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.SessionRatingsSelectAll, param, commandType: CommandType.StoredProcedure);
                SessionRatings.Values.AddRange(task.Read<SessionRatings>());
                SessionRatings.TotalRecords = task.Read<long>().SingleOrDefault();
            }

            return SessionRatings;
        }

        public override SuccessResult<AbstractSessionRatings> SessionRatingsSelect(int id)
        {
            SuccessResult<AbstractSessionRatings> SessionRatings = null;
            var param = new DynamicParameters();
            param.Add("@Id", id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.SessionRatingsSelect, param, commandType: CommandType.StoredProcedure);
                SessionRatings = task.Read<SuccessResult<AbstractSessionRatings>>().SingleOrDefault();
                SessionRatings.Item = task.Read<SessionRatings>().SingleOrDefault();
            }
            return SessionRatings;
        }

        public override SuccessResult<AbstractSessionRatings> InsertUpdateSessionRatings(AbstractSessionRatings abstractSessionRatings)
        {

            SuccessResult<AbstractSessionRatings> SessionRatings = null;
            var param = new DynamicParameters();

            param.Add("@SessionId", abstractSessionRatings.SessionId, DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@UserId", abstractSessionRatings.UserId, DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Ratings", abstractSessionRatings.Ratings, DbType.Double, direction: ParameterDirection.Input);
            param.Add("@UserDescription", abstractSessionRatings.UserDescription, DbType.String, direction: ParameterDirection.Input);
           

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                if (abstractSessionRatings.Id > 0)
                {
                    param.Add("@Id", abstractSessionRatings.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
                    param.Add("@UpdatedBy", abstractSessionRatings.UpdatedBy, DbType.Int32, direction: ParameterDirection.Input);
                    var task = con.QueryMultiple(SQLConfig.SessionsUpdate, param, commandType: CommandType.StoredProcedure);
                    SessionRatings = task.Read<SuccessResult<AbstractSessionRatings>>().SingleOrDefault();
                    SessionRatings.Item = task.Read<SessionRatings>().SingleOrDefault();
                }
                else
                {
                    param.Add("@CreatedBy", abstractSessionRatings.CreatedBy, DbType.Int32, direction: ParameterDirection.Input);
                    var task = con.QueryMultiple(SQLConfig.SessionsInsert, param, commandType: CommandType.StoredProcedure);
                    SessionRatings = task.Read<SuccessResult<AbstractSessionRatings>>().SingleOrDefault();
                    SessionRatings.Item = task.Read<SessionRatings>().SingleOrDefault();
                }
            }

            return SessionRatings;
        }

        public override bool SessionRatingsDelete(int id)
        {
            bool isDelete = false;
            var param = new DynamicParameters();
            param.Add("@Id", id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.Query<bool>(SQLConfig.SessionRatingsDelete, param, commandType: CommandType.StoredProcedure);
                isDelete = task.SingleOrDefault<bool>();
            }

            return isDelete;
        }
        
    }
}
