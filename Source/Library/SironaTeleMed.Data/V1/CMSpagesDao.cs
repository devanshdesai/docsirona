﻿using Dapper;
using SironaTeleMed.Common;
using SironaTeleMed.Common.Paging;
using SironaTeleMed.Data.Contract;
using SironaTeleMed.Entities.Contract;
using SironaTeleMed.Entities.V1;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

using System.Text;
using System.Threading.Tasks;


namespace SironaTeleMed.Data.V1
{
    public class CMSpagesDao : AbstractCMSpagesDao
    {
        public override PagedList<AbstractCMSpages> CMSpagesSelectAll(PageParam pageParam)
        {
            PagedList<AbstractCMSpages> CMSpages = new PagedList<AbstractCMSpages>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.CMSpagesSelectAll, param, commandType: CommandType.StoredProcedure);
                CMSpages.Values.AddRange(task.Read<CMSpages>());
                CMSpages.TotalRecords = task.Read<long>().SingleOrDefault();
            }

            return CMSpages;
        }

        public override SuccessResult<AbstractCMSpages> CMSpagesSelect(int id)
        {
            SuccessResult<AbstractCMSpages> CMSpages = null;
            var param = new DynamicParameters();
            param.Add("@Id", id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.CMSpagesSelect, param, commandType: CommandType.StoredProcedure);
                CMSpages = task.Read<SuccessResult<AbstractCMSpages>>().SingleOrDefault();
                CMSpages.Item = task.Read<CMSpages>().SingleOrDefault();
            }
            return CMSpages;
        }

        public override SuccessResult<AbstractCMSpages> InsertUpdateCMSpages(AbstractCMSpages abstractCMSpages)
        {

            SuccessResult<AbstractCMSpages> CMSpages = null;
            var param = new DynamicParameters();

            param.Add("@Name", abstractCMSpages.Name, DbType.String, direction: ParameterDirection.Input);
            param.Add("@Content", abstractCMSpages.Content, DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                if (abstractCMSpages.Id > 0)
                {
                    param.Add("@Id", abstractCMSpages.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
                    param.Add("@UpdatedBy", abstractCMSpages.UpdatedBy, DbType.Int32, direction: ParameterDirection.Input);
                    var task = con.QueryMultiple(SQLConfig.CMSpagesUpdate, param, commandType: CommandType.StoredProcedure);
                    CMSpages = task.Read<SuccessResult<AbstractCMSpages>>().SingleOrDefault();
                    CMSpages.Item = task.Read<CMSpages>().SingleOrDefault();
                }
                else
                {
                    param.Add("@CreatedBy", abstractCMSpages.CreatedBy, DbType.Int32, direction: ParameterDirection.Input);
                    var task = con.QueryMultiple(SQLConfig.CMSpagesInsert, param, commandType: CommandType.StoredProcedure);
                    CMSpages = task.Read<SuccessResult<AbstractCMSpages>>().SingleOrDefault();
                    CMSpages.Item = task.Read<CMSpages>().SingleOrDefault();
                }
            }

            return CMSpages;
        }

        public override bool CMSpagesDelete(int id)
        {
            bool isDelete = false;
            var param = new DynamicParameters();
            param.Add("@Id", id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.Query<bool>(SQLConfig.CMSpagesDelete, param, commandType: CommandType.StoredProcedure);
                isDelete = task.SingleOrDefault<bool>();
            }

            return isDelete;
        }
        
    }
}
