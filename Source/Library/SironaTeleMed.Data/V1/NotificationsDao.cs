﻿using Dapper;
using SironaTeleMed.Common;
using SironaTeleMed.Common.Paging;
using SironaTeleMed.Data.Contract;
using SironaTeleMed.Entities.Contract;
using SironaTeleMed.Entities.V1;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

using System.Text;
using System.Threading.Tasks;


namespace SironaTeleMed.Data.V1
{
    public class NotificationsDao : AbstractNotificationsDao
    {
        public override SuccessResult<AbstractNotifications> NotificationsInsert(AbstractNotifications abstractNotifications)
        {

            SuccessResult<AbstractNotifications> Notifications = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractNotifications.Id, DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@NotificationTo", abstractNotifications.NotificationTo, DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Message", abstractNotifications.Message, DbType.String, direction: ParameterDirection.Input);
            param.Add("@CreatedBy", abstractNotifications.CreatedBy, DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                    var task = con.QueryMultiple(SQLConfig.NotificationsInsert, param, commandType: CommandType.StoredProcedure);
                    Notifications = task.Read<SuccessResult<AbstractNotifications>>().SingleOrDefault();
                    Notifications.Item = task.Read<Notifications>().SingleOrDefault();
                
            }

            return Notifications;
        }
        
        public override PagedList<AbstractNotifications> NotificationsSelectByNotificationTo(PageParam pageParam, int notificationTo)
        {
            PagedList<AbstractNotifications> Notifications = new PagedList<AbstractNotifications>();

            var param = new DynamicParameters();
            param.Add("@NotificationTo", notificationTo, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.NotificationsSelectByNotificationTo, param, commandType: CommandType.StoredProcedure);
                Notifications.Values.AddRange(task.Read<Notifications>());
                Notifications.TotalRecords = task.Read<long>().SingleOrDefault();
            }

            return Notifications;
        }

    }
}
