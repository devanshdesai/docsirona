﻿using Dapper;
using SironaTeleMed.Common;
using SironaTeleMed.Common.Paging;
using SironaTeleMed.Data.Contract;
using SironaTeleMed.Entities.Contract;
using SironaTeleMed.Entities.V1;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

using System.Text;
using System.Threading.Tasks;


namespace SironaTeleMed.Data.V1
{
    public class AdminAvailabilityDao : AbstractAdminAvailabilityDao
    {
        public override PagedList<AbstractAdminAvailability> AdminAvailabilitySelectAll(PageParam pageParam)
        {
            PagedList<AbstractAdminAvailability> AdminAvailability = new PagedList<AbstractAdminAvailability>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.AdminAvailabilitySelectAll, param, commandType: CommandType.StoredProcedure);
                AdminAvailability.Values.AddRange(task.Read<AdminAvailability>());
                AdminAvailability.TotalRecords = task.Read<long>().SingleOrDefault();
            }

            return AdminAvailability;
        }

        public override SuccessResult<AbstractAdminAvailability> AdminAvailabilitySelect(int id)
        {
            SuccessResult<AbstractAdminAvailability> AdminAvailability = null;
            var param = new DynamicParameters();
            param.Add("@Id", id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.AdminAvailabilitySelect, param, commandType: CommandType.StoredProcedure);
                AdminAvailability = task.Read<SuccessResult<AbstractAdminAvailability>>().SingleOrDefault();
                AdminAvailability.Item = task.Read<AdminAvailability>().SingleOrDefault();
            }
            return AdminAvailability;
        }

        public override SuccessResult<AbstractAdminAvailability> InsertUpdateAdminAvailability(AbstractAdminAvailability abstractAdminAvailability)
        {

            SuccessResult<AbstractAdminAvailability> AdminAvailability = null;
            var param = new DynamicParameters();

            param.Add("@UserId", abstractAdminAvailability.UserId, DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@AvailableDate", abstractAdminAvailability.AvailableDate, DbType.String, direction: ParameterDirection.Input);
            param.Add("@AvailableStartTime", abstractAdminAvailability.AvailableStartTime, DbType.String, direction: ParameterDirection.Input);
            param.Add("@AvailableEndTime", abstractAdminAvailability.AvailableEndTime, DbType.String, direction: ParameterDirection.Input);
            param.Add("@TimeSlot", abstractAdminAvailability.TimeSlot, DbType.Double, direction: ParameterDirection.Input);
            param.Add("@Charge", abstractAdminAvailability.Charge, DbType.Decimal, direction: ParameterDirection.Input);
            

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                if (abstractAdminAvailability.Id > 0)
                {
                    param.Add("@Id", abstractAdminAvailability.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
                    param.Add("@UpdatedBy", abstractAdminAvailability.UpdatedBy, DbType.Int32, direction: ParameterDirection.Input);
                    var task = con.QueryMultiple(SQLConfig.AdminAvailabilityUpdate, param, commandType: CommandType.StoredProcedure);
                    AdminAvailability = task.Read<SuccessResult<AbstractAdminAvailability>>().SingleOrDefault();
                    AdminAvailability.Item = task.Read<AdminAvailability>().SingleOrDefault();
                }
                else
                {
                    param.Add("@CreatedBy", abstractAdminAvailability.CreatedBy, DbType.Int32, direction: ParameterDirection.Input);
                    var task = con.QueryMultiple(SQLConfig.AdminAvailabilityInsert, param, commandType: CommandType.StoredProcedure);
                    AdminAvailability = task.Read<SuccessResult<AbstractAdminAvailability>>().SingleOrDefault();
                    AdminAvailability.Item = task.Read<AdminAvailability>().SingleOrDefault();
                }
            }

            return AdminAvailability;
        }

        public override bool AdminAvailabilityDelete(int id)
        {
            bool isDelete = false;
            var param = new DynamicParameters();
            param.Add("@Id", id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.Query<bool>(SQLConfig.AdminAvailabilityDelete, param, commandType: CommandType.StoredProcedure);
                isDelete = task.SingleOrDefault<bool>();
            }

            return isDelete;
        }
        
    }
}
