﻿using Dapper;
using SironaTeleMed.Common;
using SironaTeleMed.Common.Paging;
using SironaTeleMed.Data.Contract;
using SironaTeleMed.Entities.Contract;
using SironaTeleMed.Entities.V1;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

using System.Text;
using System.Threading.Tasks;


namespace SironaTeleMed.Data.V1
{
    public class UsersDao : AbstractUsersDao
    {
        public override PagedList<AbstractUsers> UsersSelectAll(PageParam pageParam, int UserType)
        {
            PagedList<AbstractUsers> users = new PagedList<AbstractUsers>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            if (UserType == 2)
            {
                param.Add("@UserType", "2,4", dbType: DbType.String, direction: ParameterDirection.Input);
            }
            else
            {
                param.Add("@UserType", UserType.ToString(), dbType: DbType.String, direction: ParameterDirection.Input);
            }
            

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UsersSelectAll, param, commandType: CommandType.StoredProcedure);
                users.Values.AddRange(task.Read<Users>());
                users.TotalRecords = task.Read<long>().SingleOrDefault();
            }

            return users;
        }

        public override SuccessResult<AbstractUsers> UsersSelect(int id)
        {
            SuccessResult<AbstractUsers> user = null;
            var param = new DynamicParameters();
            param.Add("@Id", id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UsersSelect, param, commandType: CommandType.StoredProcedure);
                user = task.Read<SuccessResult<AbstractUsers>>().SingleOrDefault();
                user.Item = task.Read<Users>().SingleOrDefault();
            }
            return user;
        }

        public override SuccessResult<AbstractUsers> InsertUpdateUsers(AbstractUsers abstractUsers)
        {

            SuccessResult<AbstractUsers> user = null;
            var param = new DynamicParameters();

            param.Add("@FirstName", abstractUsers.FirstName, DbType.String, direction: ParameterDirection.Input);
            param.Add("@LastName", abstractUsers.LastName, DbType.String, direction: ParameterDirection.Input);
            param.Add("@Phone", abstractUsers.Phone, DbType.String, direction: ParameterDirection.Input);
            param.Add("@Address", abstractUsers.Address, DbType.String, direction: ParameterDirection.Input);
            param.Add("@AddressLine2", abstractUsers.AddressLine2, DbType.String, direction: ParameterDirection.Input);
            //param.Add("@UserType", abstractUsers.UserType, DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Dateofbirth", abstractUsers.Dateofbirth, DbType.String, direction: ParameterDirection.Input);
            param.Add("@ProfileUrl", abstractUsers.ProfileUrl, DbType.String, direction: ParameterDirection.Input);
            param.Add("@StateId", abstractUsers.StateId, DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@City", abstractUsers.City, DbType.String, direction: ParameterDirection.Input);
            param.Add("@CountryId", abstractUsers.CountryId, DbType.String, direction: ParameterDirection.Input);
            param.Add("@TimeZone", abstractUsers.TimeZone, DbType.Int32, direction: ParameterDirection.Input);
            
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                if (abstractUsers.Id > 0)
                {
                    param.Add("@Id", abstractUsers.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
                    param.Add("@UpdatedBy", abstractUsers.UpdatedBy, DbType.Int32, direction: ParameterDirection.Input);
                    param.Add("@StripeId", abstractUsers.StripeId, DbType.String, direction: ParameterDirection.Input);
                    var task = con.QueryMultiple(SQLConfig.UsersUpdate, param, commandType: CommandType.StoredProcedure);
                    user = task.Read<SuccessResult<AbstractUsers>>().SingleOrDefault();
                    user.Item = task.Read<Users>().SingleOrDefault();
                }
                else
                {
                    param.Add("@DeviceToken", abstractUsers.DeviceToken, DbType.Int32, direction: ParameterDirection.Input);
                    param.Add("@UserType", abstractUsers.UserType, DbType.Int32, direction: ParameterDirection.Input);
                    param.Add("@Email", abstractUsers.Email, DbType.String, direction: ParameterDirection.Input);
                    param.Add("@Password", abstractUsers.Password, DbType.String, direction: ParameterDirection.Input);
                    param.Add("@CreatedBy", abstractUsers.CreatedBy, DbType.Int32, direction: ParameterDirection.Input);
                    var task = con.QueryMultiple(SQLConfig.UsersInsert, param, commandType: CommandType.StoredProcedure);
                    user = task.Read<SuccessResult<AbstractUsers>>().SingleOrDefault();
                    user.Item = task.Read<Users>().SingleOrDefault();
                }
            }

            return user;
        }

        public override bool Delete(int id)
        {
            bool isDelete = false;
            var param = new DynamicParameters();
            param.Add("@Id", id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.Query<bool>(SQLConfig.UsersDelete, param, commandType: CommandType.StoredProcedure);
                isDelete = task.SingleOrDefault<bool>();
            }

            return isDelete;
        }

        public override PagedList<AbstractUsers> UsersSelectByStateId(PageParam pageParam, int stateId)
        {
            PagedList<AbstractUsers> users = new PagedList<AbstractUsers>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@StateId", stateId, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UsersSelectByStateId, param, commandType: CommandType.StoredProcedure);
                users.Values.AddRange(task.Read<Users>());
                users.TotalRecords = task.Read<long>().SingleOrDefault();
            }

            return users;
        }

        public override PagedList<AbstractUsers> UsersSelectByCountryId(PageParam pageParam, int countryId)
        {
            PagedList<AbstractUsers> users = new PagedList<AbstractUsers>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@CountryId", countryId, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UsersSelectByCountryId, param, commandType: CommandType.StoredProcedure);
                users.Values.AddRange(task.Read<Users>());
                users.TotalRecords = task.Read<long>().SingleOrDefault();
            }

            return users;
        }

        public override bool UsersDeletestateId(int stateId)
        {
            bool isDelete = false;
            var param = new DynamicParameters();
            param.Add("@StateId", stateId, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.Query<bool>(SQLConfig.UsersDeleteByStateId, param, commandType: CommandType.StoredProcedure);
                isDelete = task.SingleOrDefault<bool>();
            }

            return isDelete;
        }

        public override bool UsersDeleteBycountryId(int countryId)
        {
            bool isDelete = false;
            var param = new DynamicParameters();
            param.Add("@CountryId", countryId, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.Query<bool>(SQLConfig.UsersDeleteByCountryId, param, commandType: CommandType.StoredProcedure);
                isDelete = task.SingleOrDefault<bool>();
            }

            return isDelete;
        }

        public override SuccessResult<AbstractUsers> Login(AbstractUsers abstractUsers)
        {
            SuccessResult<AbstractUsers> users = null;
            var param = new DynamicParameters();
            param.Add("@Email", abstractUsers.Email.Trim(), dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@Password", abstractUsers.Password.Trim(), dbType: DbType.String, direction: ParameterDirection.Input);
            param.Add("@DeviceToken", abstractUsers.DeviceToken == null ? "" : abstractUsers.DeviceToken.Trim(), dbType: DbType.String, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.Login, param, commandType: CommandType.StoredProcedure);
                users = task.Read<SuccessResult<AbstractUsers>>().SingleOrDefault();
                users.Item = task.Read<Users>().SingleOrDefault();
            }
            return users;
        }

		public override int UpdateUserPassword(AbstractUsers abstractUsers)
        {
            int usersId = -1;
            var param = new DynamicParameters();
            param.Add("@Password", abstractUsers.Password, dbType: DbType.String, direction: ParameterDirection.Input);
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                if (abstractUsers.Id > 0)
                {
                    //param.Add("@Id", abstractUsers.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
                    param.Add("@Email", abstractUsers.Email, dbType: DbType.String, direction: ParameterDirection.Input);

                    var task = con.Query<int>(SQLConfig.UserPasswordUpdate, param, commandType: CommandType.StoredProcedure);
                    usersId = task.SingleOrDefault<int>();
                }
            }
            return usersId;
        }

        public override SuccessResult<AbstractUsers> UsersResetPassword(AbstractUsers abstractUsers)
        {

            SuccessResult<AbstractUsers> user = null;
            var param = new DynamicParameters();

            param.Add("@Id", abstractUsers.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Password", abstractUsers.Password, DbType.String, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                if (abstractUsers.Id > 0)
                {
                   
                    var task = con.QueryMultiple(SQLConfig.UsersResetPassword, param, commandType: CommandType.StoredProcedure);
                    user = task.Read<SuccessResult<AbstractUsers>>().SingleOrDefault();
                    user.Item = task.Read<Users>().SingleOrDefault();
                }
            }

            return user;
        }

        public override SuccessResult<AbstractUsers> ForgotPassword(string email, string password)
		{
            SuccessResult<AbstractUsers> customer = null;
            var param = new DynamicParameters();
            param.Add("@Email", email, DbType.String, direction: ParameterDirection.Input);
            param.Add("@Password", password, DbType.String, direction: ParameterDirection.Input);
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.UsersForgotPassword, param, commandType: CommandType.StoredProcedure);
                customer = task.Read<SuccessResult<AbstractUsers>>().SingleOrDefault();
                customer.Item = task.Read<Users>().SingleOrDefault();
            }
            return customer;
        }

        public override bool UserStatusUpdate(int id)
        {
            bool isDelete = false;
            var param = new DynamicParameters();

            param.Add("@Id", id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.Query<bool>(SQLConfig.UserStatusUpdate, param, commandType: CommandType.StoredProcedure);
                isDelete = task.SingleOrDefault<bool>();
            }

            return isDelete;
        }

        public override PagedList<AbstractUsers> UsersSelectByAssignId(PageParam pageParam, int? AssignedTo)
        {
            PagedList<AbstractUsers> users = new PagedList<AbstractUsers>();

            var param = new DynamicParameters();
            param.Add("@AssignedTo", AssignedTo, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                    var task = con.QueryMultiple(SQLConfig.UsersSelectByAssignId, param, commandType: CommandType.StoredProcedure);
                    users.Values.AddRange(task.Read<Users>());
                    users.TotalRecords = task.Read<long>().SingleOrDefault();
            }
            return users;
        }

        public override bool DoctorStatusUpdate(int id)
        {
            bool isDelete = false;
            var param = new DynamicParameters();

            param.Add("@Id", id, dbType: DbType.Int32, direction: ParameterDirection.Input);
            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.Query<bool>(SQLConfig.DoctorStatusUpdate, param, commandType: CommandType.StoredProcedure);
                isDelete = task.SingleOrDefault<bool>();
            }
            return isDelete;
        }


        public override SuccessResult<AbstractUsers> GuestInsert(AbstractUsers abstractUsers)
        {

            SuccessResult<AbstractUsers> user = null;
            var param = new DynamicParameters();

            param.Add("@FirstName", abstractUsers.FirstName, DbType.String, direction: ParameterDirection.Input);
            param.Add("@LastName", abstractUsers.LastName, DbType.String, direction: ParameterDirection.Input);
            param.Add("@Phone", abstractUsers.Phone, DbType.String, direction: ParameterDirection.Input);
            param.Add("@Address", abstractUsers.Address, DbType.String, direction: ParameterDirection.Input);
            param.Add("@AddressLine2", abstractUsers.AddressLine2, DbType.String, direction: ParameterDirection.Input);
            param.Add("@Dateofbirth", abstractUsers.Dateofbirth, DbType.String, direction: ParameterDirection.Input);
            param.Add("@StateId", abstractUsers.StateId, DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@City", abstractUsers.City, DbType.String, direction: ParameterDirection.Input);
            param.Add("@CountryId", abstractUsers.CountryId, DbType.String, direction: ParameterDirection.Input);
            param.Add("@TimeZone", abstractUsers.TimeZone, DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@StripeId", abstractUsers.StripeId, DbType.String, direction: ParameterDirection.Input);


            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                param.Add("@Email", abstractUsers.Email, DbType.String, direction: ParameterDirection.Input);
                param.Add("@CreatedBy", abstractUsers.CreatedBy, DbType.Int32, direction: ParameterDirection.Input);
                var task = con.QueryMultiple(SQLConfig.GuestInsert, param, commandType: CommandType.StoredProcedure);
                user = task.Read<SuccessResult<AbstractUsers>>().SingleOrDefault();
                user.Item = task.Read<Users>().SingleOrDefault();
            }
            return user;
        }
    }
}
