﻿using Dapper;
using SironaTeleMed.Common;
using SironaTeleMed.Common.Paging;
using SironaTeleMed.Data.Contract;
using SironaTeleMed.Entities.Contract;
using SironaTeleMed.Entities.V1;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

using System.Text;
using System.Threading.Tasks;


namespace SironaTeleMed.Data.V1
{
    public class MasterStatusDao : AbstractMasterStatusDao
    {
        public override PagedList<AbstractMasterStatus> MasterStatusSelectAll(PageParam pageParam)
        {
            PagedList<AbstractMasterStatus> MasterStatus = new PagedList<AbstractMasterStatus>();

            var param = new DynamicParameters();
            param.Add("@Offset", pageParam.Offset, dbType: DbType.Int32, direction: ParameterDirection.Input);
            param.Add("@Limit", pageParam.Limit, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MasterStatusSelectAll, param, commandType: CommandType.StoredProcedure);
                MasterStatus.Values.AddRange(task.Read<MasterStatus>());
                MasterStatus.TotalRecords = task.Read<long>().SingleOrDefault();
            }

            return MasterStatus;
        }

        public override SuccessResult<AbstractMasterStatus> MasterStatusSelect(int id)
        {
            SuccessResult<AbstractMasterStatus> MasterStatus = null;
            var param = new DynamicParameters();
            param.Add("@Id", id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.QueryMultiple(SQLConfig.MasterStatusSelect, param, commandType: CommandType.StoredProcedure);
                MasterStatus = task.Read<SuccessResult<AbstractMasterStatus>>().SingleOrDefault();
                MasterStatus.Item = task.Read<MasterStatus>().SingleOrDefault();
            }
            return MasterStatus;
        }

        public override SuccessResult<AbstractMasterStatus> InsertUpdateMasterStatus(AbstractMasterStatus abstractMasterStatus)
        {

            SuccessResult<AbstractMasterStatus> MasterStatus = null;
            var param = new DynamicParameters();

            param.Add("@Status", abstractMasterStatus.Status, DbType.String, direction: ParameterDirection.Input);
            

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                if (abstractMasterStatus.Id > 0)
                {
                    param.Add("@Id", abstractMasterStatus.Id, dbType: DbType.Int32, direction: ParameterDirection.Input);
                    param.Add("@UpdatedBy", abstractMasterStatus.UpdatedBy, DbType.Int32, direction: ParameterDirection.Input);
                    var task = con.QueryMultiple(SQLConfig.MasterStatusUpdate, param, commandType: CommandType.StoredProcedure);
                    MasterStatus = task.Read<SuccessResult<AbstractMasterStatus>>().SingleOrDefault();
                    MasterStatus.Item = task.Read<MasterStatus>().SingleOrDefault();
                }
                else
                {
                    param.Add("@CreatedBy", abstractMasterStatus.CreatedBy, DbType.Int32, direction: ParameterDirection.Input);
                    var task = con.QueryMultiple(SQLConfig.MasterStatusInsert, param, commandType: CommandType.StoredProcedure);
                    MasterStatus = task.Read<SuccessResult<AbstractMasterStatus>>().SingleOrDefault();
                    MasterStatus.Item = task.Read<MasterStatus>().SingleOrDefault();
                }
            }

            return MasterStatus;
        }

        public override bool MasterStatusDelete(int id)
        {
            bool isDelete = false;
            var param = new DynamicParameters();
            param.Add("@Id", id, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (SqlConnection con = new SqlConnection(Configurations.ConnectionString))
            {
                var task = con.Query<bool>(SQLConfig.MasterStatusDelete, param, commandType: CommandType.StoredProcedure);
                isDelete = task.SingleOrDefault<bool>();
            }

            return isDelete;
        }
        
    }
}
