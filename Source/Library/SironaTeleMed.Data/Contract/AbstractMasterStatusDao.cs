﻿using SironaTeleMed.Common;
using SironaTeleMed.Common.Paging;
using SironaTeleMed.Entities.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SironaTeleMed.Data.Contract
{
    public abstract class AbstractMasterStatusDao
    {
        public abstract PagedList<AbstractMasterStatus> MasterStatusSelectAll(PageParam pageParam);

        public abstract SuccessResult<AbstractMasterStatus> MasterStatusSelect(int id);

        public abstract SuccessResult<AbstractMasterStatus> InsertUpdateMasterStatus(AbstractMasterStatus abstractMasterStatus);

        public abstract bool MasterStatusDelete(int id);
    }
}
