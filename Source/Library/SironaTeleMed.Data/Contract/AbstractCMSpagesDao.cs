﻿using SironaTeleMed.Common;
using SironaTeleMed.Common.Paging;
using SironaTeleMed.Entities.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SironaTeleMed.Data.Contract
{
    public abstract class AbstractCMSpagesDao
    {
        public abstract PagedList<AbstractCMSpages> CMSpagesSelectAll(PageParam pageParam);

        public abstract SuccessResult<AbstractCMSpages> CMSpagesSelect(int id);

        public abstract SuccessResult<AbstractCMSpages> InsertUpdateCMSpages(AbstractCMSpages abstractCMSpages);

        public abstract bool CMSpagesDelete(int id);
    }
}
