﻿using SironaTeleMed.Common;
using SironaTeleMed.Common.Paging;
using SironaTeleMed.Entities.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SironaTeleMed.Data.Contract
{
    public abstract class AbstractUsersDao
    {
        public abstract PagedList<AbstractUsers> UsersSelectAll(PageParam pageParam, int UserType);

        public abstract SuccessResult<AbstractUsers> UsersSelect(int id);

        public abstract SuccessResult<AbstractUsers> InsertUpdateUsers(AbstractUsers abstractUsers);

        public abstract bool Delete(int id);

        public abstract PagedList<AbstractUsers> UsersSelectByStateId(PageParam pageParam, int stateId);

        public abstract PagedList<AbstractUsers> UsersSelectByCountryId(PageParam pageParam, int countryId);

        public abstract bool UsersDeletestateId(int stateId);

        public abstract bool UsersDeleteBycountryId(int countryId);

        public abstract SuccessResult<AbstractUsers> Login(AbstractUsers abstractUsers);

        public bool UsersDelete(int id)
        {
            throw new NotImplementedException();
        }

        public bool UsersDeleteByStateId(int stateId)
        {
            throw new NotImplementedException();
        }

        public bool DeletecountryId(int countryId)
        {
            throw new NotImplementedException();
        }

		public abstract int UpdateUserPassword(AbstractUsers abstractUsers);

        public abstract SuccessResult<AbstractUsers> UsersResetPassword(AbstractUsers abstractUsers);

        public abstract SuccessResult<AbstractUsers> ForgotPassword(string email, string password);

        public abstract bool UserStatusUpdate(int id);

        public abstract PagedList<AbstractUsers> UsersSelectByAssignId(PageParam pageParam, int? AssignId);

        public abstract bool DoctorStatusUpdate(int id);

        public abstract SuccessResult<AbstractUsers> GuestInsert(AbstractUsers abstractUsers);
    }
}
