﻿using SironaTeleMed.Common;
using SironaTeleMed.Common.Paging;
using SironaTeleMed.Entities.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SironaTeleMed.Data.Contract
{
    public abstract class AbstractMasterCountryDao
    {
        public abstract PagedList<AbstractMasterCountry> MasterCountrySelectAll(PageParam pageParam);

        public abstract SuccessResult<AbstractMasterCountry> MasterCountrySelect(int id);

        public abstract SuccessResult<AbstractMasterCountry> InsertUpdateMasterCountry(AbstractMasterCountry abstractMasterCountry);

        public abstract bool MasterCountryDelete(int id);
    }
}
