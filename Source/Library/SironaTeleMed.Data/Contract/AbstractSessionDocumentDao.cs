﻿using SironaTeleMed.Common;
using SironaTeleMed.Common.Paging;
using SironaTeleMed.Entities.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SironaTeleMed.Data.Contract
{
    public abstract class AbstractSessionDocumentDao
    {
        public abstract PagedList<AbstractSessionDocument> SessionDocumentBySessionId(int id);

        public abstract SuccessResult<AbstractSessionDocument> InsertSessionDocument(AbstractSessionDocument abstractSessionDocument);

    }
}
