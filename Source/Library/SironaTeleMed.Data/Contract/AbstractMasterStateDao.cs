﻿using SironaTeleMed.Common;
using SironaTeleMed.Common.Paging;
using SironaTeleMed.Entities.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SironaTeleMed.Data.Contract
{
    public abstract class AbstractMasterStateDao
    {
        public abstract PagedList<AbstractMasterState> MasterStateSelectAll(PageParam pageParam);

        public abstract SuccessResult<AbstractMasterState> MasterStateSelect(int id);

        public abstract SuccessResult<AbstractMasterState> InsertUpdateMasterState(AbstractMasterState abstractMasterState);

        public abstract bool MasterStateDelete(int id);

        public abstract PagedList<AbstractMasterState> MasterStateSelectByCountryId(PageParam pageParam, int CountryId);
        
        public abstract bool MasterStateDeleteByCountryId(int CountryId);
        
    }
}
