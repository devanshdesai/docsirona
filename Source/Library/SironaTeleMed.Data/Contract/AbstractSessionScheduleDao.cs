﻿using SironaTeleMed.Common;
using SironaTeleMed.Common.Paging;
using SironaTeleMed.Entities.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SironaTeleMed.Data.Contract
{
    public abstract class AbstractSessionScheduleDao
    {
        public abstract PagedList<AbstractSessionSchedule> SessionScheduleSelectAll(PageParam pageParam);

        public abstract SuccessResult<AbstractSessionSchedule> SessionScheduleSelect(int id);

        public abstract bool SesisonsScheduleIsAvailableUpdate(int id);

        public abstract SuccessResult<AbstractSessionSchedule> SesisonsIsAvailableUpdate(int id);

        public abstract SuccessResult<AbstractSessionSchedule> InsertUpdateSessionSchedule(AbstractSessionSchedule abstractSessionSchedule);

        public abstract bool SessionScheduleDelete(int id);

        public abstract SuccessResult<AbstractSessionSchedule> SessionScheduleUpdateByStatusId(int id, int statusid);

        public abstract PagedList<AbstractSessionSchedule> SearchSessionsSelectByDate(string Date,int type=0);

        public abstract PagedList<AbstractSessionSchedule> SessionScheduleSelectAllByDate(PageParam pageParam, string Date);
    }
}
