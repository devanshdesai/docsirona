﻿using SironaTeleMed.Common;
using SironaTeleMed.Common.Paging;
using SironaTeleMed.Entities.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SironaTeleMed.Data.Contract
{
    public abstract class AbstractAdminAvailabilityDao
    {
        public abstract PagedList<AbstractAdminAvailability> AdminAvailabilitySelectAll(PageParam pageParam);

        public abstract SuccessResult<AbstractAdminAvailability> AdminAvailabilitySelect(int id);

        public abstract SuccessResult<AbstractAdminAvailability> InsertUpdateAdminAvailability(AbstractAdminAvailability abstractAdminAvailability);

        public abstract bool AdminAvailabilityDelete(int id);
    }
}
