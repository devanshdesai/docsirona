﻿ //<copyright file = "SQLConfig.cs" company="Dexoc Solutions">
 //    Copyright Dexoc Solutions.All rights reserved.
 //</copyright>

namespace SironaTeleMed.Data
{
    /// <summary>
    /// SQL configuration class which holds stored procedure name.
    /// </summary>
    internal sealed class SQLConfig
    {
        #region SessionDocument
        public const string SessionDocumentBySessionId = "dbo.SessionDocumentBySessionId";
        public const string SessionDocumentInsert = "dbo.SessionDocumentInsert";
        #endregion

        #region User
        public const string UsersSelectAll = "dbo.UsersSelectAll";
        public const string UsersSelect = "dbo.UsersSelect";
        public const string UsersInsert = "dbo.UsersInsert";
        public const string UsersUpdate = "dbo.UsersUpdate";
        public const string UsersDelete = "dbo.UsersDelete";
        public const string UsersSelectByStateId = "dbo.UsersSelectByStateId";
        public const string UsersSelectByCountryId = "dbo.UsersSelectByCountryId";
        public const string UsersDeleteByStateId = "dbo.UsersDeleteByStateId";
        public const string UsersDeleteByCountryId = "dbo.UsersDeleteByCountryId";
        public const string GuestInsert = "dbo.GuestInsert";
        #endregion

        #region UserServices
        public const string UserServicesSelectAll = "dbo.UserServicesSelectAll";
        public const string UserServicesSelect = "dbo.UserServicesSelect";
        public const string UserServicesInsert = "dbo.UserServicesInsert";
        public const string UserServicesUpdate = "dbo.UserServicesUpdate";
        public const string UserServicesDelete = "dbo.UserServicesDelete";
        public const string UserServicesDeleteByServiceId = "dbo.UserServicesDeleteByServiceId";
        public const string UserServicesDeleteByUserId = "dbo.UserServicesDeleteByUserId";
        public const string UserServicesSelectByServiceId = "dbo.UserServicesSelectByServiceId";
        public const string UserServicesSelectByUserId = "dbo.UserServicesSelectByUserId"; 
        #endregion

        #region UserRatting
        public const string UserRatingSelectAll = "dbo.UserRatingSelectAll";
        public const string UserRatingSelect = "dbo.UserRatingSelect";
        public const string UserRatingInsert = "dbo.UserRatingInsert";
        public const string UserRatingUpdate = "dbo.UserRatingUpdate";
        public const string UserRatingDelete = "dbo.UserRatingDelete";
        public const string UserRatingDeleteByUserId = "dbo.UserRatingDeleteByUserId";
        public const string UserRatingDeleteByRatingBy = "dbo.UserRatingDeleteByRatingBy";
        public const string UserRatingDeleteBySessionId = "dbo.UserRatingDeleteBySessionId";
        public const string UserRatingSelectByRatingBy = "dbo.UserRatingSelectByRatingBy";
        public const string UserRatingSelectBySessionId = "dbo.UserRatingSelectBySessionId";
        public const string UserRatingSelectByUserId = "dbo.UserRatingSelectByUserId";


        #endregion


        public const string Login = "Login";

        public const string UserStatusUpdate = "UserStatusUpdate";

        public const string SessionUpdateByStatusId = "SessionUpdateByStatusId";

        public const string SessionScheduleSelectByUserIdStatusId = "SessionScheduleSelectByUserIdStatusId";

        public const string MasterStateDelete = "MasterStateDelete";

        public const string MasterStateDeleteByCountryId = "MasterStateDeleteByCountryId";

        public const string Top5UpcomingSessionSelectedRecords = "Top5UpcomingSessionSelectedRecords";

        public const string MasterStateUpdate = "MasterStateUpdate";

        public const string MasterStateInsert = "MasterStateInsert";

        public const string MasterStateSelect = "MasterStateSelect";

        public const string MasterStateSelectByCountryId = "MasterStateSelectByCountryId";

        public const string MasterStateSelectAll = "MasterStateSelectAll";

        public const string MasterStatusDelete = "MasterStatusDelete";

        public const string MasterStatusUpdate = "MasterStatusUpdate";

        public const string MasterStatusInsert = "MasterStatusInsert";

        public const string MasterStatusSelect = "MasterStatusSelect";

        public const string MasterStatusSelectAll = "MasterStatusSelectAll";

        public const string  SessionScheduleUpdate = "SessionScheduleUpdate";

        public const string SessionScheduleUpdateByStatusId = "SessionScheduleUpdateByStatusId";

        public const string  SessionScheduleInsert = "SessionScheduleInsert";

        public const string  SessionScheduleDelete = "SessionScheduleDelete";

        public const string  SessionScheduleSelect = "SessionScheduleSelect";
        public const string SessionScheduleSelectByDate = "SessionScheduleSelectByDate";

        public const string  SessionScheduleSelectAll = "SessionScheduleSelectAll";

        public const string SessionsUpdate = "SessionsUpdate";

        public const string SessionsInsert = "SessionsInsert";

        public const string SessionsDelete = "SessionsDelete";

        public const string SessionsSelect = "SessionsSelect";

        public const string SessionsSelectById = "SessionsSelectById";

        public const string SessionsSelectAll = "SessionsSelectAll";

        public const string SessionRatingsUpdate = "SessionRatingsUpdate";

        public const string SessionRatingsInsert = "SessionRatingsInsert";

        public const string SessionRatingsDelete = "SessionRatingsDelete";

        public const string SessionRatingsSelect = "SessionRatingsSelect";

        public const string SessionRatingsSelectAll = "SessionRatingsSelectAll";

        public const string UsersForgotPassword = "UsersForgotPassword";

        public const string AdminAvailabilityUpdate = "AdminAvailabilityUpdate";

        public const string AdminAvailabilityInsert = "AdminAvailabilityInsert";

        public const string AdminAvailabilityDelete = "AdminAvailabilityDelete";

        public const string AdminAvailabilitySelect = "AdminAvailabilitySelect";

        public const string AdminAvailabilitySelectAll = "AdminAvailabilitySelectAll";

        public const string CMSpagesUpdate = "CMSpagesUpdate";

        public const string CMSpagesInsert = "CMSpagesInsert";

        public const string CMSpagesDelete = "CMSpagesDelete";

        public const string CMSpagesSelect = "CMSpagesSelect";

        public const string CMSpagesSelectAll = "CMSpagesSelectAll";

        public const string MasterCountryUpdate = "MasterCountryUpdate";

        public const string MasterCountryInsert = "MasterCountryInsert";

        public const string MasterCountryDelete = "MasterCountryDelete";

        public const string MasterCountrySelect = "MasterCountrySelect";

        public const string MasterCountrySelectAll = "MasterCountrySelectAll";

        public const string NotificationsInsert = "NotificationsInsert";

        public const string NotificationsSelectByNotificationTo = "NotificationsSelectByNotificationTo";

		public const string UserPasswordUpdate = "UserPasswordUpdate";

		public const string UsersResetPassword = "UsersResetPassword";

        public const string SessionSelectByUserId = "SessionsSelectByUserId";

        public const string UsersSelectByAssignId = "UsersSelectByAssignId";

        public const string SearchPatientsHistory = "SearchPatientsHistory";

        public const string DoctorStatusUpdate = "DoctorStatusUpdate";

        public const string DoctorCommentsUpdateBySessionsId = "DoctorCommentsUpdateBySessionsId";

        public const string SesisonScheduleIsAvailableUpdate = "SessionScheduleIsAvailableUpdate";

        public const string SessionScheduleSelectAllByDate = "SessionScheduleSelectAllByDate";
    }
}
