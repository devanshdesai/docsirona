using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.Script.Serialization;
using Twilio;
using Twilio.Rest.Api.V2010.Account;

namespace SironaTeleMed.Common
{
    /// <summary>

    /// Represents a common helper
    /// </summary>
    public class CommonHelper
    {
        static object _locker = new object();

        /// <summary>
        /// Generate15s the unique digits.
        /// </summary>
        /// <returns></returns>
        public static long Generate15UniqueDigits()
        {
            lock (_locker)
            {
                Thread.Sleep(100);
                var number = DateTime.Now.ToString("yyyyMMddHHmmssf");
                return Convert.ToInt64(number);
            }
        }

        public static void LogInfo(string directory, string ex)
        {
            string message = string.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"));
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;
            message += string.Format("Message: {0}", ex);
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;
            string path = directory;//Server.MapPath("~/ErrorLog/ErrorLog.txt");
            using (StreamWriter writer = new StreamWriter(path, true))
            {
                writer.WriteLine(message);
                writer.Close();
            }
        }

        public static void LogError(string directory, Exception ex)
        {
            string message = string.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"));
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;
            message += string.Format("Message: {0}", ex.Message);
            message += Environment.NewLine;
            message += string.Format("StackTrace: {0}", ex.StackTrace);
            message += Environment.NewLine;
            message += string.Format("Source: {0}", ex.Source);
            message += Environment.NewLine;
            message += string.Format("TargetSite: {0}", ex.TargetSite.ToString());
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;
            string path = directory;//Server.MapPath("~/ErrorLog/ErrorLog.txt");
            //using (StreamWriter writer = new StreamWriter(path, true))
            //{
            //    writer.WriteLine(message);
            //    writer.Close();
            //}
        }

        /// <summary>
        /// Deletes the files and folders recursively.
        /// </summary>
        /// <param name="targetDir">The target_dir.</param>
        public static void DeleteFilesAndFoldersRecursively(string targetDir)
        {
            foreach (string file in Directory.GetFiles(targetDir))
            {
                File.Delete(file);
            }

            foreach (string subDir in Directory.GetDirectories(targetDir))
            {
                DeleteFilesAndFoldersRecursively(subDir);
            }

            Thread.Sleep(1); // This makes the difference between whether it works or not. Sleep(0) is not enough.
            Directory.Delete(targetDir);
        }

        /// <summary>
        /// Ensures the subscriber email or throw.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <returns></returns>
        public static string EnsureSubscriberEmailOrThrow(string email)
        {
            string output = EnsureNotNull(email);
            output = output.Trim();
            output = EnsureMaximumLength(output, 255);

            if (!IsValidEmail(output))
            {
                throw new Exception("Email is not valid.");
            }

            return output;
        }

        /// <summary>
        /// Verifies that a string is in valid e-mail format
        /// </summary>
        /// <param name="email">Email to verify</param>
        /// <returns>true if the string is a valid e-mail address and false if it's not</returns>
        public static bool IsValidEmail(string email)
        {
            if (String.IsNullOrEmpty(email))
                return false;

            email = email.Trim();
            var result = Regex.IsMatch(email, "^(?:[\\w\\!\\#\\$\\%\\&\\'\\*\\+\\-\\/\\=\\?\\^\\`\\{\\|\\}\\~]+\\.)*[\\w\\!\\#\\$\\%\\&\\'\\*\\+\\-\\/\\=\\?\\^\\`\\{\\|\\}\\~]+@(?:(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9\\-](?!\\.)){0,61}[a-zA-Z0-9]?\\.)+[a-zA-Z0-9](?:[a-zA-Z0-9\\-](?!$)){0,61}[a-zA-Z0-9]?)|(?:\\[(?:(?:[01]?\\d{1,2}|2[0-4]\\d|25[0-5])\\.){3}(?:[01]?\\d{1,2}|2[0-4]\\d|25[0-5])\\]))$", RegexOptions.IgnoreCase);
            return result;
        }

        /// <summary>
        /// Generate random digit code
        /// </summary>
        /// <param name="length">Length</param>
        /// <returns>Result string</returns>
        public static string GenerateRandomDigitCode(int length)
        {
            var random = new Random();
            string str = string.Empty;
            for (int i = 0; i < length; i++)
                str = String.Concat(str, random.Next(10).ToString());
            return str;
        }

        /// <summary>
        /// Returns an random interger number within a specified rage
        /// </summary>
        /// <param name="min">Minimum number</param>
        /// <param name="max">Maximum number</param>
        /// <returns>Result</returns>
        public static int GenerateRandomInteger(int min = 0, int max = int.MaxValue)
        {
            var randomNumberBuffer = new byte[10];
            new RNGCryptoServiceProvider().GetBytes(randomNumberBuffer);
            return new Random(BitConverter.ToInt32(randomNumberBuffer, 0)).Next(min, max);
        }

        /// <summary>
        /// Ensure that a string doesn't exceed maximum allowed length
        /// </summary>
        /// <param name="str">Input string</param>
        /// <param name="maxLength">Maximum length</param>
        /// <param name="postfix">A string to add to the end if the original string was shorten</param>
        /// <returns>Input string if its lengh is OK; otherwise, truncated input string</returns>
        public static string EnsureMaximumLength(string str, int maxLength, string postfix = null)
        {
            if (String.IsNullOrEmpty(str))
                return str;

            if (str.Length > maxLength)
            {
                var result = str.Substring(0, maxLength);
                if (!String.IsNullOrEmpty(postfix))
                {
                    result += postfix;
                }
                return result;
            }
            return str;
        }

        /// <summary>
        /// Ensures that a string only contains numeric values
        /// </summary>
        /// <param name="str">Input string</param>
        /// <returns>Input string with only numeric values, empty string if input is null/empty</returns>
        public static string EnsureNumericOnly(string str)
        {
            if (String.IsNullOrEmpty(str))
                return string.Empty;

            var result = new StringBuilder();
            foreach (char c in str)
            {
                if (Char.IsDigit(c))
                    result.Append(c);
            }
            return result.ToString();
        }

        /// <summary>
        /// Ensure that a string is not null
        /// </summary>
        /// <param name="str">Input string</param>
        /// <returns>Result</returns>
        public static string EnsureNotNull(string str)
        {
            if (str == null)
                return string.Empty;

            return str;
        }

        /// <summary>
        /// Indicates whether the specified strings are null or empty strings
        /// </summary>
        /// <param name="stringsToValidate">Array of strings to validate</param>
        /// <returns>Boolean</returns>
        public static bool AreNullOrEmpty(params string[] stringsToValidate)
        {
            bool result = false;
            Array.ForEach(stringsToValidate, str =>
            {
                if (string.IsNullOrEmpty(str)) result = true;
            });
            return result;
        }

        /// <summary>
        /// Compare two arrasy
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="a1">Array 1</param>
        /// <param name="a2">Array 2</param>
        /// <returns>Result</returns>
        public static bool ArraysEqual<T>(T[] a1, T[] a2)
        {
            //also see Enumerable.SequenceEqual(a1, a2);
            if (ReferenceEquals(a1, a2))
                return true;

            if (a1 == null || a2 == null)
                return false;

            if (a1.Length != a2.Length)
                return false;

            var comparer = EqualityComparer<T>.Default;
            return !a1.Where((t, i) => !comparer.Equals(t, a2[i])).Any();
        }

        private static AspNetHostingPermissionLevel? _trustLevel;
        /// <summary>
        /// Finds the trust level of the running application (http://blogs.msdn.com/dmitryr/archive/2007/01/23/finding-out-the-current-trust-level-in-asp-net.aspx)
        /// </summary>
        /// <returns>The current trust level.</returns>
        public static AspNetHostingPermissionLevel GetTrustLevel()
        {
            if (!_trustLevel.HasValue)
            {
                //set minimum
                _trustLevel = AspNetHostingPermissionLevel.None;

                //determine maximum
                foreach (AspNetHostingPermissionLevel trustLevel in
                        new[] {
                                AspNetHostingPermissionLevel.Unrestricted,
                                AspNetHostingPermissionLevel.High,
                                AspNetHostingPermissionLevel.Medium,
                                AspNetHostingPermissionLevel.Low,
                                AspNetHostingPermissionLevel.Minimal
                            })
                {
                    try
                    {
                        new AspNetHostingPermission(trustLevel).Demand();
                        _trustLevel = trustLevel;
                        break; //we've set the highest permission we can
                    }
                    catch (System.Security.SecurityException)
                    {
                    }
                }
            }
            return _trustLevel.Value;
        }

        /// <summary>
        /// Convert enum for front-end
        /// </summary>
        /// <param name="str">Input string</param>
        /// <returns>Converted string</returns>
        public static string ConvertEnum(string str)
        {
            string result = string.Empty;
            char[] letters = str.ToCharArray();
            foreach (char c in letters)
                if (c.ToString() != c.ToString().ToLower())
                    result += " " + c;
                else
                    result += c.ToString();
            return result;
        }

        /// <summary>
        /// Sets alert message and displays on page.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="message"></param>
        /// <param name="IsConfirmation"></param>
        /// <param name="YesResponseMethod"></param>
        /// <param name="NoResponseMethod"></param>
        /// <returns></returns>
        public static string ShowAlertMessageToastr(string type, string message, bool IsConfirmation = false, string YesResponseMethod = "", string NoResponseMethod = "")
        {
            message = message.Replace("'", " ");
            var strString = @"<script type='text/javascript' language='javascript'>$(function() { ShowMessageToastr('" + type + "','" + message + "','" + IsConfirmation.ToString().ToLower() + "','" + YesResponseMethod + "','" + NoResponseMethod + "') ; })</script>";
            return strString;
        }

        /// <summary>
        /// Verifies the connectivity.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public static bool VerifyConnectivity(string connectionString)
        {
            SqlConnection connection = null;
            try
            {
                connection = new SqlConnection(connectionString);
                connection.Open();
            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                if (connection != null)
                {
                    connection.Dispose();
                }
            }

            return true;
        }


        #region Site UrlPath

        /// <summary>
        /// Gets Url Suffix
        /// </summary>
        private static string UrlSuffix
        {
            get
            {
                var req = HttpContext.Current.Request;
                var host = req.Url.Host;
                if (req.IsLocal == true && req.Url.Port != 80)
                    host += ":" + req.Url.Port;

                if (HttpContext.Current.Request.ApplicationPath == "/")
                {
                    return host + HttpContext.Current.Request.ApplicationPath;
                }
                else
                {
                    return host + HttpContext.Current.Request.ApplicationPath + "/";
                }
            }
        }


        /// <summary>
        /// Gets the Root Path of the Project
        /// </summary>
        public static string ApplicationRootPath
        {
            get
            {
                string rootPath = HttpContext.Current.Server.MapPath("~");
                if (rootPath.EndsWith("\\", StringComparison.CurrentCulture))
                {
                    return rootPath;
                }
                else
                {
                    return rootPath + "\\";
                }
            }
        }

        /// <summary>
        /// Gets HostName
        /// </summary>
        public static string HostName
        {
            get { return HttpContext.Current.Request.Url.Host; }
        }

        /// <summary>
        /// Gets Secure User Base
        /// </summary>
        public static string SecureUrlBase
        {
            get
            {
                return "https://" + UrlSuffix;
            }
        }

        /// <summary>
        /// Gets Url Base
        /// </summary>
        public static string UrlBase
        {
            get
            {
                return "http://" + UrlSuffix;
            }
        }

        /// <summary>
        /// Gets Site Url Base
        /// </summary>
        public static string SiteUrlBase
        {
            get
            {
                if (HttpContext.Current.Request.IsSecureConnection)
                {
                    return SecureUrlBase;
                }
                else
                {
                    return UrlBase;
                }
            }
        }


        public static void NotificationSent(string token = null, string msg = null, string title = null, string aid = "")
        {
            if (!string.IsNullOrWhiteSpace(token))
            {

                #region Notification Code
                string json = string.Empty;

                string registrationToken = token;
                var postData = new
                {
                    to = token,
                    data = new
                    {
                        title = title,
                        message = msg,
                        id = aid
                    }
                };

                var serializer = new JavaScriptSerializer();
                json = serializer.Serialize(postData);

                string serverKey = Configurations.ServerKey; // Something very long
                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");

                tRequest.Method = "post";
                tRequest.ContentType = "application/json";

                if (!string.IsNullOrWhiteSpace(json))
                {
                    Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                    tRequest.Headers.Add(string.Format("Authorization: key={0}", serverKey));
                    tRequest.Headers.Add(string.Format("Sender: id={0}", "963550172828"));
                    //tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));
                    tRequest.ContentLength = byteArray.Length;

                    using (Stream dataStream = tRequest.GetRequestStream())
                    {
                        dataStream.Write(byteArray, 0, byteArray.Length);
                        using (WebResponse tResponse = tRequest.GetResponse())
                        {
                            using (Stream dataStreamResponse = tResponse.GetResponseStream())
                            {
                                using (StreamReader tReader = new StreamReader(dataStreamResponse))
                                {
                                    var sResponseFromServer = tReader.ReadToEnd();
                                }
                            }
                        }
                    }
                }
                #endregion
            }


        }

        public static void SendTextMessage(string text, string to, string from)
        {
            try
            {
                TwilioClient.Init(Configurations.AccountSid, Configurations.AuthToken);
                var message = MessageResource.Create(
                 body: text,
                 to: new Twilio.Types.PhoneNumber(to),
                 from: new Twilio.Types.PhoneNumber(Configurations.TwilioFromMsgNumber)
               );
            }
            catch (Exception ex)
            {

            }
        }

        public static void CallViaTwilio(string text, string to, string name)
        {
            try
            {
                name = name.Replace(" ", "%20");
                TwilioClient.Init(Configurations.AccountSid, Configurations.AuthToken);
                var call = CallResource.Create(
                    url: new Uri("https://demo.twilio.com/welcome/voice/?name=" + name + "&number=" + to),
                //url: new Uri("https://handler.twilio.com/twiml/EHf23bfd7b32dfa22e5393fd17eecd6b35?name=" + name + "&number=" + to),
                method: Twilio.Http.HttpMethod.Post,
                from: new Twilio.Types.PhoneNumber(Configurations.TwilioFromMsgNumber),
                to: new Twilio.Types.PhoneNumber(to)
                );
            }
            catch (Exception ex)
            {
            }
        }

        #endregion Site UrlPath

    }

    public class AdminEmailService
    {
        /// <summary>
        /// Sending An Email with master mail template
        /// </summary>
        /// <param name="mailFrom">Mail From</param>
        /// <param name="mailTo">Mail To</param>
        /// <param name="mailCC">Mail CC</param>
        /// <param name="mailBCC">Mail BCC</param>
        /// <param name="subject">Subject of mail</param>
        /// <param name="body">Body of mail</param>
        /// <param name="attachment">Attachment for the mail</param>
        /// <param name="emailType">Email Type</param>
        /// <returns>return send status</returns>
        //public static bool Send(string mailTo, string mailCC, string mailBCC, string subject, string body, List<byte[]> attachmentFile = null, List<string> attachmentName = null)
        //{
        //    Boolean issent = true;
        //    string mailFrom;
        //    mailFrom = Configurations.FromEmailAddress;

        //    try
        //    {
        //        if (ValidateEmail(mailFrom, mailTo) && (string.IsNullOrEmpty(mailCC) || IsEmail(mailCC)) && (string.IsNullOrEmpty(mailBCC) || IsEmail(mailBCC)))
        //        {
        //            MailMessage mailMesg = new MailMessage();
        //            SmtpClient objSMTP = new SmtpClient();

        //            if (Configurations.TestMode)
        //            {
        //                mailFrom = Configurations.TestEmailAddress;
        //                mailTo = Configurations.TestEmailAddress;
        //                mailCC = string.Empty;
        //                mailBCC = string.Empty;
        //            }

        //            mailMesg.From = new System.Net.Mail.MailAddress(mailFrom);
        //            mailMesg.To.Add(mailTo);

        //            if (!string.IsNullOrEmpty(mailCC))
        //            {
        //                string[] mailCCArray = mailCC.Split(';');
        //                foreach (string email in mailCCArray)
        //                {
        //                    mailMesg.CC.Add(email);
        //                }
        //            }

        //            if (!string.IsNullOrEmpty(mailBCC))
        //            {
        //                mailBCC = mailBCC.Replace(";", ",");
        //                mailMesg.Bcc.Add(mailBCC);
        //            }

        //            if (attachmentFile != null && attachmentName != null)
        //            {
        //                byte[][] Files = attachmentFile.ToArray();
        //                string[] Names = attachmentName.ToArray();
        //                if (Files != null)
        //                {
        //                    for (int i = 0; i < Files.Length; i++)
        //                    {
        //                        mailMesg.Attachments.Add(new Attachment(new MemoryStream(Files[i]), Names[i]));
        //                    }
        //                }
        //            }

        //            mailMesg.Subject = subject;
        //            mailMesg.Body = body;
        //            mailMesg.IsBodyHtml = true;

        //            try
        //            {
        //                objSMTP.Send(mailMesg);
        //                issent = true;
        //                return issent;
        //            }
        //            catch (Exception)
        //            {
        //                mailMesg.Dispose();
        //                mailMesg = null;
        //                //CommonService.Log(e);
        //                issent = false;
        //                return issent;
        //            }
        //            finally
        //            {
        //                mailMesg.Dispose();
        //            }
        //        }
        //        else
        //        {
        //            return false;
        //        }
        //    }
        //    catch
        //    {
        //        return false;
        //    }

        //}

        //public static bool SendPushNotification(PayLoadModelDTO payLoadModel)
        //{
        //    if (string.IsNullOrEmpty(payLoadModel.Fcm_Url) ||
        //        string.IsNullOrEmpty(payLoadModel.Android_Sender_Id) ||
        //        string.IsNullOrEmpty(payLoadModel.Android_Server_Api_Key) ||
        //        string.IsNullOrEmpty(payLoadModel.DeviceId) ||
        //        string.IsNullOrEmpty(payLoadModel.Message) ||
        //        string.IsNullOrEmpty(payLoadModel.Title)
        //    )
        //    {
        //        return false;
        //    }

        //    bool isSuccess = false;

        //    try
        //    {
        //        var tRequest = WebRequest.Create(payLoadModel.Fcm_Url);
        //        tRequest.Method = "post";
        //        tRequest.ContentType = "application/json";
        //        tRequest.Headers.Add(string.Format("Authorization: key={0}", payLoadModel.Android_Server_Api_Key));
        //        tRequest.Headers.Add(string.Format("Sender: id={0}", payLoadModel.Android_Sender_Id));

        //        var postData = new
        //        {
        //            to = payLoadModel.DeviceId,
        //            data = new
        //            {
        //                title = payLoadModel.Title,
        //                message = payLoadModel.Message,
        //                notificationTypeId = payLoadModel.NotificationTypeId,
        //                timestamp = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"),
        //                id = DateTime.Now.Ticks,
        //                primarykey = payLoadModel.Id,
        //                ImageUrl = payLoadModel.ImageUrl
        //            }
        //        };

        //        var serializer = new JavaScriptSerializer();
        //        var json = serializer.Serialize(postData);
        //        var byteArray = Encoding.UTF8.GetBytes(json);
        //        tRequest.ContentLength = byteArray.Length;
        //        using (var dataStream = tRequest.GetRequestStream())
        //        {
        //            dataStream.Write(byteArray, 0, byteArray.Length);
        //            using (var tResponse = tRequest.GetResponse())
        //            {
        //                using (var dataStreamResponse = tResponse.GetResponseStream())
        //                {
        //                    if (dataStreamResponse != null)
        //                        using (var tReader = new StreamReader(dataStreamResponse))
        //                        {
        //                            string response = tReader.ReadToEnd();
        //                            isSuccess = tReader.ReadToEnd().Contains("\"success\":1");
        //                        }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        isSuccess = false;
        //    }

        //    return isSuccess;
        //}

        public static bool Send(string mailTo, string mailCC, string mailBCC, string subject, string body, List<byte[]> attachmentFile = null, List<string> attachmentName = null, DateTime? startDateTime = null, DateTime? endDateTime = null, string calenderBody = null)
        {
            Boolean issent = true;
            string mailFrom;
            mailFrom = Configurations.FromEmailAddress;

            try
            {
                if (ValidateEmail(mailFrom, mailTo) && (string.IsNullOrEmpty(mailCC) || IsEmail(mailCC)) && (string.IsNullOrEmpty(mailBCC) || IsEmail(mailBCC)))
                {
                    MailMessage mailMesg = new MailMessage();
                    SmtpClient objSMTP = new SmtpClient();
                    objSMTP.Host = Configurations.EmailHost;
                    objSMTP.EnableSsl = Convert.ToBoolean(Configurations.EnableSsl);
                    
                    System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
                    NetworkCred.UserName = Configurations.SendGridUserName; //reading from web.config  
                    NetworkCred.Password = Configurations.SendGridPassword; //reading from web.config  
                    objSMTP.UseDefaultCredentials = true;

                    objSMTP.Credentials = NetworkCred;

                    objSMTP.Port = int.Parse(Configurations.Port);

                    if (Configurations.TestMode)
                    {
                        mailFrom = Configurations.TestEmailAddress;
                        mailTo = Configurations.TestEmailAddress;
                        mailCC = string.Empty;
                        mailBCC = string.Empty;
                    }

                    mailMesg.From = new System.Net.Mail.MailAddress(mailFrom);
                    mailMesg.To.Add(mailTo);

                    if (!string.IsNullOrEmpty(mailCC))
                    {
                        string[] mailCCArray = mailCC.Split(';');
                        foreach (string email in mailCCArray)
                        {
                            mailMesg.CC.Add(email);
                        }
                    }

                    if (!string.IsNullOrEmpty(mailBCC))
                    {
                        mailBCC = mailBCC.Replace(";", ",");
                        mailMesg.Bcc.Add(mailBCC);
                    }

                    if (attachmentFile != null && attachmentName != null)
                    {
                        byte[][] Files = attachmentFile.ToArray();
                        string[] Names = attachmentName.ToArray();
                        if (Files != null)
                        {
                            for (int i = 0; i < Files.Length; i++)
                            {
                                //mailMesg.Attachments.Add(new Attachment(new MemoryStream(Files[i]), Names[i]));
                            }
                        }
                    }

                    mailMesg.Subject = subject;
                    mailMesg.Body = body;

                    //mailMesg.IsBodyHtml = true;

                    #region Old Ics Code
                    //mailMesg.Headers.Add("Content-Type", string.Format("text/calendar; method=REQUEST; charset={0}", "\"utf-8\""));

                    //var htmlContentType = new System.Net.Mime.ContentType(System.Net.Mime.MediaTypeNames.Text.Html);
                    //var avHtmlBody = AlternateView.CreateAlternateViewFromString(body, htmlContentType);
                    //mailMesg.AlternateViews.Add(avHtmlBody);
                    //mailMesg.Headers.Add("Content-class", "urn:content-classes:calendarmessage");

                    //StringBuilder str = new StringBuilder();
                    //str.AppendLine("BEGIN:VCALENDAR");
                    //str.AppendLine("PRODID:-//" + mailMesg.To[0].Address);
                    //str.AppendLine("VERSION:2.0");
                    //str.AppendLine("METHOD:REQUEST");
                    ////str.AppendLine("CALSCALE:GREGORIAN");

                    //str.AppendLine("BEGIN:VEVENT");


                    //str.AppendLine(string.Format("ATTENDEE;CN=\"{0}\";RSVP=3DTRUE:mailto:{1}", "TEST USER", mailMesg.To[0].Address));
                    //str.AppendLine("CLASS:PUBLIC");
                    //str.AppendLine(string.Format("DESCRIPTION:{0}", body));
                    //str.AppendLine(string.Format("DTEND:{0:yyyyMMddTHHmmssZ}", endDateTime.Value.ToUniversalTime().ToString("yyyyMMdd\\THHmmss\\Z")));
                    //str.AppendLine(string.Format("DTSTAMP:{0:yyyyMMddTHHmmssZ}", (endDateTime.Value - startDateTime.Value).Minutes.ToString()));
                    //str.AppendLine(string.Format("DTSTART:{0:yyyyMMddTHHmmssZ}", startDateTime.Value.ToUniversalTime().ToString("yyyyMMdd\\THHmmss\\Z")));
                    //str.AppendLine("LOCATION: Meeting location");
                    //str.AppendLine(string.Format("ORGANIZER:MAILTO:{0}", mailMesg.From.Address));
                    //str.AppendLine(string.Format("SUMMARY;LANGUAGE=3Den-us:{0}", mailMesg.Subject));
                    //str.AppendLine(string.Format("UID:{0}", Guid.NewGuid().ToString()));
                    //str.AppendLine(string.Format("X-ALT-DESC;FMTTYPE=3Dtext/html:{0}", calenderBody));
                    //str.AppendLine("BEGIN:VALARM");
                    //str.AppendLine("TRIGGER:-PT15M");
                    //str.AppendLine("ACTION:DISPLAY");
                    //str.AppendLine("DESCRIPTION:Reminder");
                    //str.AppendLine("END:VALARM");
                    //str.AppendLine("END:VEVENT");
                    //str.AppendLine("END:VCALENDAR");


                    //byte[] byteArray = Encoding.ASCII.GetBytes(str.ToString());
                    //MemoryStream stream = new MemoryStream(byteArray);
                    //Attachment attach = new Attachment(stream, "invite.ics");                    
                    //System.Net.Mime.ContentType ct = new System.Net.Mime.ContentType("text/calendar");
                    //ct.Parameters.Add("method", "REQUEST");

                    ////mailMesg.Body = str.ToString();
                    //mailMesg.Attachments.Add(Attachment.CreateAttachmentFromString(stream.ToString(),ct));
                    ////AlternateView avCal = new AlternateView(stream, ct);
                    ////mailMesg.AlternateViews.Add(avCal);

                    //mailMesg.Headers.Add("Content-Type", string.Format("text/calendar; method=REQUEST; charset={0}", "\"utf-8\""));
                    // mailMesg.Body = str.ToString();
                    //mailMesg.IsBodyHtml = true;
                    #endregion

                    StringBuilder str = new StringBuilder();
                    str.AppendLine("BEGIN:VCALENDAR");

                    //PRODID: identifier for the product that created the Calendar object
                    str.AppendLine("PRODID:-//ABC Company//Outlook MIMEDIR//EN");
                    str.AppendLine("VERSION:2.0");
                    str.AppendLine("METHOD:REQUEST");

                    str.AppendLine("BEGIN:VEVENT");

                    str.AppendLine(string.Format("DTSTART:{0:yyyyMMddTHHmmssZ}", startDateTime));//TimeZoneInfo.ConvertTimeToUtc("BeginTime").ToString("yyyyMMddTHHmmssZ")));
                    str.AppendLine(string.Format("DTSTAMP:{0:yyyyMMddTHHmmssZ}", DateTime.UtcNow));
                    str.AppendLine(string.Format("DTEND:{0:yyyyMMddTHHmmssZ}", endDateTime));//TimeZoneInfo.ConvertTimeToUtc("EndTime").ToString("yyyyMMddTHHmmssZ")));
                    str.AppendLine(string.Format("LOCATION: {0}", "Location"));

                    // UID should be unique.
                    str.AppendLine(string.Format("UID:{0}", Guid.NewGuid()));
                    str.AppendLine(string.Format("DESCRIPTION:{0}", "invite"));
                    str.AppendLine(string.Format("X-ALT-DESC;FMTTYPE=text/html:{0}", "invite"));
                    str.AppendLine(string.Format("SUMMARY:{0}", subject));

                    str.AppendLine("STATUS:CONFIRMED");
                    str.AppendLine("BEGIN:VALARM");
                    str.AppendLine("TRIGGER:-PT15M");
                    str.AppendLine("ACTION:Accept");
                    str.AppendLine("DESCRIPTION:Reminder");
                    str.AppendLine("X-MICROSOFT-CDO-BUSYSTATUS:BUSY");
                    str.AppendLine("END:VALARM");
                    str.AppendLine("END:VEVENT");

                    str.AppendLine(string.Format("ORGANIZER:MAILTO:{0}", mailMesg.From.Address));
                    str.AppendLine(string.Format("ATTENDEE;CN=\"{0}\";RSVP=TRUE:mailto:{1}", mailMesg.To[0].DisplayName, mailMesg.To[0].Address));

                    str.AppendLine("END:VCALENDAR");

                    str.AppendLine("ACTION:Accept");
                    str.AppendLine("X-MICROSOFT-CDO-BUSYSTATUS:BUSY");


                    System.Net.Mime.ContentType ct = new System.Net.Mime.ContentType("text/calendar");
                    ct.Parameters.Add("method", "REQUEST");
                    ct.Parameters.Add("name", "meeting.ics");
                    AlternateView avCal = AlternateView.CreateAlternateViewFromString(str.ToString(), ct);
                    mailMesg.AlternateViews.Add(avCal);

                    try
                    {
                        objSMTP.Send(mailMesg);
                        issent = true;
                        return issent;
                    }
                    catch (Exception ex)
                    {
                        mailMesg.Dispose();
                        mailMesg = null;
                        issent = false;
                        return issent;
                    }
                    finally
                    {
                        mailMesg.Dispose();
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }

        }
        /// <summary>
        /// Method is used to Validate Email
        /// </summary>
        /// <param name="fromEmail">From email List</param>
        /// <param name="toEmail">To Email list</param>
        /// <returns>Returns validation result</returns>
        private static bool ValidateEmail(string fromEmail, string toEmail)
        {
            bool isValid = true;
            if (!IsEmail(fromEmail))
            {
                isValid = false;
            }

            if (!string.IsNullOrEmpty(toEmail))
            {
                toEmail = toEmail.Replace(" ", string.Empty);
                string[] emailList = null;
                try
                {
                    emailList = toEmail.Split(',');
                }
                catch
                {
                    isValid = false;
                }

                if (emailList != null && emailList.Count() > 0)
                {
                    foreach (string email in emailList)
                    {
                        if (!IsEmail(email))
                        {
                            isValid = false;
                        }
                    }
                }
                else
                {
                    isValid = false;
                }
            }
            else
            {
                isValid = false;
            }

            return isValid;
        }

        /// <summary>
        /// Check email string is Email or not
        /// </summary>
        /// <param name="email">Email to verify</param>
        /// <returns>return email validation result</returns>
        private static bool IsEmail(string email)
        {
            string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                  @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                  @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
            Regex re = new Regex(strRegex);
            if (re.IsMatch(email))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }

}

//public class PayLoadModelDTO
//{
//    public PayLoadModelDTO()
//    {
//        Fcm_Url = SettingConfig.FcmUrl;
//        Android_Server_Api_Key = SettingConfig.AndroidServerApiKey;
//        Android_Sender_Id = SettingConfig.AndroidSenderId;
//    }
//    public string Fcm_Url { get; set; }
//    public string Android_Server_Api_Key { get; set; }
//    public string Android_Sender_Id { get; set; }
//    public short NotificationTypeId { get; set; }
//    public string DeviceId { get; set; }
//    public string Title { get; set; }
//    public string Message { get; set; }
//    public long Id { get; set; }
//    public string ImageUrl { get; set; }
//}
