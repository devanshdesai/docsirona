﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SironaTeleMed.Common
{

    public enum SubscriptionMasterEnum
    {
        /// <summary>
        /// Free
        /// </summary>
        Free = 1,

        /// <summary>
        /// Paid
        /// </summary>
        Paid = 2,

        /// <summary>
        /// Corporate
        /// </summary>
        Corporate = 3,

        /// <summary>
        /// Employee
        /// </summary>
        Employee = 4,

    }

    public enum CategoryMasterEnum
    {
        /// <summary>
        /// Free
        /// </summary>
        Engineering = 1,

        /// <summary>
        /// Paid
        /// </summary>
        AutoMobile = 2        

    }    

    public enum ResourceTypeEnum
    {
        /// <summary>
        /// Free
        /// </summary>
        Free = 1,

        /// <summary>
        /// Subscribe
        /// </summary>
        Subscribe = 2,

        /// <summary>
        /// Paid
        /// </summary>
        Paid = 3
    }

    public enum UserTypeEnum
    {
        Provider = 1,
        Consumer = 2,
        Admin = 3
    }

}
