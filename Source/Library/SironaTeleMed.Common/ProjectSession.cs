﻿using System.Web;
using System;

namespace SironaTeleMed.Common
{
    public class ProjectSession
    {
        /// <summary>
        /// Gets or sets the client connection string.
        /// </summary>
        /// <value>The client connection string.</value>
        public static string ClientConnectionString
        {
            get
            {
                if (HttpContext.Current.Session["ConnectionString"] == null)
                {
                    return Configurations.ConnectionString;
                }
                else
                {
                    return ConvertTo.String(HttpContext.Current.Session["ConnectionString"]);
                }
            }

            set
            {
                HttpContext.Current.Session["ConnectionString"] = value;
            }
        }

        public static string UserProfilePicture
        {
            get
            {
                if (HttpContext.Current.Session["UserProfilePicture"] == null)
                {
                    return string.Empty;
                }
                else
                {
                    return ConvertTo.String(HttpContext.Current.Session["UserProfilePicture"]);
                }
            }

            set
            {
                HttpContext.Current.Session["UserProfilePicture"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the admin user identifier.
        /// </summary>
        /// <value>The admin user identifier.</value>
        public static int AdminUserID
        {
            get
            {
                if (HttpContext.Current.Session["AdminUserID"] == null)
                {
                    return 0;
                }
                else
                {
                    return ConvertTo.Integer(HttpContext.Current.Session["AdminUserID"]);
                }
            }

            set
            {
                HttpContext.Current.Session["AdminUserID"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the name of the admin user.
        /// </summary>
        /// <value>The name of the admin user.</value>
        public static string AdminUserName
        {
            get
            {
                if (HttpContext.Current.Session["AdminUserName"] == null)
                {
                    return "Admin";
                }
                else
                {
                    return ConvertTo.String(HttpContext.Current.Session["AdminUserName"]);
                }
            }

            set
            {
                HttpContext.Current.Session["AdminUserName"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the user identifier.
        /// </summary>
        /// <value>The user identifier.</value>
        public static int UserID
        {
            get
            {
                if (HttpContext.Current.Session["UserID"] == null)
                {
                    return 0;
                }
                else
                {
                    return ConvertTo.Integer(HttpContext.Current.Session["UserID"]);
                }
            }

            set
            {
                HttpContext.Current.Session["UserID"] = value;
            }
        }

        public static int DoctorStatus
        {
            get
            {
                if (HttpContext.Current.Session["DoctorStatus"] == null)
                {
                    return 0;
                }
                else
                {
                    return ConvertTo.Integer(HttpContext.Current.Session["DoctorStatus"]);
                }
            }

            set
            {
                HttpContext.Current.Session["DoctorStatus"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the name of the user.
        /// </summary>
        /// <value>The name of the user.</value>
        public static string UserName
        {
            get
            {
                if (HttpContext.Current.Session["UserName"] == null)
                {
                    return "User";
                }
                else
                {
                    return ConvertTo.String(HttpContext.Current.Session["UserName"]);
                }
            }

            set
            {
                HttpContext.Current.Session["UserName"] = value;
            }
        }

        public static string Email
        {
            get
            {
                if (HttpContext.Current.Session["Email"] == null)
                {
                    return "Email";
                }
                else
                {
                    return ConvertTo.String(HttpContext.Current.Session["Email"]);
                }
            }

            set
            {
                HttpContext.Current.Session["Email"] = value;
            }
        }

        public static int SubscriptionId
        {
            get
            {
                if (HttpContext.Current.Session["SubscriptionId"] == null)
                {
                    return 0;
                }
                else
                {
                    return ConvertTo.Integer(HttpContext.Current.Session["SubscriptionId"]);
                }
            }

            set
            {
                HttpContext.Current.Session["SubscriptionId"] = value;
            }
        }

        public static int UserType
        {
            get
            {
                if (HttpContext.Current.Session["UserType"] == null)
                {
                    return 0;
                }
                else
                {
                    return ConvertTo.Integer(HttpContext.Current.Session["UserType"]);
                }
            }

            set
            {
                HttpContext.Current.Session["UserType"] = value;
            }
        }

        public static string UserTypeStr
        {
            get
            {
                if (HttpContext.Current.Session["UserTypeStr"] == null)
                {
                    return string.Empty;
                }
                else
                {
                    return ConvertTo.String(HttpContext.Current.Session["UserTypeStr"]);
                }
            }

            set
            {
                HttpContext.Current.Session["UserTypeStr"] = value;
            }
        }

        public static int InstituteId
        {
            get
            {
                if (HttpContext.Current.Session["InstituteId"] == null)
                {
                    return 0;
                }
                else
                {
                    return ConvertTo.Integer(HttpContext.Current.Session["InstituteId"]);
                }
            }

            set
            {
                HttpContext.Current.Session["InstituteId"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the access token of the facebook user.
        /// </summary>
        /// <value>The value of the facebook user access token.</value>
        public static string AccessToken
        {
            get
            {
                if (HttpContext.Current.Session["AccessToken"] == null)
                {
                    return "AccessToken";
                }
                else
                {
                    return ConvertTo.String(HttpContext.Current.Session["AccessToken"]);
                }
            }

            set
            {
                HttpContext.Current.Session["AccessToken"] = value;
            }
        }

        public static string DeviceToken
        {
            get
            {
                if (HttpContext.Current.Session["DeviceToken"] == null)
                {
                    return "DeviceToken";
                }
                else
                {
                    return ConvertTo.String(HttpContext.Current.Session["DeviceToken"]);
                }
            }

            set
            {
                HttpContext.Current.Session["DeviceToken"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the plan.
        /// </summary>
        /// <value>
        /// The plan.
        /// </value>
        public static int Plan
        {
            get
            {
                if (HttpContext.Current.Session["Plan"] == null)
                {
                    return (int)SubscriptionMasterEnum.Free;
                }
                else
                {
                    return ConvertTo.Integer(HttpContext.Current.Session["Plan"]);
                }
            }
            set
            {
                HttpContext.Current.Session["Plan"] = value;
            }
        }


        /// <summary>
        /// Gets or sets the facebook business ads Id.
        /// </summary>
        /// <value>The value of the facebook business ads Id.</value>
        public static string BusinessAdAccountID
        {
            get
            {
                if (HttpContext.Current.Session["BusinessAdAccountID"] == null)
                {
                    return "BusinessAdAccountID";
                }
                else
                {
                    return ConvertTo.String(HttpContext.Current.Session["BusinessAdAccountID"]);
                }
            }

            set
            {
                HttpContext.Current.Session["BusinessAdAccountID"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the facebook business campaign Id.
        /// </summary>
        /// <value>The value of the facebook business campaign Id.</value>
        public static string BusinessCampaignId
        {
            get
            {
                if (HttpContext.Current.Session["BusinessCampaignId"] == null)
                {
                    return "BusinessCampaignId";
                }
                else
                {
                    return ConvertTo.String(HttpContext.Current.Session["BusinessCampaignId"]);
                }
            }

            set
            {
                HttpContext.Current.Session["BusinessCampaignId"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the facebook business adset Id.
        /// </summary>
        /// <value>The value of the facebook business adset Id.</value>
        public static string BusinessAdsetId
        {
            get
            {
                if (HttpContext.Current.Session["BusinessAdsetId"] == null)
                {
                    return "BusinessAdsetId";
                }
                else
                {
                    return ConvertTo.String(HttpContext.Current.Session["BusinessAdsetId"]);
                }
            }

            set
            {
                HttpContext.Current.Session["BusinessAdsetId"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the facebook business ads Id.
        /// </summary>
        /// <value>The value of the facebook business ads Id.</value>
        public static string BusinessAdsID
        {
            get
            {
                if (HttpContext.Current.Session["BusinessAdsID"] == null)
                {
                    return "BusinessAdsID";
                }
                else
                {
                    return ConvertTo.String(HttpContext.Current.Session["BusinessAdsID"]);
                }
            }

            set
            {
                HttpContext.Current.Session["BusinessAdsID"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the size of the page.
        /// </summary>
        /// <value>The size of the page.</value>
        public static int PageSize
        {
            get
            {
                if (HttpContext.Current != null && HttpContext.Current.Session != null && HttpContext.Current.Session["PageSize"] != null)
                {
                    if (ConvertTo.Integer(HttpContext.Current.Session["PageSize"]) == 0)
                    {
                        HttpContext.Current.Session["PageSize"] = Configurations.PageSize;
                    }

                    return ConvertTo.Integer(HttpContext.Current.Session["PageSize"]);

                }
                else
                {
                    return 10;
                }
            }

            set
            {
                HttpContext.Current.Session["PageSize"] = value;
            }
        }
        public static int TestID
        {
            get
            {
                if (HttpContext.Current.Session["TestID"] == null)
                {
                    return 0;
                }
                else
                {
                    return ConvertTo.Integer(HttpContext.Current.Session["TestID"]);
                }
            }

            set
            {
                HttpContext.Current.Session["TestID"] = value;
            }
        }

        public static DateTime StartTime
        {
            get
            {
                if (HttpContext.Current.Session["StartTime"] == null)
                {
                    return DateTime.Now;
                }
                else
                {
                    return (DateTime)(HttpContext.Current.Session["StartTime"]);
                }
            }

            set
            {
                HttpContext.Current.Session["StartTime"] = value;
            }
        }

        public static int Result
        {
            get
            {
                if (HttpContext.Current.Session["Result"] == null)
                {
                    return 0;
                }
                else
                {
                    return ConvertTo.Integer(HttpContext.Current.Session["Result"]);
                }
            }

            set
            {
                HttpContext.Current.Session["Result"] = value;
            }
        }

        public static bool IsLogin
        {
            get
            {
                if (HttpContext.Current.Session["IsLogin"] == null)
                {
                    return false;
                }
                else
                {
                    return ConvertTo.Boolean(HttpContext.Current.Session["IsLogin"]);
                }
            }

            set
            {
                HttpContext.Current.Session["IsLogin"] = value;
            }
        }

        public static string StripeId
        {
            get
            {
                if (HttpContext.Current.Session["StripeId"] == null)
                {
                    return "StripeId";
                }
                else
                {
                    return ConvertTo.String(HttpContext.Current.Session["StripeId"]);
                }
            }

            set
            {
                HttpContext.Current.Session["StripeId"] = value;
            }
        }

        public static string Password
        {
            get
            {
                if (HttpContext.Current.Session["Password"] == null)
                {
                    return "Password";
                }
                else
                {
                    return ConvertTo.String(HttpContext.Current.Session["Password"]);
                }
            }

            set
            {
                HttpContext.Current.Session["Password"] = value;
            }
        }

        public static string IsMsg
        {
            get
            {
                if (HttpContext.Current.Session["IsMsg"] == null)
                {
                    return "0";
                }
                else
                {
                    return ConvertTo.String(HttpContext.Current.Session["IsMsg"]);
                }
            }

            set
            {
                HttpContext.Current.Session["IsMsg"] = value;
            }
        }

    }
}