using StructureMap;

namespace SironaTeleMed.Common
{
    public static class IoC
    {
        public static IContainer Initialize()
        {
            return new Container(
                x => x.Scan
                       (
                           scan =>
                           {
                               scan.Assembly("SironaTeleMed.Services");
                               scan.Assembly("SironaTeleMed");                               
                               scan.WithDefaultConventions();
                               scan.LookForRegistries();
                           }
                       )
              );
        }
    }
}