﻿//-----------------------------------------------------------------------
// <copyright file="ServiceModule.cs" company="Premiere Digital Services">
//     Copyright Premiere Digital Services. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace SironaTeleMed.Services
{
    using Autofac;
    using Contract;
    using Data;

    /// <summary>
    /// The Service module for dependency injection.
    /// </summary>
    public class ServiceModule : Module
    {
        /// <summary>
        /// Override to add registrations to the container.
        /// </summary>
        /// <param name="builder">The builder through which components can be
        /// registered.</param>
        /// <remarks>
        /// Note that the ContainerBuilder parameter is unique to this module.
        /// </remarks>
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<V1.UsersService>().As<AbstractUsersService>().InstancePerDependency();

            builder.RegisterType<V1.NotificationsService>().As<AbstractNotificationsService>().InstancePerDependency();
			builder.RegisterType<V1.SessionsService>().As<AbstractSessionsServices>().InstancePerDependency();
            builder.RegisterType<V1.SessionScheduleService>().As<AbstractSessionScheduleServices>().InstancePerDependency();
            builder.RegisterType<V1.MasterCountryService>().As<AbstractMasterCountryServices>().InstancePerDependency();
            builder.RegisterType<V1.SessionDocumentServices>().As<AbstractSessionDocumentServices>().InstancePerDependency();
            builder.RegisterType<V1.MasterStateService>().As<AbstractMasterStateService>().InstancePerDependency();
            builder.RegisterType<V1.MasterStatusService>().As<AbstractMasterStatusServices>().InstancePerDependency();
            //builder.RegisterType<V1.UserRattingServices>().As<AbstractUserRattingService>().InstancePerDependency();
            builder.RegisterModule<DataModule>();            
            base.Load(builder);
        }
    }
}
