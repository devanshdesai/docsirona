﻿using SironaTeleMed.Common;
using SironaTeleMed.Common.Paging;
using SironaTeleMed.Entities.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SironaTeleMed.Services.Contract
{
    public abstract class AbstractSessionRatingsServices
    {
        public abstract PagedList<AbstractSessionRatings> SessionRatingsSelectAll(PageParam pageParam);

        public abstract SuccessResult<AbstractSessionRatings> SessionRatingsSelect(int id);

        public abstract SuccessResult<AbstractSessionRatings> InsertUpdateSessionRatings(AbstractSessionRatings abstractSessionRatings);

        public abstract bool SessionRatingsDelete(int id);
    }
}
