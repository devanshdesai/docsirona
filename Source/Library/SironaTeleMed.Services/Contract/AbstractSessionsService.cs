﻿using SironaTeleMed.Common;
using SironaTeleMed.Common.Paging;
using SironaTeleMed.Entities.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SironaTeleMed.Services.Contract
{
    public abstract class AbstractSessionsServices
    {
        public abstract PagedList<AbstractSessions> SessionsSelectAll(PageParam pageParam);

        public abstract PagedList<AbstractSessions> SessionScheduleSelectByUserIdStatusId(PageParam pageParam, string StatusId, int? UserId, string DateSearch);

        public abstract PagedList<AbstractSessions> Top5UpcomingSessionSelectedRecords(PageParam pageParam, string StatusId, int? UserId);
        
        public abstract SuccessResult<AbstractSessions> SessionUpdateByStatusId(int Id, int StatusId, string RejectedReason, string CancelledReason, string AssignedTo);

        public abstract SuccessResult<AbstractSessions> SessionsSelect(int id, int UserTypeId = 0, int UsrId = 0);

        public abstract SuccessResult<AbstractSessions> InsertUpdateSessions(AbstractSessions abstractSessions);

        public abstract bool SessionsDelete(int id);

        public abstract SuccessResult<AbstractSessions> UpdateComments(AbstractSessions abstractSessions);

        public abstract PagedList<AbstractSessions> SessionSelectByUserId(int UserId);

        public abstract SuccessResult<AbstractSessions> SessionsSelectById(int id);

        public abstract PagedList<AbstractSessions> SearchPatientsHistory(PageParam pageParam, int StatusId, int? UserId, string DateSearch);

    }
}
