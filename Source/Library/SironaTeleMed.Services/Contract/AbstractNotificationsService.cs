﻿using SironaTeleMed.Common;
using SironaTeleMed.Common.Paging;
using SironaTeleMed.Entities.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SironaTeleMed.Services.Contract
{
    public abstract class AbstractNotificationsService
    {
        public abstract SuccessResult<AbstractNotifications> NotificationsInsert(AbstractNotifications abstractNotifications);
        
        public abstract PagedList<AbstractNotifications> NotificationsSelectByNotificationTo(PageParam pageParam, int notificationTo);
        
    }
}
