﻿using SironaTeleMed.Common;
using SironaTeleMed.Common.Paging;
using SironaTeleMed.Entities.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SironaTeleMed.Services.Contract
{
    public abstract class AbstractSessionDocumentServices
    {
        public abstract PagedList<AbstractSessionDocument> SessionDocumentBySessionId(int id);

        public abstract SuccessResult<AbstractSessionDocument> InsertSessionDocument(AbstractSessionDocument abstractSessionDocument);
    }
}
