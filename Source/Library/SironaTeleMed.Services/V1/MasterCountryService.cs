﻿using SironaTeleMed.Common;
using SironaTeleMed.Common.Paging;
using SironaTeleMed.Data.Contract;
using SironaTeleMed.Entities.Contract;
using SironaTeleMed.Services.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SironaTeleMed.Services.V1
{
    public class MasterCountryService : AbstractMasterCountryServices
    {
        private AbstractMasterCountryDao masterCountryDao;

        public MasterCountryService(AbstractMasterCountryDao masterCountryDao)
        {
            this.masterCountryDao = masterCountryDao;
        }
        public override PagedList<AbstractMasterCountry> MasterCountrySelectAll(PageParam pageParam)
        {
            return this.masterCountryDao.MasterCountrySelectAll(pageParam);
        }
        public override SuccessResult<AbstractMasterCountry> MasterCountrySelect(int id)
        {
            return this.masterCountryDao.MasterCountrySelect(id);
        }
        public override SuccessResult<AbstractMasterCountry> InsertUpdateMasterCountry(AbstractMasterCountry abstractMasterCountry)
        {
            return this.masterCountryDao.InsertUpdateMasterCountry(abstractMasterCountry);
        }
        public override bool MasterCountryDelete(int id)
        {
            return this.masterCountryDao.MasterCountryDelete(id);
        }

    }
}
