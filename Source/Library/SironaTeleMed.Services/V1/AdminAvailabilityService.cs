﻿using SironaTeleMed.Common;
using SironaTeleMed.Common.Paging;
using SironaTeleMed.Data.Contract;
using SironaTeleMed.Entities.Contract;
using SironaTeleMed.Services.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SironaTeleMed.Services.V1
{
    public class AdminAvailabilityService : AbstractAdminAvailabilityService
    {
        private AbstractAdminAvailabilityDao adminAvailabilityDao;

        public AdminAvailabilityService(AbstractAdminAvailabilityDao adminAvailabilityDao)
        {
            this.adminAvailabilityDao = adminAvailabilityDao;
        }
        public override PagedList<AbstractAdminAvailability> AdminAvailabilitySelectAll(PageParam pageParam)
        {
            return this.adminAvailabilityDao.AdminAvailabilitySelectAll(pageParam);
        }
        public override SuccessResult<AbstractAdminAvailability> AdminAvailabilitySelect(int id)
        {
            return this.adminAvailabilityDao.AdminAvailabilitySelect(id);
        }
        public override SuccessResult<AbstractAdminAvailability> InsertUpdateAdminAvailability(AbstractAdminAvailability abstractAdminAvailability)
        {
            return this.adminAvailabilityDao.InsertUpdateAdminAvailability(abstractAdminAvailability);
        }
        public override bool AdminAvailabilityDelete(int id)
        {
            return this.adminAvailabilityDao.AdminAvailabilityDelete(id);
        }

    }
}
