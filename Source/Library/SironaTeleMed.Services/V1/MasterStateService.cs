﻿using SironaTeleMed.Common;
using SironaTeleMed.Common.Paging;
using SironaTeleMed.Data.Contract;
using SironaTeleMed.Entities.Contract;
using SironaTeleMed.Services.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SironaTeleMed.Services.V1
{
    public class MasterStateService : AbstractMasterStateService
    {
        private AbstractMasterStateDao masterStateDao;

        public MasterStateService(AbstractMasterStateDao usersDao)
        {
            this.masterStateDao = usersDao;
        }
        public override PagedList<AbstractMasterState> MasterStateSelectAll(PageParam pageParam)
        {
            return this.masterStateDao.MasterStateSelectAll(pageParam);
        }
        public override SuccessResult<AbstractMasterState> MasterStateSelect(int id)
        {
            return this.masterStateDao.MasterStateSelect(id);
        }
        public override SuccessResult<AbstractMasterState> InsertUpdateMasterState(AbstractMasterState abstractMasterState)
        {
            return this.masterStateDao.InsertUpdateMasterState(abstractMasterState);
        }
        public override bool MasterStateDelete(int id)
        {
            return this.masterStateDao.MasterStateDelete(id);
        }
        public override PagedList<AbstractMasterState> MasterStateSelectByCountryId(PageParam pageParam, int CountryId)
        {
            return this.masterStateDao.MasterStateSelectByCountryId(pageParam, CountryId);
        }
        public override bool MasterStateDeleteByCountryId(int CountryId)
        {
            return this.masterStateDao.MasterStateDeleteByCountryId(CountryId);
        }
    }
}
