﻿using SironaTeleMed.Common;
using SironaTeleMed.Common.Paging;
using SironaTeleMed.Data.Contract;
using SironaTeleMed.Entities.Contract;
using SironaTeleMed.Services.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SironaTeleMed.Services.V1
{
    public class SessionScheduleService : AbstractSessionScheduleServices
    {
        private AbstractSessionScheduleDao sessionScheduleDao;

        public SessionScheduleService(AbstractSessionScheduleDao sessionScheduleDao)
        {
            this.sessionScheduleDao = sessionScheduleDao;
        }
        public override PagedList<AbstractSessionSchedule> SessionScheduleSelectAll(PageParam pageParam)
        {
            return this.sessionScheduleDao.SessionScheduleSelectAll(pageParam);
        }
        public override SuccessResult<AbstractSessionSchedule> SessionScheduleSelect(int id)
        {
            return this.sessionScheduleDao.SessionScheduleSelect(id);
        }
        public override PagedList<AbstractSessionSchedule> SessionScheduleSelectAllByDate(PageParam pageParam, string Date)
        {
            return this.sessionScheduleDao.SessionScheduleSelectAllByDate(pageParam, Date);
        }
        public override bool SesisonsScheduleIsAvailableUpdate(int id)
        {
            return this.sessionScheduleDao.SesisonsScheduleIsAvailableUpdate(id);
        }
        public override PagedList<AbstractSessionSchedule> SearchSessionsSelectByDate(string Date, int type = 0)
        {
            return this.sessionScheduleDao.SearchSessionsSelectByDate(Date,type);
        }
        public override SuccessResult<AbstractSessionSchedule> InsertUpdateSessionSchedule(AbstractSessionSchedule abstractSessionSchedule)
        {
            return this.sessionScheduleDao.InsertUpdateSessionSchedule(abstractSessionSchedule);
        }
        public override bool SessionScheduleDelete(int id)
        {
            return this.sessionScheduleDao.SessionScheduleDelete(id);
        }
        public override SuccessResult<AbstractSessionSchedule> SessionScheduleUpdateByStatusId(int id, int statusid)
        {
            return this.sessionScheduleDao.SessionScheduleUpdateByStatusId(id, statusid);
        }
    }
}
