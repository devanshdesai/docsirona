﻿using SironaTeleMed.Common;
using SironaTeleMed.Common.Paging;
using SironaTeleMed.Data.Contract;
using SironaTeleMed.Entities.Contract;
using SironaTeleMed.Services.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SironaTeleMed.Services.V1
{
    public class SessionRatingsService : AbstractSessionRatingsServices
    {
        private AbstractSessionRatingsDao sessionRatingsDao;

        public SessionRatingsService(AbstractSessionRatingsDao sessionRatingsDao)
        {
            this.sessionRatingsDao = sessionRatingsDao;
        }
        public override PagedList<AbstractSessionRatings> SessionRatingsSelectAll(PageParam pageParam)
        {
            return this.sessionRatingsDao.SessionRatingsSelectAll(pageParam);
        }
        public override SuccessResult<AbstractSessionRatings> SessionRatingsSelect(int id)
        {
            return this.sessionRatingsDao.SessionRatingsSelect(id);
        }
        public override SuccessResult<AbstractSessionRatings> InsertUpdateSessionRatings(AbstractSessionRatings abstractSessionRatings)
        {
            return this.sessionRatingsDao.InsertUpdateSessionRatings(abstractSessionRatings);
        }
        public override bool SessionRatingsDelete(int id)
        {
            return this.sessionRatingsDao.SessionRatingsDelete(id);
        }

    }
}
