﻿using SironaTeleMed.Common;
using SironaTeleMed.Common.Paging;
using SironaTeleMed.Data.Contract;
using SironaTeleMed.Entities.Contract;
using SironaTeleMed.Services.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SironaTeleMed.Services.V1
{
    public class MasterStatusService : AbstractMasterStatusServices
    {
        private AbstractMasterStatusDao masterStatusDao;

        public MasterStatusService(AbstractMasterStatusDao masterStatusDao)
        {
            this.masterStatusDao = masterStatusDao;
        }
        public override PagedList<AbstractMasterStatus> MasterStatusSelectAll(PageParam pageParam)
        {
            return this.masterStatusDao.MasterStatusSelectAll(pageParam);
        }
        public override SuccessResult<AbstractMasterStatus> MasterStatusSelect(int id)
        {
            return this.masterStatusDao.MasterStatusSelect(id);
        }
        public override SuccessResult<AbstractMasterStatus> InsertUpdateMasterStatus(AbstractMasterStatus abstractMasterStatus)
        {
            return this.masterStatusDao.InsertUpdateMasterStatus(abstractMasterStatus);
        }
        public override bool MasterStatusDelete(int id)
        {
            return this.masterStatusDao.MasterStatusDelete(id);
        }

    }
}
