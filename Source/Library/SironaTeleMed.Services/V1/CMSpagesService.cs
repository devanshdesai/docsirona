﻿using SironaTeleMed.Common;
using SironaTeleMed.Common.Paging;
using SironaTeleMed.Data.Contract;
using SironaTeleMed.Entities.Contract;
using SironaTeleMed.Services.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SironaTeleMed.Services.V1
{
    public class CMSpagesService : AbstractCMSpagesServices
    {
        private AbstractCMSpagesDao cMSpagesDao;

        public CMSpagesService(AbstractCMSpagesDao cMSpagesDao)
        {
            this.cMSpagesDao = cMSpagesDao;
        }
        public override PagedList<AbstractCMSpages> CMSpagesSelectAll(PageParam pageParam)
        {
            return this.cMSpagesDao.CMSpagesSelectAll(pageParam);
        }
        public override SuccessResult<AbstractCMSpages> CMSpagesSelect(int id)
        {
            return this.cMSpagesDao.CMSpagesSelect(id);
        }
        public override SuccessResult<AbstractCMSpages> InsertUpdateCMSpages(AbstractCMSpages abstractCMSpages)
        {
            return this.cMSpagesDao.InsertUpdateCMSpages(abstractCMSpages);
        }
        public override bool CMSpagesDelete(int id)
        {
            return this.cMSpagesDao.CMSpagesDelete(id);
        }

    }
}
