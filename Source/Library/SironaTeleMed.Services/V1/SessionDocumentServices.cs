﻿using SironaTeleMed.Common;
using SironaTeleMed.Common.Paging;
using SironaTeleMed.Data.Contract;
using SironaTeleMed.Entities.Contract;
using SironaTeleMed.Services.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SironaTeleMed.Services.V1
{
    public class SessionDocumentServices : AbstractSessionDocumentServices
    {
        private AbstractSessionDocumentDao sessionDocumentDao;

        public SessionDocumentServices(AbstractSessionDocumentDao sessionDocumentDao)
        {
            this.sessionDocumentDao = sessionDocumentDao;
        }
        public override PagedList<AbstractSessionDocument> SessionDocumentBySessionId(int id)
        {
            return this.sessionDocumentDao.SessionDocumentBySessionId(id);
        }
        public override SuccessResult<AbstractSessionDocument> InsertSessionDocument(AbstractSessionDocument abstractSessionDocument)
        {
            return this.sessionDocumentDao.InsertSessionDocument(abstractSessionDocument);
        }
    }
}
