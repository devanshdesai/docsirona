﻿using SironaTeleMed.Common;
using SironaTeleMed.Common.Paging;
using SironaTeleMed.Data.Contract;
using SironaTeleMed.Entities.Contract;
using SironaTeleMed.Services.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SironaTeleMed.Services.V1
{
    public class SessionsService : AbstractSessionsServices
    {
        private AbstractSessionsDao sessionsDao;

        public SessionsService(AbstractSessionsDao sessionsDao)
        {
            this.sessionsDao = sessionsDao;
        }
        public override PagedList<AbstractSessions> SessionsSelectAll(PageParam pageParam)
        {
            return this.sessionsDao.SessionsSelectAll(pageParam);
        }
        public override SuccessResult<AbstractSessions> SessionUpdateByStatusId(int Id, int StatusId, string RejectedReason, string CancelledReason, string AssignedTo)
        {
            return this.sessionsDao.SessionUpdateByStatusId(Id,StatusId, RejectedReason, CancelledReason, AssignedTo);
        }
        public override PagedList<AbstractSessions> SessionScheduleSelectByUserIdStatusId(PageParam pageParam, string StatusId, int? UserId, string DateSearch)
        {
            return this.sessionsDao.SessionScheduleSelectByUserIdStatusId(pageParam , StatusId,  UserId, DateSearch);
        }
        public override PagedList<AbstractSessions> Top5UpcomingSessionSelectedRecords(PageParam pageParam, string StatusId, int? UserId)
        {
            return this.sessionsDao.Top5UpcomingSessionSelectedRecords(pageParam, StatusId, UserId);
        }
        public override SuccessResult<AbstractSessions> SessionsSelect(int id, int UserTypeId = 0, int UsrId = 0)
        {
            return this.sessionsDao.SessionsSelect(id, UserTypeId, UsrId);
        }
        public override SuccessResult<AbstractSessions> InsertUpdateSessions(AbstractSessions abstractSessions)
        {
            return this.sessionsDao.InsertUpdateSessions(abstractSessions);
        }
        public override SuccessResult<AbstractSessions> UpdateComments(AbstractSessions abstractSessions)
        {
            return this.sessionsDao.UpdateComments(abstractSessions);
        }
        public override bool SessionsDelete(int id)
        {
            return this.sessionsDao.SessionsDelete(id);
        }
		public override PagedList<AbstractSessions> SessionSelectByUserId(int UserId)
        {
            return this.sessionsDao.SessionSelectByUserId(UserId);
		}	
		public override SuccessResult<AbstractSessions> SessionsSelectById(int id)
        {
            return this.sessionsDao.SessionsSelectById(id);
		}
        public override PagedList<AbstractSessions> SearchPatientsHistory(PageParam pageParam, int StatusId, int? UserId, string DateSearch)
        {
            return this.sessionsDao.SearchPatientsHistory(pageParam, StatusId, UserId, DateSearch);
        }
    }
}
