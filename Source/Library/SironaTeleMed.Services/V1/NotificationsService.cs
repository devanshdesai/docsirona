﻿using SironaTeleMed.Common;
using SironaTeleMed.Common.Paging;
using SironaTeleMed.Data.Contract;
using SironaTeleMed.Entities.Contract;
using SironaTeleMed.Services.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SironaTeleMed.Services.V1
{
    public class NotificationsService : AbstractNotificationsService
    {
        private AbstractNotificationsDao notificationsDao;

        public NotificationsService(AbstractNotificationsDao notificationsDao)
        {
            this.notificationsDao = notificationsDao;
        }
        public override SuccessResult<AbstractNotifications> NotificationsInsert(AbstractNotifications abstractNotifications)
        {
            return this.notificationsDao.NotificationsInsert(abstractNotifications);
        }
        public override PagedList<AbstractNotifications> NotificationsSelectByNotificationTo(PageParam pageParam, int notificationsTo)
        {
            return this.notificationsDao.NotificationsSelectByNotificationTo(pageParam, notificationsTo);
        }

    }
}
