﻿using SironaTeleMed.Common;
using SironaTeleMed.Common.Paging;
using SironaTeleMed.Data.Contract;
using SironaTeleMed.Entities.Contract;
using SironaTeleMed.Services.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SironaTeleMed.Services.V1
{
    public class UsersService : AbstractUsersService
    {
        private AbstractUsersDao usersDao;

        public UsersService(AbstractUsersDao usersDao)
        {
            this.usersDao = usersDao;
        }
        public override PagedList<AbstractUsers> UsersSelectAll(PageParam pageParam, int UserType)
        {
            return this.usersDao.UsersSelectAll(pageParam, UserType);
        }
        public override SuccessResult<AbstractUsers> UsersSelect(int id)
        {
            return this.usersDao.UsersSelect(id);
        }
        public override SuccessResult<AbstractUsers> InsertUpdateUsers(AbstractUsers abstractUsers)
        {
            return this.usersDao.InsertUpdateUsers(abstractUsers);
        }
        public override bool UsersDelete(int id)
        {
            return this.usersDao.UsersDelete(id);
        }
        public override PagedList<AbstractUsers> UsersSelectByStateId(PageParam pageParam, int stateId)
        {
            return this.usersDao.UsersSelectByStateId(pageParam, stateId);
        }
        public override PagedList<AbstractUsers> UsersSelectByCountryId(PageParam pageParam, int countryId)
        {
            return this.usersDao.UsersSelectByCountryId(pageParam, countryId);
        }
        public override bool UsersDeleteByStateId(int stateId)
        {
            return this.usersDao.UsersDeleteByStateId(stateId);
        }
        public override bool UsersDeleteBycountryId(int countryId)
        {
            return this.usersDao.UsersDeleteBycountryId(countryId);
        }
        public override SuccessResult<AbstractUsers> Login(AbstractUsers abstractUsers)
        {
            return this.usersDao.Login(abstractUsers);
        }
		public override int UpdateUserPassword(AbstractUsers abstractUsers)
        {
            return this.usersDao.UpdateUserPassword(abstractUsers);
        }
        public override SuccessResult<AbstractUsers> UsersResetPassword(AbstractUsers abstractUsers)
        {
            return this.usersDao.UsersResetPassword(abstractUsers);
        }
        public override SuccessResult<AbstractUsers> ForgotPassword(string email, string password)
        {
            return this.usersDao.ForgotPassword(email, password);
        }
        public override bool UserStatusUpdate(int id)
        {
            return this.usersDao.UserStatusUpdate(id);
        }
        public override PagedList<AbstractUsers> UsersSelectByAssignId(PageParam pageParam, int? AssignId)
        {
            return this.usersDao.UsersSelectByAssignId(pageParam, AssignId);
        }
        public override bool DoctorStatusUpdate(int id)
        {
            return this.usersDao.DoctorStatusUpdate(id);
        }
        public override SuccessResult<AbstractUsers> GuestInsert(AbstractUsers abstractUsers)
        {
            return this.usersDao.GuestInsert(abstractUsers);
        }
    }
}
